provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

resource "aws_s3_bucket" "mc5e-terraform" {
  bucket = "mc5e-terraform"
  acl = "private"

  versioning {
    enabled = true
  }

  tags = {
    Name = "Terraform State"
  }
}
