locals {
  s3_state     = "mc5e-terraform-state"
  s3_state_key = "state"
}

terraform {
  backend "http" {
  }
}

//terraform {
//  backend "s3" {
//    bucket = "mc5e-terraform"
//    key    = "state"
//    region = "eu-west-1"
//  }
//}

provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

resource "aws_dynamodb_table" "monsters" {
  name           = "mc5e-monsters-${terraform.workspace}"
  hash_key       = "MonsterID"
  read_capacity  = 5
  write_capacity = 5

  attribute {
    name = "MonsterID"
    type = "S"
  }

  tags = {
    Environment = "${terraform.workspace}"
  }
}

resource "aws_cognito_user_pool" "users" {
  name                     = "mc5e-users-${terraform.workspace}"
  auto_verified_attributes = [
    "email"
  ]

  username_configuration {
    case_sensitive = false
  }

  schema {
    name                     = "email"
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    required                 = true
    string_attribute_constraints {
      max_length = 2048
      min_length = 0
    }
  }

  password_policy {
    minimum_length                   = 8
    require_lowercase                = true
    require_numbers                  = true
    require_symbols                  = false
    require_uppercase                = true
    temporary_password_validity_days = 7
  }

  tags = {
    Environment = terraform.workspace
  }
}

resource "aws_cognito_user_pool_client" "client" {
  name                                 = "5e-monster-creator-${terraform.workspace}"
  user_pool_id                         = aws_cognito_user_pool.users.id
  allowed_oauth_flows_user_pool_client = true
  default_redirect_uri                 = terraform.workspace == "prod" ? "https://aquavitae.gitlab.io/5e-monster-creator/" : "http://localhost:3000/5e-monster-creator/"
  logout_urls                          = [
    terraform.workspace == "prod" ? "https://aquavitae.gitlab.io/5e-monster-creator/" : "http://localhost:3000/5e-monster-creator/"
  ]

  allowed_oauth_flows          = [
    "implicit"
  ]
  allowed_oauth_scopes         = [
    "openid",
    "phone",
    "aws.cognito.signin.user.admin",
    "profile",
    "email",
  ]
  callback_urls                = [
    terraform.workspace == "prod" ? "https://aquavitae.gitlab.io/5e-monster-creator/" : "http://localhost:3000/5e-monster-creator/"
  ]
  explicit_auth_flows          = [
    "ALLOW_USER_SRP_AUTH",
    "ALLOW_REFRESH_TOKEN_AUTH",
    "ALLOW_CUSTOM_AUTH",
  ]
  supported_identity_providers = [
    "COGNITO"
  ]

}

resource "aws_cognito_user_pool_domain" "domain" {
  user_pool_id = aws_cognito_user_pool.users.id
  domain       = "5e-monster-creator${terraform.workspace == "prod" ? "" : "-${terraform.workspace}"}"
}

resource "aws_cognito_identity_pool" "identity" {
  identity_pool_name               = "5e Monster Creator ${terraform.workspace == "prod" ? "" : "${terraform.workspace}"}"
  allow_unauthenticated_identities = false

  cognito_identity_providers {
    client_id               = aws_cognito_user_pool_client.client.id
    provider_name           = aws_cognito_user_pool.users.arn
    server_side_token_check = false
  }
}

data "aws_iam_policy_document" "users_db_policy_document" {
  statement {
    actions   = [
      "dynamodb:DeleteItem",
      "dynamodb:GetItem",
      "dynamodb:PutItem",
      "dynamodb:Query",
      "dynamodb:UpdateItem"
    ]

    resources = [
      aws_dynamodb_table.monsters.arn]

    condition {
      test     = "ForAllValues:StringEquals"
      variable = "dynamodb:LeadingKeys"
      values   = [
        "&{cognito-identity.amazonaws.com:sub}"]
    }
  }
}

resource "aws_iam_policy" "user_db_policy" {
  policy = data.aws_iam_policy_document.users_db_policy_document.json
}

resource "aws_iam_role" "mc5e_auth_role" {
  name               = "mc5e-auth-role-${terraform.workspace}"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": [
          "dynamodb.amazonaws.com"
        ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ec2-read-only-policy-attachment" {
  role       = aws_iam_role.mc5e_auth_role.name
  policy_arn = aws_iam_policy.user_db_policy.arn
}

output "identity_pool_id" {
  value = aws_cognito_identity_pool.identity.id
}

output "auth_domain" {
  value = "${aws_cognito_user_pool_domain.domain.domain}.auth.eu-west-1.amazoncognito.com"
}

output login_client_id {
  value = aws_cognito_user_pool_client.client.id
}

output login_redirect_uri {
  value = aws_cognito_user_pool_client.client.default_redirect_uri
}