import { MonsterDefinition } from './monster/types';
import YAML from 'yaml';

export const cmp = <T>(indexer: (x: T) => string | number) => (a: T, b: T) => {
  const va = indexer(a);
  const vb = indexer(b);
  return va > vb ? 1 : va < vb ? -1 : 0;
};

export const parseYAML = (doc: string): MonsterDefinition => {
  try {
    return YAML.parse(doc) || {};
  } catch (e) {
    console.debug(e);
  }
  return {} as MonsterDefinition;
};
