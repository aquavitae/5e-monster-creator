import AWS from 'aws-sdk';

class DynamoDBProvider {
  private readonly docClient?: AWS.DynamoDB.DocumentClient;

  constructor() {
    this.docClient = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });
  }

  listMonsters() {
    return {};
  }
}

export default DynamoDBProvider;
