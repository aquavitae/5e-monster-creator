import { makeStoredMonster, StoredMonster } from '../components/storage';

class LocalStorageProvider {
  listMonsters() {
    return JSON.parse(localStorage.getItem('monsters') || '[{}]').map(
      (sm: any): StoredMonster =>
        typeof sm === 'string'
          ? makeStoredMonster(sm)
          : makeStoredMonster(sm.doc || '', sm.name || 'UNNAMED', '0'),
    );
  }
}

export default LocalStorageProvider;
