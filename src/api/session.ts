import { CognitoAuth } from 'amazon-cognito-auth-js';
import { Config } from '../components/config';
import DynamoDBProvider from './DynamoDBProvider';
import LocalStorageProvider from './LocalStorageProvider';
import AWS from 'aws-sdk';

interface Provider {
  listMonsters(): object;
}

export class Session {
  private readonly auth?: CognitoAuth;
  private readonly dataProvider: Provider;

  constructor(config?: Config, auth?: CognitoAuth) {
    this.auth = auth;
    if (config !== undefined && this.auth !== undefined && this.auth.isUserSignedIn()) {
      AWS.config.update({
        region: 'eu-west-1',
        credentials: new AWS.CognitoIdentityCredentials({
          IdentityPoolId: config.identityPoolId,
        }),
      });

      this.dataProvider = new DynamoDBProvider();
    } else {
      this.dataProvider = new LocalStorageProvider();
    }
  }

  isLoggedIn(): boolean {
    return this.auth !== undefined && this.auth.isUserSignedIn();
  }

  login() {
    if (this.auth !== undefined) {
      this.auth.getSession();
    }
  }

  logout() {
    if (this.auth !== undefined) {
      this.auth.signOut();
    }
  }

  listMonsters() {
    return this.dataProvider.listMonsters();
  }
}

export const initSession = (config: Config): Session => {
  const authData = {
    ClientId: config.loginClientId,
    AppWebDomain: config.authDomain,
    TokenScopesArray: ['email', 'profile', 'openid'],
    RedirectUriSignIn: config.loginRedirectUri,
    RedirectUriSignOut: config.loginRedirectUri,
  };

  const auth = new CognitoAuth(authData);
  auth.userhandler = {
    onSuccess: (result) => {
      window.history.replaceState(null, '', window.location.href.split('#')[0]);
    },
    onFailure: (err) => {
      console.error(err);
    },
  };

  // The a user has just logged in, the token will be in the URL, so we
  // strip it out and stick it in local storage
  const curUrl = window.location.href;
  auth.parseCognitoWebResponse(curUrl);

  return new Session(config, auth);
};
