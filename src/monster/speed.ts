export interface Speed {
  walk?: number;
  fly?: number;
  climb?: number;
  burrow?: number;
  swim?: number;
  extra?: string;
}

export type SSpeed = string | number | Speed | undefined;
export const parseSpeed = (s: SSpeed): Speed => {
  s = s || undefined;
  switch (typeof s) {
    default:
    case 'undefined':
      return { walk: 30 };
    case 'string':
      return { walk: parseInt(s) };
    case 'number':
      return { walk: s };
    case 'object':
      return s;
  }
};
