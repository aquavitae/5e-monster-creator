import { parseSpeed } from './speed';

test('parses a numeric speed', () => {
  expect(parseSpeed(40)).toEqual({ walk: 40 });
});
