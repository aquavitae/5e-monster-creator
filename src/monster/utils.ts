import { Monster } from './types';
import { TraitType } from './traits/types';
import { Trait } from './traits/trait';
import { sentence } from './traits/util';

export const modifier = (ability: number) => Math.floor((ability - 10) / 2);

export const withSign = (n: number) => `${n >= 0 ? '+' : ''}${n}`;

export const getAttack = <T>(m: Monster, name: string, call: (t: Trait) => T): T | undefined => {
  const t = m.traits
    .filter((t) => t.type === TraitType.Action)
    .filter((t) => t.canonicalName.toLowerCase() === name.toLowerCase())[0];
  return t ? call(t) : undefined;
};

export const parseStringArray = (s: any) =>
  s
    ? typeof s === 'string'
      ? s.split(/\s*,\s*/g)
      : Array.isArray(s)
      ? s.map((x) => x.toString())
      : []
    : [];

export const parseLanguages = (s: any) =>
  parseStringArray(s)
    .map((x) => sentence(x))
    .sort();
