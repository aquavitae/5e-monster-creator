export enum Alignment {
  Any = 'any alignment',
  Unaligned = 'unaligned',
  LawfulGood = 'lawful good',
  LawfulNeutral = 'lawful neutral',
  LawfulEvil = 'lawful evil',
  NeutralGood = 'neutral good',
  Neutral = 'neutral',
  NeutralEvil = 'neutral evil',
  ChaoticGood = 'chaotic good',
  ChaoticNeutral = 'chaotic neutral',
  ChaoticEvil = 'chaotic evil',
}

export type SAlignment = string | undefined;
export const parseAlignment = (text: SAlignment) => {
  if (text === undefined) {
    return Alignment.Any;
  }

  const al = text
    ? text.includes(' ') && text.length > 2
      ? text
          .split(' ')
          .map((x) => x[0])
          .join('')
      : text
    : '';

  switch (al.toLowerCase()) {
    case 'lawfulgood':
    case 'lg':
      return Alignment.LawfulGood;
    case 'lawfulneutral':
    case 'ln':
      return Alignment.LawfulNeutral;
    case 'lawfulevil':
    case 'le':
      return Alignment.LawfulEvil;
    case 'neutralgood':
    case 'ng':
      return Alignment.NeutralGood;
    case 'neutral':
    case 'n':
      return Alignment.Neutral;
    case 'neutralevil':
    case 'ne':
      return Alignment.NeutralEvil;
    case 'chaoticgood':
    case 'cg':
      return Alignment.ChaoticGood;
    case 'chaoticneutral':
    case 'cn':
      return Alignment.ChaoticNeutral;
    case 'chaoticevil':
    case 'ce':
      return Alignment.ChaoticEvil;
    case 'unaligned':
    case 'u':
      return Alignment.Unaligned;
    case 'any':
    default:
      return Alignment.Any;
  }
};
