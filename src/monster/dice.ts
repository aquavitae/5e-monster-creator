interface Die {
  n: number;
  d: number;
}

const parseDie = (text: string) => {
  const m = /([+-]?\d+)?d(\d+)/.exec(text);
  if (!m || m.length < 3) {
    return null;
  }
  const d = { n: parseInt(m[1]), d: parseInt(m[2]) } as Die;
  Object.freeze(d);
  return d;
};

export interface Dice {
  mod: number;
  dice: Die[];
  average: number;
  text: string;
}

export const makeDice = (t: { dice?: Die[]; mod?: number }) => {
  const avg = average(t.dice || [], t.mod || 0);
  return Object.freeze({
    mod: t.mod || 0,
    dice: Object.freeze((t.dice || []).map((d) => Object.freeze(d))),
    average: Math.floor(avg),
    text: renderDice(t.dice, t.mod),
  } as Dice);
};

export const addDice = (...all: (Dice | string | number)[]) =>
  makeDice(
    all
      .map((d) => (typeof d === 'object' ? d : parseDice(d)))
      .reduce(
        (r, d) => ({
          dice: [...r.dice, ...d.dice],
          mod: r.mod + d.mod,
        }),
        { dice: [] as Die[], mod: 0 },
      ),
  );

export const increaseDieCount = (dice: Dice, n: number) =>
  makeDice({ dice: dice.dice.map((die) => ({ d: die.d, n: die.n + n })) });

export const parseDice = (text: string | number | undefined) => {
  if (typeof text === 'number') {
    return makeDice({ dice: [], mod: text });
  }

  const matches = (text?.toString() || '').replace(/ /g, '').match(/((?:^|[+-])[^+-]+)/g);

  let mod = 0;
  let dice: Die[] = [];

  for (const m of matches || []) {
    if (m.includes('d')) {
      const die = parseDie(m);
      if (die === null) {
        console.error(`Ignoring invalid die expression ${text}`);
      } else {
        dice = [...dice, die];
      }
    } else {
      mod += parseInt(m);
    }
  }

  return makeDice({ dice, mod });
};

const average = (dice: Die[], mod: number) =>
  dice.reduce((r, die) => r + ((die.d + 1) * die.n) / 2, 0) + mod;

const sign = (positive: boolean, first: boolean) =>
  positive ? (first ? '' : '+\u00A0') : first ? '-' : '-\u00A0';

const renderDie = (d: Die, first: boolean) => `${sign(d.n >= 0, first)}${Math.abs(d.n)}d${d.d}`;

const renderMod = (mod?: number) => (mod ? `${mod > 0 ? '+' : '-'}\u00A0${Math.abs(mod)}` : '');

const renderDice = (dice?: Die[], mod?: number) =>
  dice
    ? dice
        .map((d, i) => renderDie(d, i === 0))
        .concat([renderMod(mod)])
        .join('\u00A0')
        .trim()
    : `${mod || ''}`;
