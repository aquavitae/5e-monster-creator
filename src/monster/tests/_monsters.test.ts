import {
  ChallengeRating,
  cr0125,
  cr025,
  cr05,
  cr0x0,
  cr0x10,
  cr1,
  cr11,
  cr16,
  cr19,
  cr2,
  cr20,
  cr21,
  cr3,
  cr4,
  cr5,
  cr7,
} from '../challenge-rating';
import { compile } from '../build';
import YAML from 'yaml';
import { readFileSync, writeFileSync } from 'fs';

const UPDATE = false;

const testCR = (name: string, expectedCR?: ChallengeRating) => () => {
  const def = YAML.parse(readFileSync(`src/monster/tests/${name}.yml`, 'utf-8'));

  const m = compile(def);
  const gotCR = m.cr.value;

  delete m.cr;
  delete m.crDetails;

  const jsonPath = `src/monster/tests/${name}.json`;

  if (UPDATE) {
    console.log(JSON.stringify(m));
    writeFileSync(jsonPath, JSON.stringify(m, undefined, 2));
    fail('UPDATE is true');
  }

  const compiled = JSON.parse(readFileSync(jsonPath, 'utf-8'));

  expect(JSON.parse(JSON.stringify(m))).toEqual(compiled);
  if (expectedCR) {
    expect(gotCR).toEqual(expectedCR.value);
  }
};

describe('test monster CR calculations', () => {
  test('Aarakocra', testCR('aarakocra', cr025));
  // test('Ancient black dragon', testCR('ancient black dragon', cr21));
  // test('Ancient brass dragon', testCR('ancient brass dragon', cr20));
  // test('Androsphinx', testCR('androsphinx', cr17))
  test('Awakened Shrub', testCR('awakened shrub', cr0x10));
  test('Azer', testCR('azer', cr2));
  test('Balor', testCR('balor', cr19));
  test('Banshee', testCR('banshee', cr4));
  // test('Barbed devil', testCR('barbed devil', cr5))
  // test('Bearded devil', testCR('bearded devil',cr3 ))
  test('Behir', testCR('behir', cr11));
  // test('Black pudding', testCR('black pudding', cr4))
  test('Boar', testCR('boar', cr025));
  test('Bugbear', testCR('bugbear', cr1));
  // test('Bullywug', testCR('bullywug', cr025))
  // test('Cambion', testCR('cambion', cr5))
  // test('Centaur', testCR('centaur', cr2))
  // test('Constrictor snake', testCR('constrictor snake', cr025))
  // test('Dao', testCR('dao', cr11))
  // test('Darkmantle', testCR('darkmantle', cr05))
  // test('Demilich', testCR('demilich', cr18))
  // test('Deva', testCR('deva', cr10))
  // test('Djinni', testCR('djinni', cr11))
  // test('Doppelganger', testCR('doppelganger', cr3))
  // test('Drow', testCR('drow', cr025))
  // test('Duergar', testCR('duergar', cr1))
  // test('Earth elemental', testCR('earth elemental', cr5));
  // test('Ettercap', testCR('ettercap', cr2))
  // test('Ettin', testCR('ettin', cr4))
  test('Frog', testCR('frog', cr0x0));
  // test('Faerie dragon (red)', testCR('faerie dragon', cr1))
  // test('Flameskull', testCR('flameskull', cr4))
  // test('Flesh golem', testCR('flesh golem', cr5))
  test('Flying snake', testCR('flying snake')); // Stupidly overpowered, and the calcs show
  // test('Flying sword', testCR('flying sword', cr025))
  // test('Gargoyle', testCR('gargoyle', cr2))
  // test('Ghost', testCR('ghost', cr4))
  // test('Giant spider', testCR('giant spider', cr))
  // test('Gnoll', testCR('gnoll', cr05))
  // test('Goblin', testCR('goblin', cr025))
  // test('Goblin boss', testCR('goblin boss', cr1))
  test('Green hag', testCR('green hag', cr3));
  // test('Grimlock', testCR('grimlock', cr025))
  // test('Hell hound', testCR('hell hound', cr3))
  // test('Hobgoblin', testCR('hobgoblin', cr05))
  // test('Hobgoblin captain', testCR('hobgoblin captain', cr3))
  // test('Hobgoblin warlord', testCR('hobgoblin warlord', cr6))
  test('Hook horror', testCR('hook horror', cr3));
  // test('Imp', testCR('imp', cr1))
  // test('Iron golem', testCR('iron golem', cr16))
  test('Kobold', testCR('kobold', cr0125));
  test('Kua-toa', testCR('kua-toa', cr025));
  // test('Lich', testCR('lich', cr21))
  test('Lizardfolk', testCR('lizardfolk', cr05));
  test('Magmin', testCR('magmin', cr05));
  test('Marilith', testCR('marilith', cr16));
  test('Mimic', testCR('mimic', cr2));
  // test('Minotaur', testCR('minotaur', cr3))
  // test('Night hag', testCR('night hag', cr5))
  // test('Ogre', testCR('ogre', cr2));
  test('Orc', testCR('orc', cr05));
  // test('Peryton', testCR('peryton', cr2))
  // test('Quaggoth', testCR('quaggoth', cr2))
  // test('Revenant', testCR('revenant', cr5))
  // test('Roper', testCR('roper', cr5))
  // test('Sahuagin', testCR('sahuagin', cr05))
  // test('Shadow demon', testCR('shadow demon', cr4))
  test('Stone giant', testCR('stone giant', cr7));
  // test('Tiger', testCR('tiger', cr1))
  // test('Troglodyte', testCR('troglodyte', cr025))
  // test('Troll', testCR('troll', cr5))
  // test('Umber hulk', testCR('umber hulk', cr5))
  // test('Vampire', testCR('vampire', cr13))
  test('Wereboar', testCR('wereboar', cr4));
  // test('Wererat', testCR('wererat', cr2))

  // Problem ones
  // test('Kenku', testCR('kenku', cr025));
  // test('Stirge', testCR('stirge', cr0125));
  // test('Wight', testCR('wight', cr3)); => 2
  // test('Zombie', testCR('zombie', cr025));
});
