import { Monster } from './types';
import { getBestAttack } from './traits/calculators';
import { totalArmorAC } from './armor';
import { modifier } from './utils';
import { AdjustCRStats, CRStatChange, CRStats, TraitType } from './traits/types';
import {
  ChallengeRating,
  cr0x0,
  cr0x10,
  crByIndexFloor,
  crTable,
  indexFor,
} from './challenge-rating';
import { Trait } from './traits/trait';

const makeFakeTrait = (name: string, adjustCRStats: AdjustCRStats<any>): Trait =>
  new Trait(
    { name, type: TraitType.Action, render: '', adjustCRStats },
    { canonicalName: name, props: {} },
  );

const makeLegendaryTraits = (m: Monster): Trait[] => {
  if (!m.legendaryActions) return [];
  const total = m.legendaryActions.number;
  const r = (m.legendaryActions.actions || [])
    .map((la) => ({ dpr: la.calcDPR(m), n: la.cost(), name: la.canonicalName }))
    .filter((x): x is { dpr: number; n: number; name: string } => !!x.dpr)
    .map((x) => ({ ...x, dpc: x.dpr / x.n }))
    .sort((a, b) => (a.dpc < b.dpc ? -1 : a.dpc > b.dpc ? 1 : 0))
    .reduce(
      (r, x) => {
        const toAdd = Math.floor((total - r.used) / x.n);
        const trait = makeFakeTrait(x.name, (m: Monster, s: CRStats) => ({
          ...s,
          dpr: s.dpr + toAdd * x.dpr,
        }));
        return { used: r.used + toAdd * x.n, traits: [...r.traits, trait] };
      },
      { used: 0, traits: [] as Trait[] },
    );
  return r.traits;
};

// Increase AC for flying monsters at lower CR ratings
const flyingTrait = makeFakeTrait('Speed: flying', (m: Monster, s: CRStats) => ({
  ...s,
  ac: s.ac + (m.speed.fly && m.cr.value <= 10 ? 2 : 0),
}));

// Increase AC for more than 3 saves
const savesTrait = makeFakeTrait('Saves: 3 or more', (m: Monster, s: CRStats) => ({
  ...s,
  ac: s.ac + (m.saves.length >= 5 ? 4 : m.saves.length >= 3 ? 2 : 0),
}));

// Adjust for immunities and resistances
const resistancesTrait = makeFakeTrait('Resistances: multiplier', (m: Monster, s: CRStats) => ({
  ...s,
  hp:
    s.hp *
    (m.damageImmunities?.affectsCR
      ? immunityMultiplier(m.cr.value)
      : m.damageResistances?.affectsCR
      ? resistanceMultiplier(m.cr.value)
      : 1),
}));

export const calculateCR = (m: Monster): Monster => {
  const bestAttack = getBestAttack(m);
  const saveDC = (bestAttack && bestAttack.calcDC(m)) || 0;
  const startingStats = {
    hp: m.hitPoints,
    ac: totalArmorAC(m.optimalArmor, modifier(m.scores.dex)),
    dpr: (bestAttack && bestAttack.calcDPR(m)) || 0,
    ab: (bestAttack && bestAttack.calcAB(m)) || 0,
    dc: saveDC || 0,
  };

  const startingChanges: CRStatChange[] = [
    {
      trait: bestAttack?.canonicalName || '',
      explain: '',
      diff: { hp: 0, ac: 0, dpr: startingStats.dpr, ab: startingStats.ab, dc: startingStats.dc },
    },
  ];

  const { stats, statChanges } = [
    resistancesTrait,
    ...m.traits,
    ...makeLegendaryTraits(m),
    flyingTrait,
    savesTrait,
  ].reduce(
    (
      s: {
        statChanges: CRStatChange[];
        stats: { ab: number; ac: number; dpr: number; hp: number; dc: number };
      },
      t,
    ) => {
      const [newStats, explain] = t.adjustCRStats(m, s.stats);
      if (explain == null) return s;
      const diff: CRStats = {
        dpr: newStats.dpr - s.stats.dpr,
        ab: newStats.ab - s.stats.ab,
        dc: newStats.dc - s.stats.dc,
        hp: newStats.hp - s.stats.hp,
        ac: newStats.ac - s.stats.ac,
      };
      return {
        stats: newStats,
        statChanges: [...s.statChanges, { trait: t.canonicalName, explain, diff }],
      };
    },
    { stats: startingStats, statChanges: startingChanges },
  );

  const offensiveIndex = offensiveCRIndex(stats.dpr, stats.ab, stats.dc);
  const defensiveIndex = defensiveCRIndex(stats.hp, stats.ac);

  // Use 0.8 (0.3 + 0.5) as the threshold for rounding up/down
  const index = Math.floor(0.3 + (offensiveIndex + defensiveIndex) / 2);
  const unadjusted = testCR0(m, stats.dpr, crByIndexFloor(index));

  // // Adjust the CR upward if the monster is capable of killing a party member
  // // in one round. I have no idea how this is actually supposed to work since
  // // it's not really documented. Simply increasing by one rating if the DPR
  // // exceeds a party member's HP seems to roughly calibrate with what's in the Monster Manual.
  // const cr = Math.round(stats.dpr) > partyMemberHP(unadjusted) ? nextCR(unadjusted) : unadjusted;

  const cr = crTable[crTable.indexOf(unadjusted) + m.adjustCR];

  return {
    ...m,
    cr,
    crDetails: {
      stats,
      statChanges,
      offensiveIndex,
      defensiveIndex,
      unadjusted,
    },
  };
};

const resistanceMultiplier = (cr: number) => {
  if (cr <= 4) return 2;
  if (cr <= 10) return 1.5;
  if (cr <= 16) return 1.25;
  return 1;
};

const immunityMultiplier = (cr: number) => {
  if (cr <= 10) return 2;
  if (cr <= 16) return 1.5;
  return 1.25;
};

const offensiveCRIndex = (dpr: number, ab: number, dc: number) => {
  const i = indexFor(dpr, 'dpr');
  if (dc) {
    return i + (dc - crByIndexFloor(i).saveDC) / 2;
  }
  return i + (ab - crByIndexFloor(i).attackBonus) / 2;
};

const defensiveCRIndex = (hp: number, ac: number) => {
  const i = indexFor(hp, 'hp');
  return i + (ac - crByIndexFloor(i).armorClass) / 2;
};

const testCR0 = (m: Monster, dpr: number, cr: ChallengeRating) =>
  cr.value === 0 ? (dpr === 0 ? cr0x0 : cr0x10) : cr;
