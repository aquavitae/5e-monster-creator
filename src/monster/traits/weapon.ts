import { Range, TraitTemplate, TraitType, WeaponTemplate } from './types';
import { Monster } from '../types';
import { renderDamage } from './renderers';
import {
  calculateWeaponAttackBonus,
  calculateWeaponDPR,
  calculatePrimaryDamageDice,
} from './calculators';
import { applyRender, lookup, signed } from './util';
import { weaponsTable } from './weapons';
import { AttackType, parseAttackType, parseWeaponType, WeaponType } from './weapon-type';
import { Damage, DamageType, parseDamage } from './damage';
import { Dice } from '../dice';

const setValue = <T>(
  value: any,
  templateDefault: T,
  defaultValue: T | undefined,
  parser: (v: any) => T = (x) => x,
) => {
  if (value !== undefined) value = parser(value);
  return value !== undefined
    ? value
    : templateDefault !== undefined
    ? templateDefault
    : defaultValue;
};

const setDamage = (value: any, defaultValue?: Damage) =>
  value === undefined
    ? defaultValue || undefined
    : !!value
    ? parseDamage(value, defaultValue)
    : undefined;

const meleeOrRanged = (p: WeaponTemplate) =>
  p.weaponType === WeaponType.Melee ? `Melee${p.thrown ? ' or Ranged' : ''}` : 'Ranged';

const attackBonus = (m: Monster, p: WeaponTemplate) => signed(calculateWeaponAttackBonus(m, p));

const reachText = (p: WeaponTemplate) => `reach ${p.reach || '5'} ft.`;
const rangeText = (prefix: string, r?: Range) => (r ? `${prefix}range ${r[0]}/${r[1]} ft.` : '');

const reachOrRange = (p: WeaponTemplate) => {
  const range = p.thrown || p.range;
  return p.weaponType === WeaponType.Melee
    ? `${reachText(p)}${rangeText(' or ', range)}`
    : rangeText('', range);
};

const renderBaseDamage = (amount?: Dice, type?: DamageType, ranged?: Damage) => {
  if (amount === undefined || type === undefined) return '';
  const typeText = ranged ? ' in melee' : '';
  return renderDamage({ amount, type }) + typeText;
};

const renderAllDamage = (m: Monster, p: WeaponTemplate) => {
  const base = calculatePrimaryDamageDice(m, p, p.baseDamage);
  const versatile = calculatePrimaryDamageDice(m, p, p.versatileDamage);
  const ranged = calculatePrimaryDamageDice(m, p, p.rangedDamage);

  const main = [
    p.baseDamage && renderBaseDamage(base, p.baseDamage.type, p.rangedDamage),
    p.versatileDamage && versatile
      ? `${renderDamage({
          amount: versatile,
          type: p.versatileDamage.type,
        })} if used with two hands`
      : '',
    p.rangedDamage && ranged
      ? `${renderDamage({
          amount: ranged,
          type: p.rangedDamage.type,
        })} at range`
      : '',
  ]
    .filter((x) => x)
    .join(', or ');

  const ds = p.extraDamage
    .map((d) => `${renderDamage(d)}`)
    .filter((x) => x)
    .join(', plus ');

  const all = [main, ds].filter((x) => x).join(', plus ');
  return all ? all + '.' : '';
};

export const renderWeapon = (m: Monster, p: WeaponTemplate) =>
  `*${meleeOrRanged(p)} ${p.attackType} Attack:* ${attackBonus(m, p)} to hit, 
    ${reachOrRange(p)}, ${p.targets || 'one target'}.
    *Hit:*\u00A0${renderAllDamage(m, p)}${applyRender(p.extraDesc, m, p)}`;

export const weaponAttack: TraitTemplate<WeaponTemplate> = {
  name: (m: Monster, p: WeaponTemplate) => p.weapon,
  type: TraitType.Action,
  parseProps: (s: any) => {
    const w =
      lookup(weaponsTable, (s.weapon || '').toString().toLowerCase()) || ({} as WeaponTemplate);
    const attackType = parseAttackType(s.attackType) || w.attackType || AttackType.Weapon;

    return {
      weapon: w.weapon || s.weapon || '<.weapon>',
      weaponType: parseWeaponType(s.weaponType) || w.weaponType || WeaponType.Melee,
      attackType,
      baseDamage: setDamage(s.baseDamage, w.baseDamage),
      versatileDamage: setDamage(s.versatileDamage, w.versatileDamage),
      omitDamageModifier: setValue(
        s.omitDamageModifier,
        w.omitDamageModifier,
        attackType === AttackType.Spell,
      ),
      extraDamage: setValue(s.extraDamage, w.extraDamage, [], (e) => e.map(parseDamage)),
      adjustForSize: w.adjustForSize === undefined ? !!s.adjustForSize : w.adjustForSize,
      bonus: setValue(s.bonus, w.bonus, undefined, parseInt),
      reach: setValue(s.reach, w.reach, 5, parseInt),
      thrown: setValue(s.thrown, w.thrown, undefined),
      range: setValue(s.range, w.range, undefined),
      finesse: setValue(s.finesse, w.finesse, true),
      ability: setValue(s.ability, w.ability, undefined),
      targets: setValue(s.targets, w.targets, 'one target'),
      extraDesc: setValue(s.extraDesc, w.extraDesc, undefined),
    };
  },
  render: renderWeapon,
  attackBonus: calculateWeaponAttackBonus,
  dpr: calculateWeaponDPR,
};
