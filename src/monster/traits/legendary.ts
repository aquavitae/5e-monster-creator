import { TraitTemplate, TraitType } from './types';
import { parseTrait } from './parser';
import { Monster } from '../types';
import { sentence } from './util';
import { getAttack } from '../utils';
import { Trait } from './trait';

export interface LegendaryActions {
  number: number;
  actions: Trait[];
}

type LAReferenceTraitProps = { action: string };
const laReferenceTemplate: TraitTemplate<LAReferenceTraitProps> = {
  name: (m: Monster, p: LAReferenceTraitProps) =>
    getAttack(m, p.action, (a) => a.renderName(m)) || 'missing',
  type: TraitType.Action,
  parseProps: (p: any) => ({ action: p.action || '' }),
  attackBonus: (m: Monster, p: LAReferenceTraitProps) =>
    getAttack(m, p.action, (a) => a.calcAB(m)) || 0,
  dpr: (m: Monster, p: LAReferenceTraitProps) => getAttack(m, p.action, (a) => a.calcDPR(m)) || 0,
  saveDC: (m: Monster, p: LAReferenceTraitProps) => getAttack(m, p.action, (a) => a.calcDC(m)) || 0,
  render: (m: Monster, p: LAReferenceTraitProps) =>
    `${sentence(m.theName)} makes a ${p.action} attack.`,
};

const parseAsAction = (s: any): Trait =>
  new Trait(laReferenceTemplate, { ...s, canonicalName: s.name || s.action });

const parseLegendaryAction = (s: any): Trait | undefined => {
  if (!s || typeof s !== 'object') return undefined;
  return 'action' in s ? parseAsAction(s) : parseTrait(s);
};

export const parseLegendaryActions = (s: any) => {
  if (!s || typeof s !== 'object') return undefined;
  return {
    number: parseInt(s.number) || 3,
    actions:
      s.actions && Array.isArray(s.actions)
        ? s.actions.map(parseLegendaryAction).filter((x: any) => x)
        : [],
  };
};
