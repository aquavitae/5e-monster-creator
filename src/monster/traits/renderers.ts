import { Damage } from './damage';
import { addDice, Dice } from '../dice';

export const renderDamageDice = (d: Dice) => `${d.average}\u00A0(${d.text})`;

export const renderDamage = (d: Damage, bonus?: number) =>
  `${renderDamageDice(bonus ? addDice(d.amount, bonus) : d.amount)} ${d.type} damage`;
