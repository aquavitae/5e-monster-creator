import { ChargeProps, traitsTable } from './traits';
import { applyRender } from './util';
import { parseDice } from '../dice';
import { DamageType } from './damage';
import { sampleMonster } from './monster-base.test';

const clean = (t: string) => t.replace(/\n\s*/gm, ' ');

describe('Charge', () => {
  it('renders correctly', () => {
    const props: ChargeProps = {
      range: 20,
      attackName: 'tusk',
      damage: { amount: parseDice('1d6'), type: DamageType.Slashing },
    };
    const s = applyRender(traitsTable.charge.render, sampleMonster, props);
    expect(clean(s.toString())).toEqual(
      clean(
        `If the monster moves at least 20 feet straight toward a
          target and then hits it with a tusk attack on the same turn, the
          target takes an extra 3 (1d6) slashing damage. If the target is
          a creature, it must succeed on a DC 12 Strength
          saving throw or be knocked prone`,
      ),
    );
  });
});
