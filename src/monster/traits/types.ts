import { Monster } from '../types';
import { AttackType, WeaponType } from './weapon-type';
import { Damage } from './damage';

export enum TraitType {
  Ability = 'Ability',
  Action = 'Action',
  Reaction = 'Reaction',
}

export const parseTraitType = (s: any): TraitType => {
  switch (s?.toLowerCase()) {
    default:
    case 'ability':
      return TraitType.Ability;
    case 'action':
      return TraitType.Action;
    case 'reaction':
      return TraitType.Reaction;
  }
};

export type Range = [number, number];

export type AdjustCRStats<T> =
  | ((m: Monster, s: CRStats) => CRStats)
  | ((m: Monster, s: CRStats, p: T) => CRStats);

export type ExplainCRStats<T> =
  | ((m: Monster) => string)
  | ((m: Monster, p: T) => string)
  | (() => string);

export type AdjustMonster<T> = ((m: Monster) => Monster) | ((m: Monster, p: T) => Monster);

export type Render<T> = ((m: Monster) => string) | ((m: Monster, p: T) => string) | string;

export type Calculator<T> = ((m: Monster, p: T) => number) | number;

export interface WeaponTemplate {
  weapon: string;
  weaponType: WeaponType;
  attackType: AttackType;

  // damage is complicated. We have the regular base damage, that may increase
  // with creature size, and will have proficiency and ability modifiers.
  // Then there is versatile damage, which is basically the same, but can be
  // used as an alternative. And then there are additional effects which can increase
  // the damage (e.g. angelic weapons), or the attack and damage (e.g. +1 weapons).
  // On top of that, some monster-specific "weapons" break some or all of those rules
  // (e.g. spell-like abilities such as life drain). To deal with this, we apply
  // all bonuses to baseDamage and versatileDamage, and none of them to extraDamage.
  // bonus and ability can be used to do some fine tuning.
  baseDamage?: Damage;
  versatileDamage?: Damage;
  omitDamageModifier?: boolean;
  rangedDamage?: Damage;
  extraDamage: Damage[];
  adjustForSize?: boolean;
  bonus?: number; // e.g. a +1 weapon
  ability?: string;

  reach?: number;
  thrown?: Range;
  range?: Range;
  finesse: boolean;
  targets?: string;
  extraDesc?: Render<{}>;
}

export type CRStats = {
  hp: number;
  ac: number;
  dpr: number;
  ab: number;
  dc: number;
};

export type CRStatChange = {
  trait: string;
  explain: string;
  diff: CRStats;
};

export interface TraitTemplate<T extends object = {}> {
  name?: Render<T>;
  canonicalName?: string;
  type?: TraitType;
  render?: Render<T>;
  parseProps?: (p: any) => T;
  adjustCRStats?: AdjustCRStats<T>;
  explainCRStats?: ExplainCRStats<T>;
  adjustMonster?: AdjustMonster<T>;
  attackBonus?: Calculator<T>;
  saveDC?: Calculator<T>;
  dpr?: Calculator<T>;
}
