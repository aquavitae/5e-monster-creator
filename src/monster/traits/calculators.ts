import { WeaponTemplate } from './types';
import { lookup } from './util';
import { Monster } from '../types';
import { modifier } from '../utils';
import { addDice, Dice, increaseDieCount } from '../dice';
import { Size } from '../size';
import { Damage } from './damage';
import { Ability } from '../ability';
import { Skill } from '../skills';

export const resolveAbility = (m: Monster, a?: string) =>
  lookup(m.scores, !a || a.length < 3 ? null : a.toLowerCase().slice(0, 3)) || 10;

export const modifierForWeapon = (m: Monster, p: WeaponTemplate) => {
  if (p.ability !== undefined) {
    return modifier(resolveAbility(m, p.ability));
  }

  const dex = modifier(m.scores.dex);
  const str = modifier(m.scores.str);
  return (p.finesse && dex > str) || p.range ? dex : str;
};

export const getBestAttack = (m: Monster) =>
  m.traits.length > 0
    ? m.traits.map((t) => ({ t, dpr: t.calcDPR(m) })).reduce((r, c) => (r && r.dpr > c.dpr ? r : c))
        .t
    : undefined;

export const calculateWeaponAttackBonus = (m: Monster, p: WeaponTemplate) =>
  modifierForWeapon(m, p) + m.cr.proficiency + (p.bonus || 0);

const adjustDamageForSize = (m: Monster, d: Dice) => {
  switch (m.size) {
    case Size.Large:
      return increaseDieCount(d, 1);
    case Size.Huge:
      return increaseDieCount(d, 2);
    case Size.Gargantuan:
      return increaseDieCount(d, 3);
    default:
      return d;
  }
};

export const calculatePrimaryDamageDice = (m: Monster, p: WeaponTemplate, d?: Damage) => {
  if (!d) {
    return undefined;
  }
  const adjusted = p.adjustForSize ? adjustDamageForSize(m, d.amount) : d.amount;
  const mod = p.omitDamageModifier ? 0 : modifierForWeapon(m, p);
  return addDice(adjusted, p.bonus || 0, mod);
};

export const calculateWeaponDPR = (m: Monster, p: WeaponTemplate) =>
  (calculatePrimaryDamageDice(m, p, p.versatileDamage || p.baseDamage)?.average || 0) +
    p.extraDamage.map((d) => d.amount.average).reduce((r, a) => r + a, 0) || 0;

export const calculateDC = (m: Monster, ability: Ability, skill?: Skill) =>
  8 +
  modifier(resolveAbility(m, ability)) +
  (skill && m.expertise.indexOf(skill) >= 0 ? 2 : 1) * m.cr.proficiency;
