import { applyRender } from './util';
import { sampleMonster } from './monster-base.test';

describe('applyRender', () => {
  it('renders a multiline string', () => {
    const s = applyRender(
      `
      |This has
      |  multiple indented
      |lines.
      |
      |With another paragraph.`,
      sampleMonster,
      {},
    );

    expect(s).toEqual(`This has
  multiple indented
lines.

With another paragraph`);
  });
});
