import { parseTraitType, TraitTemplate } from './types';
import { weaponAttack } from './weapon';
import { traitsTable } from './traits';
import { lookup } from './util';
import { multiattack } from './multiattack';
import { Trait } from './trait';

export const parseTraits = (st: any): Trait[] =>
  st && Array.isArray(st) ? (st.map(parseTrait) as Trait[]) : [];

export const parseTrait = <T extends object>(s: any): Trait => {
  const tmpl = findTemplate(s);
  if (tmpl === undefined) {
    return new Trait();
  }
  return new Trait(tmpl, s);
};

export const findTemplate = <T>(s: any) => {
  switch (true) {
    case typeof s != 'object' || s === null:
      return undefined;
    case 'weapon' in s:
      return { ...weaponAttack, canonicalName: (s || '').weapon.toString() };
    case 'trait' in s:
      return findTraitTemplate(s.trait || '');
    case 'multiattack' in s:
      return multiattack;
    default:
      return buildGenericTraitTemplate(s);
  }
};

const findTraitTemplate = <T extends object>(name: string): TraitTemplate<T> => {
  return (lookup(traitsTable, (name || '').toLowerCase()) as TraitTemplate<any>) || undefined;
};

export const buildGenericTraitTemplate = (s: any) => ({ type: parseTraitType(s?.type) });
