import { TraitTemplate, TraitType } from './types';
import { possessive, sentence } from './util';
import { Monster } from '../types';
import { getAttack as utilGetAttack } from '../utils';
import { Trait } from './trait';

export type MultiattackProps = { multiattack: { [key: string]: number }[] };

const getAttack = <T>(m: Monster, n: string, c: (t: Trait) => T) =>
  n === 'multiattack' ? undefined : utilGetAttack(m, n, c);

const numberToString = (n: number) =>
  n < 0 || n >= 10
    ? n.toString()
    : ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten'][n - 1];

const sentenceJoin = (parts: string[], sep: string, finalSep: string) => {
  if (parts.length === 0) {
    return '';
  }
  if (parts.length === 1) {
    return parts[0];
  }
  const last = parts.pop();
  return `${parts.join(sep)}${finalSep}${last}`;
};

const renderEffectiveAttack = (m: Monster, name: string, num: number) =>
  `${numberToString(num)} attack${num >= 2 ? 's' : ''} with ${possessive(m.pronoun)} ${name}`;

const renderMultiattack = (m: Monster, p: MultiattackProps) => {
  const attacks = p.multiattack.map((attacks: { [key: string]: number }) =>
    sentenceJoin(
      Object.keys(attacks).map((k) => renderEffectiveAttack(m, k, attacks[k])),
      ', ',
      ' and ',
    ),
  );
  return `${sentence(m.theName)} makes ${sentenceJoin(attacks, ', ', ' or ')}`;
};

const parseMultiattacks = (s: any) =>
  s && Array.isArray(s) ? s.filter((e) => e && typeof e === 'object') : [];

export const multiattack: TraitTemplate<MultiattackProps> = {
  name: 'Multiattack',
  type: TraitType.Action,
  parseProps: (s: any) => ({ multiattack: parseMultiattacks(s.multiattack) }),
  attackBonus: (m, p) =>
    Math.max(
      ...p.multiattack.flatMap((attacks) =>
        Object.keys(attacks).map((a) => getAttack(m, a, (t) => t.calcAB(m)) || 0),
      ),
    ),

  dpr: (m, p) =>
    p.multiattack
      .map((attacks) =>
        Object.keys(attacks)
          .map((a) => (getAttack(m, a, (t) => t.calcDPR(m)) || 0) * attacks[a])
          .reduce((r, v) => r + v, 0),
      )
      .reduce((r, v) => (r > v ? r : v), 0),

  render: renderMultiattack,
};
