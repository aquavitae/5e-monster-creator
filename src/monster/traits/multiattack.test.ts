import { multiattack } from './multiattack';
import { sampleMonster } from './monster-base.test';
import { parseTraits } from './parser';
import { Trait } from './trait';
import { Monster } from '../types';

describe('multiattack', () => {
  const monster: Monster = { ...sampleMonster, traits: parseTraits([{ weapon: 'shortsword' }]) };

  test('parses multiattack correctly', () => {
    const ma = (multiattack.parseProps || fail)({ multiattack: [{ Shortsword: 2 }] });
    expect(ma).toEqual({ multiattack: [{ Shortsword: 2 }] });
  });

  test('renders a single effect attack', () => {
    const ma = new Trait(multiattack, { multiattack: [{ Shortsword: 2 }] });

    expect(ma.renderDesc(monster)).toEqual('The monster makes two attacks with its Shortsword.');
  });
});
