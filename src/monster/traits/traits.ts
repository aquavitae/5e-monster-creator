import { Dice, makeDice, parseDice } from '../dice';
import { CRStats, TraitTemplate, TraitType, WeaponTemplate } from './types';
import { possessive, sentence } from './util';
import { Monster } from '../types';
import { getAttack, modifier } from '../utils';
import { AttackType, WeaponType } from './weapon-type';
import { Damage, DamageType, parseDamage } from './damage';
import {
  calculateDC,
  calculateWeaponAttackBonus,
  calculateWeaponDPR,
  getBestAttack,
} from './calculators';
import { renderDamage, renderDamageDice } from './renderers';
import { innateSpellcasting } from './spellcasting/innate-spellcasting';
import { spellcasting } from './spellcasting/spellcasting';
import { Ability } from '../ability';
import { renderWeapon } from './weapon';
import { Skill } from '../skills';

const adjustWeaponDamage = (m: Monster, fn: (d: Damage[]) => Damage[]): Monster => {
  m.traits.forEach((t) => {
    const props = t.props as WeaponTemplate;
    t.props =
      props.weaponType === WeaponType.Melee
        ? (t.props = { ...props, extraDamage: fn(props.extraDamage || []) })
        : props;
  });
  return m;
};

const aggressive: TraitTemplate = {
  name: 'Aggressive',
  type: TraitType.Ability,
  adjustCRStats: (m: Monster, s: CRStats) => ({ ...s, dpr: s.dpr + 2 }),
  render: (m: Monster) =>
    `As a bonus action, ${m.theName} can move up to its speed toward a
    hostile creature that it can see.`,
};

const ambusher: TraitTemplate = {
  name: 'Ambusher',
  type: TraitType.Ability,
  adjustCRStats: (m: Monster, s: CRStats) => ({ ...s, ab: s.ab + 1 }),
  render: (m: Monster) =>
    `${sentence(m.theName)} has advantage on attack rolls against
    any creature ${m.pronoun} has surprised.`,
};

const amorphous: TraitTemplate = {
  name: 'Amorphous',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} can move through a space as narrow as 1 inch
    wide without squeezing`,
};

const amphibious: TraitTemplate = {
  name: 'Amphibious',
  type: TraitType.Ability,
  render: (m: Monster) => `${sentence(m.theName)} can breathe air and water.`,
};

// e.g. Deva
type AngelicWeaponProps = { damage: Damage };
const angelicWeapons: TraitTemplate<AngelicWeaponProps> = {
  name: 'Angelic Weapons',
  type: TraitType.Ability,
  parseProps: (p: any) => ({ damage: parseDamage(p.damage) }),
  adjustMonster: (m: Monster, p: AngelicWeaponProps) =>
    adjustWeaponDamage(m, (d) => [...d, p.damage]),
  render: (m: Monster, p: AngelicWeaponProps) =>
    `${possessive(sentence(m.theName))} weapon attacks are magical.
    When ${m.theName} hits with any weapon, the weapon deals an
    extra ${renderDamage(p.damage)} damage (included in the attack)`,
};

const antimagicSusceptibility: TraitTemplate = {
  name: 'Antimagic Susceptibility',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} is incapacitated while in the area of an
    antimagic field. If targeted by dispel magic, the sword must succeed on a
    Constitution saving throw against the caster's spell save DC or fall
    unconscious for 1 minute.`,
};

const avoidance: TraitTemplate = {
  name: 'Avoidance',
  type: TraitType.Ability,
  adjustCRStats: (m: Monster, s: CRStats) => ({ ...s, ac: s.ac + 1 }),
  render: (m: Monster) =>
    `If ${m.theName} is subjected to an effect that allows it to make a saving
    throw to take only half damage, it instead takes no damage if it succeeds
    on the saving throw, and only half damage if it fails.`,
};

const blindSenses: TraitTemplate = {
  name: 'Blind Senses',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} can't use its blindsight while deafened and unable
    to smell.`,
};

const bloodFrenzy: TraitTemplate = {
  name: 'Blood Frenzy',
  type: TraitType.Ability,
  adjustCRStats: (m: Monster, s: CRStats) => ({ ...s, ab: s.ab + 4 }),
  render: (m: Monster) =>
    `${sentence(m.theName)} has advantage on melee attack rolls
    against any create that doesn't have all its hit points.`,
};

type BreathWeaponProps = { damage: Damage; width: number; length: number };
const breathWeapon: TraitTemplate<BreathWeaponProps> = {
  canonicalName: 'Breath Weapon',
  name: (m: Monster, p: BreathWeaponProps) => `${p.damage.type} Breath (recharge 5-6)`,
  type: TraitType.Action,
  parseProps: (p: any) => ({
    damage: parseDamage(p.damage),
    width: parseInt(p.width) || 5,
    length: parseInt(p.length) || 20,
  }),
  // DMG is not clear on this, so we'll assume that it only gets used once per
  // encounter (an encounter lasting 3 rounds), effectively decreasing the DPR
  // to 1/3 * 2 targets = 2/3
  dpr: (m: Monster, p: BreathWeaponProps) => (p.damage.amount.average * 2) / 3,
  saveDC: (m: Monster) => calculateDC(m, Ability.Dex),
  render: (m: Monster, p: BreathWeaponProps) =>
    `${sentence(m.theName)} exhales ${p.damage.type} in a ${p.length}-foot
    line that is ${p.width} feet wide. Each creature in that line must make
    a DC\u00A0${calculateDC(m, Ability.Con)} Dexterity saving throw, taking
    ${renderDamage(p.damage)} damage on a failed save, or half as much
    damage on a successful one.`,
};

const addOneDie = (d: Dice) =>
  makeDice({ dice: d.dice.map((die) => ({ n: die.n + 1, d: die.d })), mod: d.mod });

const addBruteWeaponDamage = (w: WeaponTemplate) =>
  'weaponType' in w && w.weaponType === WeaponType.Melee && w.baseDamage
    ? {
        ...w,
        rangedDamage: w.baseDamage,
        baseDamage: { amount: addOneDie(w.baseDamage.amount), type: w.baseDamage.type },
      }
    : w;

const brute: TraitTemplate = {
  name: 'Brute',
  type: TraitType.Ability,
  adjustMonster: (m: Monster) => {
    m.traits.forEach((t) => {
      t.props = addBruteWeaponDamage(t.props);
    });
    return m;
  },
  render: (m: Monster) =>
    `A melee weapon deals one extra die of its damage when ${m.theName}
    hits with it (included in the attack).`,
};

const chameleonSkin: TraitTemplate = {
  name: 'Chameleon Skin',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} has advantage on Dexterity (Stealth) checks
    made to hide.`,
};

const changeShape: TraitTemplate = {
  name: 'Change Shape',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} magically polymorphs into a
    humanoid or beast that has a challenge rating no higher than
    its own, or back into its true form. It reverts to its true form if
    it dies. Any equipment it is wearing or carrying is absorbed or
    borne by the new form (${possessive(m.theName)} choice).

    In a new form, ${m.theName} retains its alignment, hit points,
    Hit Dice, ability to speak, proficiencies, Legendary Resistance,
    lair actions, and Intelligence, Wisdom, and Charisma scores, as
    well as this action. Its statistics and capabilities are otherwise
    replaced by those of the new form, except any class features or
    legendary actions of that form.`,
};

export type ChargeProps = {
  range: number;
  damage: Damage;
  attackName: string;
};
const charge: TraitTemplate<ChargeProps> = {
  name: 'Charge',
  type: TraitType.Ability,
  parseProps: (p: any) => ({
    range: parseInt(p.range) || 20,
    damage: parseDamage(p.damage),
    attackName: p.attackName,
  }),
  // Technically this damage should be added to one attack, but since it is highly likely
  // that the one attack will be used in a round, just add it to the DPR.
  adjustCRStats: (m: Monster, s: CRStats, p: ChargeProps) => ({
    ...s,
    dpr: s.dpr + p.damage.amount.average,
  }),
  render: (m, p) =>
    `If ${m.theName} moves at least ${p.range} feet straight toward a
    target and then hits it with a ${p.attackName} attack on the same turn, the
    target takes an extra ${renderDamage(p.damage)}. If the target is
    a creature, it must succeed on a DC ${calculateDC(m, Ability.Str)} Strength
    saving throw or be knocked prone.`,
};

const charm: TraitTemplate = {
  name: 'Charm',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} targets one humanoid it can see within 30 feet
    of it. If the target can see ${m.theName}, the target must succeed on a
    ${calculateDC(m, Ability.Cha)} Charisma saving throw against this magic
    or be charmed by ${m.theName}. The charmed target regards
    ${m.theName} as a trusted friend to be heeded and protected.
    Although the target isn't under ${possessive(m.theName)} control, it takes
    ${possessive(m.theName)} requests or actions in the most favorable way it
    can, and it is a willing target for ${possessive(m.theName)} bite attack.

    Each time ${m.theName} or ${possessive(m.theName)} companions do
    anything harmful to the target, it can repeat the saving throw,
    ending the effect on itself on a success. Otherwise, the
    effect lasts 24 hours or until ${m.theName} is destroyed, is on a
    different plane of existence than the target, or takes a bonus
    action to end the effect.`,
};

type ConstrictProps = { damage: Dice };
const constrictTemplate = (p: ConstrictProps) => ({
  weapon: 'Constrict',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  baseDamage: {
    type: DamageType.Bludgeoning,
    amount: p.damage,
  },
  omitDamageModifier: false,
  extraDamage: [],
  finesse: false,
  targets: 'one creature',
  extraDesc: (m: Monster) =>
    `and the target is grappled (escape DC ${calculateDC(m, Ability.Str)}).
    Until this grapple ends, the creature is restrained, and ${m.theName}
    can't constrict another target.`,
});

const constrict: TraitTemplate<ConstrictProps> = {
  name: 'Constrict',
  type: TraitType.Action,
  parseProps: (p: any) => ({ damage: parseDice(p.damage || '1d8') }),
  adjustCRStats: (m: Monster, s: CRStats) => ({ ...s, ac: s.ac + 1 }),
  attackBonus: (m: Monster, p: ConstrictProps) =>
    calculateWeaponAttackBonus(m, constrictTemplate(p)),
  dpr: (m: Monster, p: ConstrictProps) => calculateWeaponDPR(m, constrictTemplate(p)),
  render: (m: Monster, p: ConstrictProps) => renderWeapon(m, constrictTemplate(p)),
};

type DamageAbsorptionProps = { damageType: DamageType };
const damageAbsorption: TraitTemplate<DamageAbsorptionProps> = {
  canonicalName: 'Damage Absorption',
  name: (m: Monster, p: DamageAbsorptionProps) => `${p.damageType} Absorption`,
  type: TraitType.Ability,
  render: (m: Monster, p: DamageAbsorptionProps) =>
    `Whenever ${m.theName} is subjected to ${p.damageType} damage, it takes
    no damage and instead regains a number of hit points equal to the
    ${p.damageType} damage dealt.`,
};

const damageTransfer: TraitTemplate = {
  name: 'Damage Transfer',
  type: TraitType.Ability,
  adjustCRStats: (m: Monster, s: CRStats) => ({
    ...s,
    hp: s.hp * 2,
    dpr: s.dpr + s.hp / 3,
  }),
  render: (m: Monster) =>
    `While attached to a creature, ${m.theName} takes only half the damage
    dealt to it (rounded down). and that creature takes the other half.`,
};

// e.g. Magmin
type DeathBurstProps = { damage: Damage };
const deathBurst: TraitTemplate<DeathBurstProps> = {
  name: 'Death Burst',
  type: TraitType.Ability,
  parseProps: (p: any) => ({ damage: parseDamage(p.damage) }),
  // This damage should be added for two creatures, one round only. If we assume
  // 3 rounds of combat, that means multiplying by 2/3
  adjustCRStats: (m: Monster, s: CRStats, p: DeathBurstProps) => ({
    ...s,
    dpr: s.dpr + (p.damage.amount.average * 2) / 3,
  }),
  render: (m, p) =>
    `When ${m.theName} dies, it explodes in a burst of
    fire and magma. Each creature within 10 feet of it must make
    a DC ${calculateDC(m, Ability.Con)} Dexterity saving throw,
    taking ${renderDamage(p.damage)} on a failed save, or half as much
    damage on a successful one. Flammable objects that aren't being worn
    or carried in that area are ignited.`,
};

// e.g. Barbed Devil
const devilSight: TraitTemplate = {
  name: "Devil's Sight",
  type: TraitType.Ability,
  render: (m: Monster) => `Magical darkness doesn't impede ${possessive(m.theName)} darkvision.`,
};

// e.g. Aarakocra
type DiveProps = { damage: Dice };
const dive: TraitTemplate<DiveProps> = {
  name: 'Dive Attack',
  type: TraitType.Ability,
  parseProps: (p: any) => ({ damage: parseDice(p.damage) }),
  adjustCRStats: (m: Monster, s: CRStats, p: DiveProps) => ({
    ...s,
    dpr: s.dpr + p.damage.average / 3,
  }),
  render: (m, p) =>
    `If ${m.theName} is flying and dives at least 30 feet
    straight toward a target and then hits it with a melee weapon
    attack, the attack deals an extra ${renderDamageDice(p.damage)}
    damage to the target.`,
};

// e.g. Hook Horror
const echolocation: TraitTemplate = {
  name: 'Echolocation',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} can't use ${possessive(m.pronoun)} blindsight while deafened`,
};

// e.g. Azer
type ElementalBodyProps = { damage: Damage };
const elementalBody: TraitTemplate<ElementalBodyProps> = {
  name: 'Elemental Body',
  type: TraitType.Ability,
  parseProps: (p: any) => ({ damage: parseDamage(p.damage) }),
  adjustCRStats: (m: Monster, s: CRStats, p: ElementalBodyProps) => ({
    ...s,
    dpr: s.dpr + p.damage.amount.average,
  }),
  render: (m, p) =>
    `A creature that touches ${m.theName} or hits it with a
    melee attack while within 5 feet of it takes ${renderDamage(p.damage)}.`,
};

// e.g. Duergar
const enlarge: TraitTemplate = {
  type: TraitType.Action,
  name: 'Enlarge (Recharges after a Short or Long Rest)',
  parseProps: (p: any) => ({ damage: parseDice(p.damage) }),
  adjustCRStats: (m: Monster, s: CRStats) => {
    const attack = getBestAttack(m);
    if (attack === undefined) return s;
    const props = (attack.props as WeaponTemplate) || null;
    if (props === null || props.baseDamage === undefined) return s;
    const d = props.baseDamage.amount;
    return { ...s, dpr: s.dpr + (d.average - d.mod) };
  },
  render: (m: Monster) =>
    `For 1 minute, ${m.theName} magically increases in size,
    along with anything ${m.pronoun} is wearing or carrying. While enlarged,
    ${m.theName} is Large, doubles ${possessive(m.pronoun)} damage dice on
    Strength-based weapon attacks, and makes Strength checks and
    Strength saving throws with advantage. If ${m.theName} lacks the
    room to become Large, {m.pronoun} attains the maximum size possible in
    the space available.`,
};

// e.g. Night Hag
const etherealness: TraitTemplate = {
  name: 'Etherealness',
  type: TraitType.Action,
  render: (m: Monster) =>
    `${sentence(m.theName)} magically enters the Ethereal Plane from
    the Material Plane, or vice versa.`,
};

type FalseAppearanceProps = { normal: string };
const falseAppearance: TraitTemplate<FalseAppearanceProps> = {
  name: 'False Appearance',
  type: TraitType.Ability,
  parseProps: (p: any) => ({ normal: p.normal }),
  render: (m: Monster, p: FalseAppearanceProps) =>
    `While ${m.theName} remains motionless, ${m.pronoun} is
     indistinguishable from a normal ${p.normal}.`,
};

// e.g. Drow
const feyAncestry: TraitTemplate = {
  name: 'Fey Ancestry',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} has advantage on saving throws against
    being charmed, and magic can't put ${m.theName} to sleep.`,
};

const fiendishBlessing: TraitTemplate = {
  name: 'Fiendish Blessing',
  type: TraitType.Ability,
  adjustMonster: (m: Monster) => ({
    ...m,
    traitArmor: [
      ...m.armor,
      {
        name: '',
        baseAC: modifier(m.scores.cha),
        maxAddDex: 0,
        isAdditional: true,
      },
    ],
  }),
  render: (m: Monster) => 
    `The AC of ${m.theName} includes ${possessive(m.pronoun)} Charisma bonus.`,
};

const flyby: TraitTemplate = {
  name: 'Flyby',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} doesn't provoke opportunity attacks when ${
      m.pronoun
    } flies out of an enemy's reach.`,
};

const frightfulPresence: TraitTemplate = {
  name: 'Frightful Presence',
  type: TraitType.Action,
  adjustCRStats: (m: Monster, s: CRStats) => ({
    ...s,
    hp: s.hp * (m.cr.value <= 10 ? 1.25 : 1),
  }),
  render: (m: Monster) =>
    `Each creature of ${possessive(m.theName)} choice that
    is within 120 feet of ${m.theName} and aware of it must succeed
    on a DC ${calculateDC(m, Ability.Cha)} Wisdom saving throw or become frightened for 1
    minute. A creature can repeat the saving throw at the end of
    each of its turns, ending the effect on itself on a success. If a
    creature's saving throw is successful or the effect ends for it,
    the creature is immune to ${possessive(m.theName)} Frightful Presence for
    the next 24 hours.`,
};

const grappler: TraitTemplate = {
  name: 'Grappler',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} has advantage on attack rolls against any
    creature grappled by ${m.pronoun}`,
};

const holdBreath: TraitTemplate = {
  name: 'Hold Breath',
  type: TraitType.Ability,
  render: (m: Monster) => `${sentence(m.theName)} can hold its breath for 15 minutes.`,
};

const horrifyingVisage: TraitTemplate = {
  name: 'Horrifying Visage',
  type: TraitType.Action,
  adjustCRStats: (m: Monster, s: CRStats) => ({
    ...s,
    hp: s.hp * (m.cr.value <= 10 ? 1.25 : 1),
  }),
  render: (m: Monster) =>
    `Each non-undead creature within 60 feet of ${m.theName}
    that can see ${m.pronoun} must succeed on a DC ${calculateDC(m, Ability.Cha)} Wis
    saving throw or be frightened for 1 minute. A frightened target
    can repeat the saving throw at the end of each of its turns,
    with disadvantage if ${m.theName} is within line of sight, ending
    the effect on itself on a success. If a target's saving throw is
    successful or the effect ends for it, the target is immune to
    ${possessive(m.theName)} Horrifying Visage for the next 24 hours.`,
};

const illumination: TraitTemplate = {
  name: 'Illumination',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} sheds bright light in a 30 ft radius and dim light
    in an additional 30 feet.`,
};

type IllusoryAppearanceProps = { dc?: number };
const illusoryAppearance: TraitTemplate<IllusoryAppearanceProps> = {
  name: 'Illusory Appearance',
  type: TraitType.Action,
  parseProps: (p: any) => ({ dc: parseInt(p.dc) || undefined }),
  render: (m: Monster, p: IllusoryAppearanceProps) => {
    const her = possessive(m.pronoun);
    return `${sentence(m.theName)} covers ${her}elf and anything
      ${m.pronoun} is wearing or carrying with a magical illusion that makes
${m.pronoun} is wearing or carrying with a magical illusion that makes
      ${m.pronoun} is wearing or carrying with a magical illusion that makes
      ${her} look like another creature of ${her} general size and humanoid shape.
      The illusion ends if ${m.theName} takes a bonus action to end it or
      if ${m.pronoun} dies.

      The changes wrought by this effect fail to hold up to physical
      inspection. For example, ${m.theName} could appear to have smooth
      skin, but someone touching ${her} would feel ${her} rough flesh.
      Otherwise, a creature must take an action to visually inspect
      the illusion and succeed on a DC ${p.dc || calculateDC(m, Ability.Int)}
      Intelligence (Investigation) check to discern that ${m.theName} is disguised.`;
  },
};

// e.g. Iron Golem
const immutableForm: TraitTemplate = {
  name: 'Immutable Form',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} is immune to any spell or effect that would alter 
    ${possessive(m.pronoun)} form.`,
};

const incorporealMovement: TraitTemplate = {
  name: 'Incorporeal Movement',
  type: TraitType.Ability,
  render: (m: Monster) => `${sentence(m.theName)} can move through other
    creatures and objects as if they were difficult terrain. ${sentence(m.pronoun)}
    takes 5 (1d10) force damage if ${m.pronoun} ends ${possessive(m.pronoun)}
    turn inside an object.`,
};

const inscrutable: TraitTemplate = {
  name: 'Inscrutable',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} is immune to any effect that would sense
    ${possessive(m.pronoun)} emotions or read ${possessive(m.pronoun)}
    thoughts, as well as any divination spell that ${m.pronoun} refuses.
    Wisdom (Insight) checks made to ascertain ${possessive(m.theName)}
    intentions or sincerity have disadvantage.`,
};

const invisibility: TraitTemplate = {
  name: 'Invisibility',
  type: TraitType.Action,
  render: (m: Monster) =>
    `${sentence(m.theName)} magically turns invisible until ${m.pronoun} attacks or
    until ${possessive(m.pronoun)} concentration ends (as if concentrating on a spell). Any
    equipment ${m.theName} wears or carries is invisible with ${m.pronoun}.`,
};

type KeenSensesProps = { senses: string };
const keenSenses: TraitTemplate<KeenSensesProps> = {
  name: 'Keen Senses',
  type: TraitType.Ability,
  parseProps: (p: any) => ({ senses: p.senses ? p.senses : '' }),
  render: (m: Monster, p: KeenSensesProps) =>
    `${sentence(m.theName)} has advantage on Wisdom (Perception) checks that
      rely on ${p.senses}.`,
};

// e.g. Minotaur
const labyrinthineRecall: TraitTemplate = {
  name: 'Labyrinthine Recall',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} can perfectly recall any path ${m.pronoun} has traveled.`,
};

const leadership: TraitTemplate = {
  canonicalName: 'Leadership',
  name: 'Leadership (Recharges after a Short or Long Rest)',
  type: TraitType.Action,
  render: (m: Monster) =>
    `For 1 minute, ${m.theName} can utter a special command or warning
    whenever a nonhostile creature that ${m.pronoun} can see within 30 feet
    of ${m.pronoun} makes an attack roll or a saving throw. The creature can
    add a d4 to its roll provided it can hear and understand ${m.theName}.
    A creature can benefit from only one Leadership die at a ti me. This
    effect ends if ${m.theName} is incapacitated.`,
};

// e.g. Ancient Black Dragon
type LegendaryResistanceProps = { number: number };
const legendaryResistance: TraitTemplate<LegendaryResistanceProps> = {
  canonicalName: 'Legendary Resistance',
  name: (m: Monster, p: LegendaryResistanceProps) => `Legendary Resistance (${p.number}/day)`,
  type: TraitType.Ability,
  parseProps: (p: any) => ({ number: parseInt(p.number) || 3 }),
  adjustCRStats: (m: Monster, s: CRStats, p: LegendaryResistanceProps) => {
    const mul = m.cr.value <= 4 ? 10 : m.cr.value <= 10 ? 20 : 30;
    return { ...s, hp: s.hp + mul * p.number };
  },
  render: (m: Monster) =>
    `If ${m.theName} fails a saving throw, ${m.pronoun} can choose to succeed instead`,
};

type LifeDrainProps = { damage: Dice };
const lifeDrainTemplate = (p: LifeDrainProps) => ({
  weapon: 'Life Drain',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Spell,
  baseDamage: {
    type: DamageType.Necrotic,
    amount: p.damage,
  },
  omitDamageModifier: true,
  extraDamage: [],
  finesse: true,
  targets: 'one creature',
  extraDesc: (m: Monster) =>
    `. The target must succeed on a DC ${calculateDC(m, Ability.Con)}
      Constitution saving throw or its hit point maximum is reduced by an amount equal to the damage
      taken. This reduction lasts until the target finishes a long rest. The target dies if this
      effect reduces its hit point maximum to 0.

      A humanoid slain by this attack rises 24 hours later as a zombie under
      ${possessive(m.name)} control, unless the humanoid is restored to life or its body is
      destroyed. ${sentence(m.name)} can have no more than twelve zombies under its control at one
      time.`,
});

const lifeDrain: TraitTemplate<LifeDrainProps> = {
  name: 'Life Drain',
  type: TraitType.Action,
  parseProps: (p: any) => ({ damage: parseDice(p.damage) }),
  attackBonus: (m: Monster, p: LifeDrainProps) =>
    calculateWeaponAttackBonus(m, lifeDrainTemplate(p)),
  dpr: (m: Monster, p: LifeDrainProps) => calculateWeaponDPR(m, lifeDrainTemplate(p)),
  render: (m: Monster, p: LifeDrainProps) => renderWeapon(m, lifeDrainTemplate(p)),
};

// e.g. Shadow demon
const lightSensitivity: TraitTemplate = {
  name: 'Light Sensitivity',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `While in bright light, ${m.theName} has disadvantage on attack rolls, 
    as well as on Wisdom (Perception) checks that rely on sight.`,
};

//e.g. Balor
const magicResistance: TraitTemplate = {
  name: 'Magic Resistance',
  type: TraitType.Ability,
  adjustCRStats: (m: Monster, s: CRStats) => ({ ...s, ac: s.ac + 2 }),
  render: (m: Monster) =>
    `${sentence(m.theName)} has advantage on saving throws against spells
    and other magical effects.`,
};

const magicWeapons: TraitTemplate = {
  name: 'Magic Weapons',
  type: TraitType.Ability,
  render: (m: Monster) => `${possessive(sentence(m.theName))} weapon attacks are magical.`,
};

// e.g. Hobgoblin
type MartialAdvantageProps = { damage: Dice };
const martialAdvantage: TraitTemplate<MartialAdvantageProps> = {
  name: 'Martial Advantage',
  type: TraitType.Ability,
  parseProps: (p: any) => ({ damage: parseDice(p.damage) }),
  adjustCRStats: (m: Monster, s: CRStats, p: MartialAdvantageProps) => ({
    ...s,
    dpr: s.dpr + p.damage.average,
  }),
  render: (m, p) =>
    `Once per turn, ${m.theName} can deal an extra ${renderDamageDice(p.damage)})
    damage to a creature it hits with a weapon attack if that creature is within 5 feet
    of an ally of ${m.theName} that isn't incapacitated.`,
};

// e.g. Kenku
type MimicryProps = { dc?: number };
const mimicry: TraitTemplate<MimicryProps> = {
  name: 'Mimicry',
  type: TraitType.Ability,
  parseProps: (p: any) => ({ dc: parseInt(p.dc) || undefined }),
  render: (m: Monster, p: MimicryProps) =>
    `${sentence(m.theName)} can mimic animal sounds and humanoid
    voices. A creature that hears the sounds can tell they are
    imitations with a successful DC ${
      p.dc || calculateDC(m, Ability.Cha, Skill.Deception)
    } Wisdom (Insight) check.`,
};

// e.g. Goblin
const nimbleEscape: TraitTemplate = {
  name: 'Nimble Escape',
  type: TraitType.Ability,
  adjustCRStats: (m: Monster, s: CRStats) => ({
    ...s,
    ac: s.ac + 4,
    ab: s.ab + 4,
  }),
  render: (m: Monster) =>
    `${sentence(m.theName)} can take the Disengage or Hide action as a
    bonus action on each of ${possessive(m.pronoun)} turns.`,
};

// e.g. Koa-toa
const otherworldlyPerception: TraitTemplate = {
  name: 'Otherworldly Perception',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} can sense the presence of any creature within 30 feet
    of it that is invisible or on the Ethereal Plane. ${sentence(m.pronoun)} can
    pinpoint such a creature that is moving.`,
};

// e.g. Kobold
const packTactics: TraitTemplate = {
  name: 'Pack Tactics',
  type: TraitType.Ability,
  adjustCRStats: (m: Monster, s: CRStats) => ({ ...s, ab: s.ab + 1 }),
  render: (m: Monster) =>
    `${sentence(m.theName)} has advantage on attack rolls against
    a creature if at least one of ${possessive(m.pronoun)} allies is within
    5 feet of the creature and the ally isn't incapacitated.`,
};

// e.g. Hobgoblin Warlord
type ParryProps = { addAC: number };
const parry: TraitTemplate<ParryProps> = {
  name: 'Parry',
  type: TraitType.Reaction,
  parseProps: (p: any) => ({ addAC: parseInt(p.addAC) || 1 }),
  adjustCRStats: (m: Monster, s: CRStats, p: ParryProps) => ({ ...s, ac: s.ac + p.addAC }),
  render: (m, p) =>
    `${sentence(m.theName)} adds ${p.addAC} to its AC against one melee attack that
    would hit it. To do so, ${m.theName} must see the attacker and be
    wielding a melee weapon.`,
};

// e.g. Ghost
const possession: TraitTemplate = {
  canonicalName: 'Possession',
  name: 'Possession (Recharge 6)',
  type: TraitType.Action,
  adjustCRStats: (m: Monster, s: CRStats) => ({ ...s, hp: s.hp * 2 }),
  render: (m: Monster) =>
    `One humanoid that ${m.theName} can see within 5 feet of it must succeed
    on a DC ${calculateDC(m, Ability.Cha)} Charisma saving throw or be
    possessed by ${m.theName}; ${m.theName} then disappears, and the target
    is incapacitated and loses control of its body. ${sentence(m.theName)}
    now controls the body but doesn't deprive the target of awareness. 
    ${sentence(m.theName)} can't be targeted by any attack, spell, or other
    effect, except ones that turn undead, and ${m.pronoun} retains 
    ${possessive(m.pronoun)} alignment, Intelligence, Wisdom, Charisma,
    and immunity to being charmed and frightened. ${sentence(m.pronoun)}
    otherwise uses the possessed target's statistics, but doesn't gain
    access to the target's knowledge, class features, or proficiencies.
    
    The possession lasts until the body drops to 0 hit points ,
    ${m.theName} ends it as a bonus action, or ${m.theName} is turned or
    forced out by an effect like the dispel evil and good spell. When
    the possession ends, ${m.theName} reappears in an unoccupied
    space within 5 feet of the body. The target is immune to 
    ${possessive(m.theName)} Possession for 24 hours after succeeding on
    the saving throw or after the possession ends.`,
};

// e.g. Tiger
type PounceProps = { attackName: string };
const pounce: TraitTemplate<PounceProps> = {
  name: 'Pounce',
  type: TraitType.Ability,
  parseProps: (p: any) => ({ attackName: p.attackName || 'bite' }),
  adjustCRStats: (m: Monster, s: CRStats, p: PounceProps) => ({
    ...s,
    dpr: s.dpr + (getAttack(m, p.attackName, (a) => a.calcDPR(m)) || 0),
  }),
  render: (m, p) =>
    `If ${m.theName} moves at least 20 feet straight toward a
    creature and then hits it with a claw attack on the same turn,
    that target must succeed on a DC ${calculateDC(m, Ability.Str)}
    Strength saving throw or be knocked prone. If the target is prone,
    ${m.theName} can make one ${p.attackName} attack against it as a bonus action.`,
};

// e.g. Gnoll
type RampageProps = { attackName: string };
const rampage: TraitTemplate<RampageProps> = {
  name: 'Rampage',
  type: TraitType.Ability,
  parseProps: (p: any) => ({ attackName: p.attackName }),
  adjustCRStats: (m: Monster, s: CRStats) => ({
    ...s,
    dpr: s.dpr + 2,
  }),
  render: (m, p) =>
    `When ${m.theName} reduces a creature to 0 hit points
    with a melee attack on its turn, ${m.theName} can take a bonus
    action to move up to half its speed and make a ${p.attackName} attack.`,
};

// e.g. Marilith
const reactive: TraitTemplate = {
  name: 'Reactive',
  type: TraitType.Ability,
  render: (m: Monster) => `${sentence(m.theName)} can take one reaction on every turn in a combat.`,
};

// e.g. Doppelganger
const readThoughts: TraitTemplate = {
  name: 'Read Thoughts',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} magically reads the surface
    thoughts of one creature within 60 feet of ${m.pronoun}. The effect can
    penetrate barriers, but 3 feet of wood or dirt, 2 feet of stone,
    2 inches of metal, or a thin sheet of lead blocks it. While the
    target is in range, ${m.theName} can continue reading its
    thoughts, as long as ${possessive(m.theName)} concentration isn't
    broken (as if concentrating on a spell). While reading the
    target's mind, ${m.theName} has advantage on Wisdom (Insight) and
    Charisma (Deception, Intimidation, and Persuasion) checks against the
    target.`,
};

// Minotaur
const reckless: TraitTemplate = {
  name: 'Reckless',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `At the start of ${possessive(m.pronoun)} turn, ${m.theName} can gain
    advantage on all melee weapon attack rolls ${m.pronoun} makes during
    that turn, but attack rolls against ${m.pronoun} have advantage until the
    start of ${possessive(m.pronoun)} next turn.`,
};

// e.g. Goblin Boss
const redirectAttack: TraitTemplate = {
  name: 'Redirect Attack',
  type: TraitType.Reaction,
  render: (m: Monster) =>
    `When a creature ${m.theName} can see targets ${m.pronoun}
    with an attack, ${m.theName} chooses another ${m.name} within 5
    feet of it. The two ${m.name}s swap places, and the chosen ${m.name}
    becomes the target instead.`,
};

// e.g. Roper
const reel: TraitTemplate = {
  name: 'Reel',
  type: TraitType.Action,
  render: (m: Monster) =>
    `${sentence(m.theName)} pulls each creature grappled by ${m.pronoun}
    up to 25 feet straight toward it.`,
};

// e.g. Troll
type RegenerationProps = { amount: Dice };
const regeneration: TraitTemplate<RegenerationProps> = {
  name: 'Regeneration',
  type: TraitType.Ability,
  parseProps: (p: any) => ({ amount: parseDice(p.amount) }),
  adjustCRStats: (m: Monster, s: CRStats, p: RegenerationProps) => ({
    ...s,
    hp: s.hp + p.amount.average * 3,
  }),
  render: (m, p) =>
    `${sentence(m.theName)} regains ${p.amount} hit points at the start
    of ${possessive(m.pronoun)} turn`,
};

// e.g. Lich
const rejuvenation: TraitTemplate = {
  name: 'Rejuvenation',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `If ${m.pronoun} has a phylactery, a destroyed ${m.name} gains
    a new body in 1d10 days, regaining all its hit points and
    becoming active again. The new body appears within 5 feet of
    the phylactery.`,
};

// e.g. Wereboar
type RelentlessProps = { threshold: number };
const relentless: TraitTemplate<RelentlessProps> = {
  canonicalName: 'Relentless',
  name: 'Relentless (Recharges after a Short or Long Rest)',
  type: TraitType.Ability,
  parseProps: (p: any) => ({ threshold: p.threshold || 7 }),
  adjustCRStats: (m: Monster, s: CRStats, _: RelentlessProps) => {
    let add;
    if (m.cr.value <= 4) add = 7;
    else if (m.cr.value <= 10) add = 14;
    else if (m.cr.value <= 16) add = 21;
    else add = 28;
    return { ...s, hp: s.hp + add };
  },
  render: (m, p) =>
    `If ${m.theName} takes ${p.threshold} damage or less that would
    reduce it to 0 hit points, it is reduced to 1 hit point instead.`,
};

// e.g. Shadow Demon
const shadowStealth: TraitTemplate = {
  name: 'Shadow Stealth',
  type: TraitType.Ability,
  adjustCRStats: (m: Monster, s: CRStats) => ({ ...s, ac: s.ac + 4 }),
  render: (m: Monster) =>
    `While in dim light or darkness, ${m.theName} can take the Hide
    action as a bonus action`,
};

// e.g. Wererat
type ShapechangerProps = {
  trueForm: string;
  hybridForm: string;
  changedForm: string;
};
const shapechanger: TraitTemplate<ShapechangerProps> = {
  name: 'Shapechanger',
  type: TraitType.Ability,
  parseProps: (p: any) => ({
    trueForm: p.trueForm,
    hybridForm: p.hybridForm,
    changedForm: p.changedForm,
  }),
  render: (m, p) =>
    `${sentence(m.theName)} can use its action to polymorph into
    ${p.hybridForm ? p.hybridForm + ' hybrid or into a ' : ''}${p.changedForm}
    or back into its true form, which is ${p.trueForm}. Its statistics,
    other than its size, are the same in each form. Any equipment it is wearing or
    carrying isn't transformed. It reverts to its true form if it dies.`,
};

// e.g. Earth Elemental
const siegeMonster: TraitTemplate = {
  name: 'Siege Monster',
  type: TraitType.Ability,
  render: (m: Monster) => `${sentence(m.theName)} deals double damage to objects and structures.`,
};

// e.g. Kuo-toa
const slippery: TraitTemplate = {
  name: 'Slippery',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} has advantage on ability checks and
    saving throws made to escape a grapple`,
};

// e.g. Ettercap
const spiderClimb: TraitTemplate = {
  name: 'Spider Climb',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} can climb difficult surfaces,
    including upside down on ceilings, without needing to make an
    ability check.`,
};

// e.g. Bullywug
type StandingLeapProps = { long: number; high: number };
const standingLeap: TraitTemplate<StandingLeapProps> = {
  name: 'Standing Leap',
  type: TraitType.Ability,
  parseProps: (p: any) => ({ long: parseInt(p.long) || 0, high: parseInt(p.high) || 0 }),
  render: (m: Monster, p: StandingLeapProps) =>
    `${possessive(sentence(m.theName))} long jump is up to ${p.long} feet and
    ${possessive(m.pronoun)} high jump is up to ${p.high} feet, with or
     without a running start.`,
};

// e.g. Bearded Devil
const steadfast: TraitTemplate = {
  name: 'Steadfast',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} can't be frightened while ${m.pronoun} can see an
    allied creature within 30 feet of ${m.pronoun}.`,
};

// e.g. Troglodyte
const stench: TraitTemplate = {
  name: 'Stench',
  type: TraitType.Ability,
  adjustCRStats: (m: Monster, s: CRStats) => ({ ...s, ac: s.ac + 1 }),
  render: (m: Monster) =>
    `Any creature other than a ${m.name} that starts its
    turn within 5 feet of ${m.theName} must succeed on a DC ${calculateDC(m, Ability.Con)}
    Constitution saving throw or be poisoned until the start of the
    creature's next turn. On a successful saving throw, the creature
    is immune to the stench of all ${m.name}s for 1 hour.`,
};

// e.g. Kobold
const sunlightSensitivity: TraitTemplate = {
  name: 'Sunlight Sensitivity',
  type: TraitType.Ability,
  render: (m: Monster) => `While in sunlight, ${m.theName} has disadvantage on attack
    rolls, as well as on Wisdom (Perception) checks that rely on sight.`,
};

// e.g. Faerie dragon
const superiorInvisibility: TraitTemplate = {
  name: 'Superior Invisibility',
  type: TraitType.Ability,
  adjustCRStats: (m: Monster, s: CRStats) => ({ ...s, ac: s.ac + 2 }),
  render: (m: Monster) =>
    `As a bonus action, ${m.theName} can magically turn invisible until 
    ${possessive(m.pronoun)} concentration ends (as if concentrating on a spell).
    Any equipment ${m.theName} wears or carries is invisible with ${m.pronoun}.`,
};

// e.g. Dao
const sureFooted: TraitTemplate = {
  name: 'Sure-Footed',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} has advantage on Strength and Dexterity
    saving throws made against effects that would knock ${m.pronoun} prone`,
};

// e.g. Bugbear
type SurpriseAttackProps = { damage: Dice };
const surpriseAttack: TraitTemplate<SurpriseAttackProps> = {
  type: TraitType.Ability,
  name: 'Surprise Attack',
  parseProps: (p: any) => ({ damage: parseDice(p.damage || '2d6') }),
  adjustCRStats: (m: Monster, s: CRStats, p: SurpriseAttackProps) => ({
    ...s,
    dpr: s.dpr + p.damage.average,
  }),
  render: (m, p) =>
    `If ${m.theName} surprises a creature and hits it with an attack
    during the first round of combat, the target takes an extra
    ${renderDamageDice(p.damage)} damage from the attack.`,
};

// e.g. Behir
type SwallowProps = { damage: Damage };
const swallow: TraitTemplate<SwallowProps> = {
  type: TraitType.Action,
  name: 'Swallow',
  parseProps: (p: any) => ({ damage: parseDamage(p.damage) }),
  adjustCRStats: (m: Monster, s: CRStats, p: SwallowProps) => ({
    ...s,
    dpr: s.dpr + (p.damage.amount.average * 2) / 3,
  }),
  render: (m, p) =>
    `${sentence(m.theName)} makes one bite attack against a Medium or
      smaller target it is grappling. If the attack hits, the target is also
      swallowed, and the grapple ends. While swallowed, the target
      is blinded and restrained, it has total cover against attacks
      and other effects outside ${m.theName}, and it takes ${renderDamage(p.damage)}
      damage at the start of each of ${possessive(m.theName)} turns. ${m.theName} can
      have only one creature swallowed at a time.

      If ${m.theName} takes 30 damage or more on a single turn from
      the swallowed creature, ${m.theName} must succeed on a DC 14
      Constitution saving throw at the end of that turn or regurgitate
      the creature, which falls prone in a space within 10 feet of
      ${m.theName}. If ${m.theName} dies, a swallowed creature is no longer
      restrained by it and can escape from the corpse by using 15 feet
      of movement, exiting prone.`,
};

// e.g. Balor
const teleport: TraitTemplate = {
  name: 'Teleport',
  type: TraitType.Action,
  render: (m: Monster) =>
    `${sentence(m.theName)} magically teleports, along with any
    equipment it is wearing or carrying, up to 120 feet to an
    unoccupied space it can see.`,
};

// e.g. Bullywug
type TerrainCamouflageProps = { terrain: string };
const terrainCamouflage: TraitTemplate<TerrainCamouflageProps> = {
  canonicalName: 'Terrain Camouflage',
  name: (m: Monster, p: TerrainCamouflageProps) => `${p.terrain} Camouflage`,
  type: TraitType.Ability,
  render: (m: Monster, p: TerrainCamouflageProps) =>
    `${sentence(m.theName)} has advantage on Dexterity (Stealth) checks
    made to hide in ${p.terrain} terrain.`,
};

// e.g. Umber Hulk
const tunneler: TraitTemplate = {
  name: 'Tunneler',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} can burrow through solid rock at
    half ${possessive(m.pronoun)} burrowing speed and leaves a 5 foot-wide,
    8-foot-high tunnel in ${possessive(m.pronoun)} wake.`,
};

// e.g. Revenant
const turnImmunity: TraitTemplate = {
  name: 'Turn Immunity',
  type: TraitType.Ability,
  render: (m: Monster) => `${sentence(m.theName)} is immune to effects that turn undead`,
};

// e.g. Lich
const turnResistance: TraitTemplate = {
  name: 'Turn Resistance',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} has advantage on saving throws against any effect
    that turns undead.`,
};

// e.g. Ettin
const twoHeads: TraitTemplate = {
  name: 'Two Heads',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `${sentence(m.theName)} has advantage on Wisdom (Perception)
    checks and on saving throws against being blinded, charmed,
    deafened, frightened, stunned, and knocked unconscious.`,
};

// e.g. Zombie
const undeadFortitude: TraitTemplate = {
  name: 'Undead Fortitude',
  type: TraitType.Ability,
  adjustCRStats: (m: Monster, s: CRStats) => {
    let hp;
    if (m.cr.value <= 4) hp = 7;
    else if (m.cr.value <= 10) hp = 14;
    else if (m.cr.value <= 16) hp = 21;
    else hp = 28;
    return { ...s, hp: s.hp + hp };
  },
  render: (m: Monster) =>
    `If damage reduces ${m.theName} to 0 hit points, ${m.pronoun} must make
    a Constitution saving throw with a DC of 5 + damage taken, unless it is
    radiant or from a critical hit. On a success, ${m.pronoun} drops to 1 hit point instead.`,
};

// e.g. Giant Spider
const web: TraitTemplate = {
  canonicalName: 'Web',
  name: 'Web (Recharge 5-6)',
  type: TraitType.Action,
  adjustCRStats: (m: Monster, s: CRStats) => ({ ...s, ac: s.ac + 1 }),
  render: (m: Monster) =>
    renderWeapon(m, {
      weapon: 'Web',
      weaponType: WeaponType.Ranged,
      attackType: AttackType.Weapon,
      omitDamageModifier: true,
      extraDamage: [],
      finesse: true,
      targets: 'one creature',
      extraDesc: (m: Monster) =>
        `The target is restrained by webbing. As an action, the restrained target 
        can make a DC ${calculateDC(m, Ability.Str)} Strength check, bursting
        the webbing on a success. The webbing can also be attacked and 
        destroyed (AC 10; hp 5; vulnerability to fire damage; immunity to 
        bludgeoning, poison, and psychic damage).`,
    }),
};

// e.g. Giant Spider
const webSense: TraitTemplate = {
  name: 'Web Sense',
  type: TraitType.Ability,
  render: (m: Monster) =>
    `While in contact with a web, ${m.theName} knows the exact location of
    any other creature in contact with the same web.`,
};

// e.g. Giant Spider
const webWalker: TraitTemplate = {
  name: 'Web Walker',
  type: TraitType.Ability,
  render: (m: Monster) => `${sentence(m.theName)} ignores movement restrictions caused by webbing.`,
};

// e.g. Quaggoth
type WoundedFuryProps = { damage: Dice };
const woundedFury: TraitTemplate<WoundedFuryProps> = {
  name: 'Wounded Fury',
  type: TraitType.Ability,
  parseProps: (p: any) => ({ damage: parseDice(p.damage) }),
  adjustCRStats: (m: Monster, s: CRStats, p: WoundedFuryProps) => ({
    ...s,
    dpr: s.dpr + p.damage.average / 3,
  }),
  render: (m, p) =>
    `While ${m.pronoun} has 10 hit points or fewer, ${m.theName} has advantage
    on attack rolls. In addition, ${m.pronoun} deals an extra ${renderDamageDice(p.damage)}
    damage to any target ${m.pronoun} hits with a melee attack.`,
};

export const traitsTable = {
  aggressive: aggressive,
  ambusher: ambusher,
  amorphous: amorphous,
  amphibious: amphibious,
  'angelic weapons': angelicWeapons,
  'antimagic susceptibility': antimagicSusceptibility,
  avoidance: avoidance,
  'blind senses': blindSenses,
  'blood frenzy': bloodFrenzy,
  'breath weapon': breathWeapon,
  brute: brute,
  'chameleon skin': chameleonSkin,
  'change shape': changeShape,
  charge: charge,
  charm: charm,
  constrict: constrict,
  'damage absorption': damageAbsorption,
  'damage transfer': damageTransfer,
  'death burst': deathBurst,
  'devil sight': devilSight,
  'dive attack': dive,
  echolocation: echolocation,
  'elemental body': elementalBody,
  enlarge: enlarge,
  etherealness: etherealness,
  'false appearance': falseAppearance,
  'fey ancestry': feyAncestry,
  'fiendish blessing': fiendishBlessing,
  flyby: flyby,
  'frightful presence': frightfulPresence,
  grappler: grappler,
  'hold breath': holdBreath,
  'horrifying visage': horrifyingVisage,
  illumination: illumination,
  'illusory appearance': illusoryAppearance,
  'immutable form': immutableForm,
  'innate spellcasting': innateSpellcasting,
  'incorporeal movement': incorporealMovement,
  inscrutable: inscrutable,
  invisibility: invisibility,
  'keen senses': keenSenses,
  'labyrinthine recall': labyrinthineRecall,
  leadership: leadership,
  'legendary resistance': legendaryResistance,
  'life drain': lifeDrain,
  'light sensitivity': lightSensitivity,
  'magic resistance': magicResistance,
  'magic weapons': magicWeapons,
  'martial advantage': martialAdvantage,
  mimicry: mimicry,
  'nimble escape': nimbleEscape,
  'otherworldly perception': otherworldlyPerception,
  'pack tactics': packTactics,
  parry: parry,
  possession: possession,
  pounce: pounce,
  rampage: rampage,
  reactive: reactive,
  'read thoughts': readThoughts,
  reckless: reckless,
  'redirect attack': redirectAttack,
  reel: reel,
  regeneration: regeneration,
  rejuvenation: rejuvenation,
  relentless: relentless,
  'shadow stealth': shadowStealth,
  shapechanger: shapechanger,
  'siege monster': siegeMonster,
  slippery: slippery,
  spellcasting: spellcasting,
  'spider climb': spiderClimb,
  'standing leap': standingLeap,
  steadfast: steadfast,
  stench: stench,
  'sunlight sensitivity': sunlightSensitivity,
  'superior invisibility': superiorInvisibility,
  'sure footed': sureFooted,
  'surprise attack': surpriseAttack,
  swallow: swallow,
  teleport: teleport,
  'terrain camouflage': terrainCamouflage,
  tunneler: tunneler,
  'turn immunity': turnImmunity,
  'turn resistance': turnResistance,
  'two heads': twoHeads,
  'undead fortitude': undeadFortitude,
  web: web,
  'web sense': webSense,
  'web walker': webWalker,
  'wounded fury': woundedFury,
};
