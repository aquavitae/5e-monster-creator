import { spellcasting, SpellcastingProps } from './spellcasting';
import { sampleMonster } from '../monster-base.test';
import { Ability } from '../../ability';
import { applyRender } from '../util';

describe('spellcasting', () => {
  it('renders correctly', () => {
    const props: SpellcastingProps = {
      level: 5,
      list: 'cleric',
      slots: [3, 2, 1],
      ability: Ability.Wis,
      spells: {
        0: [
          { name: 'thaumaturgy', level: 0 },
          { name: 'guidance', level: 0 },
          { name: 'toll the dead', level: 0 },
        ],
        1: [
          { name: 'cure wounds', level: 1 },
          { name: 'guiding bolt', level: 1 },
        ],
        2: [{ name: 'healing word', level: 2 }],
      },
    };

    const s = applyRender(spellcasting.render, sampleMonster, props);
    expect(s).toEqual(`The monster is a 5th-level spellcaster.
It's spellcasting ability is Wisdom
(spell save DC 12, +4
to hit with spell attacks). The monster has the following cleric spells prepared:

* Cantrips (at will): *thaumaturgy, guidance, toll the dead*
* 1st level (2 slots): *cure wounds, guiding bolt*
* 2nd level (1 slots): *healing word*`);
  });
});
