import { Dice, parseDice } from '../../dice';
import { Ability } from '../../ability';
import { Monster } from '../../types';
import { modifier, parseStringArray } from '../../utils';
import { knownSpellDamage } from './builtin-spells';

export type SpellLevel = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9;

export type Spell = {
  name: string;
  level: SpellLevel;
  damage?: Dice;
};

export type SpellList = { [key: number]: Spell[] };

const asLevel = (value: number): SpellLevel =>
  value < 0 ? 0 : value > 9 ? 9 : (value as SpellLevel);

export const spellListMap = <T>(sl: SpellList, fn: (k: SpellLevel, s: Spell[]) => T): T[] =>
  Object.keys(sl)
    .sort()
    .map((x) => parseInt(x))
    .map((l) => fn(l as SpellLevel, (sl as any)[l] as Spell[]));

export const parseSpell = (s: any, level: number): Spell | undefined => {
  if (!(s && (typeof s === 'object' || typeof s == 'string'))) {
    return undefined;
  }

  const raw = typeof s === 'string' ? { name: s, level } : s;
  const damage = raw.damage || knownSpellDamage[raw.name.toLowerCase()];

  return {
    name: raw.name,
    level: asLevel(level),
    damage: damage && parseDice(damage),
  };
};

const parseArrayOfSpells = (s: any, level: SpellLevel): Spell[] => {
  return (typeof s === 'string' ? parseStringArray(s) : s)
    .map((x: any) => parseSpell(x, level))
    .filter((x: Spell | undefined): x is Spell => x !== undefined)
    .sort((a: Spell, b: Spell) => (a.name < b.name ? -1 : a.name > b.name ? 1 : 0));
};

export const parseSpells = (s: any): SpellList =>
  s && typeof s === 'object'
    ? Object.keys(s)
        .map((v) => asLevel(parseInt(v) || 0))
        .reduce(
          (r, v) => ({
            ...r,
            [v]: parseArrayOfSpells(s[v], v),
          }),
          {} as SpellList,
        )
    : {};

export const spellMod = (m: Monster, ability: Ability) =>
  modifier(m.scores[ability]) + m.cr.proficiency;

export const spellDamage = (s?: Spell) => (s && s.damage ? s.damage.average : 0);
