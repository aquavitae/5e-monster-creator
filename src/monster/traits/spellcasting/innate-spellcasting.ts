import { TraitTemplate, TraitType } from '../types';
import { parseSpells, SpellList, spellListMap } from './spells';
import { Monster } from '../../types';
import { Ability, abilityName, parseAbility } from '../../ability';
import { possessive, sentence } from '../util';
import { calculateDC } from '../calculators';

type InnateSpellcastingProps = {
  ability: Ability;
  spells: SpellList;
};

const renderSpells = (spells: SpellList) =>
  spellListMap(
    spells,
    (k, spells) =>
      `* ${k === 0 ? 'At will' : `${k}/day`}: *${spells.map((s) => s.name).join(', ')}*`,
  ).join('\n');

export const innateSpellcasting: TraitTemplate<InnateSpellcastingProps> = {
  name: 'Innate Spellcasting',
  type: TraitType.Ability,
  adjustCRStats: undefined,
  adjustMonster: undefined,
  attackBonus: undefined,
  dpr: undefined,
  explainCRStats: undefined,
  parseProps: (s: any): InnateSpellcastingProps => ({
    ability: parseAbility(s.ability) || Ability.Int,
    spells: parseSpells(s.spells),
  }),
  render: (m: Monster, p: InnateSpellcastingProps) =>
    `${possessive(sentence(m.theName))} innate spellcasting ability is 
    ${abilityName(p.ability)} (spell save DC ${calculateDC(m, p.ability)}). 
    ${sentence(m.pronoun)} can innately cast the following spells, requiring no components:
  
    ${renderSpells(p.spells)}`,
  saveDC: undefined,
};
