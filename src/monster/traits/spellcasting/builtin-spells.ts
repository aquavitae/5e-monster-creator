export const knownSpellDamage: any = {
  'acid splash': '2d6',
  "aganazzar's scorcher": '6d8',
  'arms of hadar': '4d6',
  blight: '8d8',
  'burning hands': '6d6',
  'call lightning': '3d10',
  catapult: '3d8',
  'chain lighning': '10d8',
  'chill touch': '1d8',
  'chromatic orb': '3d8',
  cloudkill: '5d8',
  'create bonfire': '1d8',
  disintegrate: '10d6+40',
  'dissonant whispers': '3d6',
  'earth tremor': '2d6',
  'eldritch blast': '1d10',
  feeblemind: '4d6',
  'finger of death': '7d8+30',
  fireball: '8d6',
  firebolt: '1d10',
  'guiding bolt': '4d6',
  harm: '14d6',
  'hellish rebuke': '2d10',
  'inflict wounds': '3d10',
  'lightning arrow': '4d8',
  'lightning lure': '1d8',
  'magic missile': '3d4+3',
  "melf's acid arrow": '6d4',
  'poison spray': '1d12',
  'power word kill': '100',
  'produce flame': '1d8',
  'ray of frost': '1d8',
  'ray of sickness': '2d8',
  'sacred flame': '1d8',
  'shocking grasp': '1d8',
  'sword burst': '2d6',
  'thorn whip': '1d6',
  thunderclap: '2d6',
  thunderwave: '4d8',
  'vicious mockery': '1d4',
}

// 'animate objects (medium)': spread~5 x 2d6+1
// 'abi-dalzim's horrid wilting': aoe~10d8
// 'animate objects (huge)': spread~1.25 x 2d12+4
// 'animate objects (large)': spread~2.5 x 2d10+2
// 'animate objects (small)': spread~10 x 1d8+2
// 'animate objects (tiny)': spread~10 x 1d4+4
// 'banishing smite': 'w+5d10',
// 'bigby's hand (crush)': spread~2d6/turn
// 'bigby's hand (punch)': spread~4d8/turn
// 'blade barrier': aoe~6d10
// 'blight': '8d8',
// 'blinding smite': 'w+3d8',
// 'bones of earth (crush to ceiling)': x6~6d6
// 'branding smite': 'w+2d6',
// 'call lightning': aoe (small)~3d10/turn
// 'chain lightning': x4~10d8
// 'circle of death': aoe (huge)~8d6
// 'cloud of daggers': '4d4/turn',
// 'cloudkill': aoe (large)~5d8/turn
// 'cone of cold': aoe (large)~8d8
// 'conjure barrage': aoe (large)~3d8
// 'conjure volley': aoe (large)~8d8
// 'cordon of arrows': spread~1d6 x4
// 'delayed blast fireball': aoe (large)~12d6 + 1d6/turn
// 'destructive wave': aoe (large)~5d6+5d6
// 'dust devil': '2d8/turn',
// 'ensnaring strike': 'w+1d6/turn',
// 'erupting earth': aoe~3d12
// 'evard's black tentacles': aoe~3d6/turn
// 'fire shield': '2d8/attack',
// 'fire storm': aoe (large)~7d10
// 'fireball': aoe (large)~8d6
// 'flame blade': spread~3d6/turn
// 'flame strike': aoe~4d6+4d6
// 'flaming sphere': aoe (small)~2d6/turn
// 'glyph of warding (explosive runes)': aoe (large)~5d8
// 'guardian of faith': aoe~20 (total 60)
// 'hail of thorns': aoe (small)~w+1d10
// 'heat metal': '2d8/turn',
// 'hunger of hadar': aoe (large)~2d6+2d6/turn
// 'ice knife': single + aoe~1d10+2d6
// 'ice storm': aoe (large)~2d8+4d6
// 'immolation': '7d6 + 3d6/turn',
// 'incendiary cloud': aoe (large)~10d8/turn
// 'insect plague': aoe (large)~4d10/turn
// 'lightning bolt': aoe (line)~8d6
// 'maelstrom': aoe (large)~6d6/turn
// 'maximillian's earthen grasp': '2d6/turn',
// 'melf's acid arrow': '4d4+2d4',
// 'melf's minute meteors': aoe (small)~2d6 x2/turn
// 'meteor swarm': aoe (huge+)~20d6+20d6
// 'moonbeam': aoe (small)~2d10/turn
// 'mordenkainen's faithful hound': spread~4d8/turn
// 'mordenkainen's sword': spread~3d10/turn
// 'phantasmal killer': '4d10/turn',
// 'prismatic spray': aoe (random)~10d6
// 'prismatic wall': aoe (line)~10d6/layer
// 'scorching ray': spread~2d6 x3
// 'searing smite': 'w+1d6/turn',
// 'shatter': aoe~3d8
// 'snilloc's snowball swarm': aoe (small)~3d6
// 'spike growth': aoe (large)~2d4/square
// 'spirit guardians': aoe~3d8/turn
// 'spiritual weapon': spread~1d8+wis/turn
// 'staggering smite': 'w+4d6',
// 'storm sphere': aoe (large)~2d6/turn
// 'sunbeam': aoe (line)~6d8/turn
// 'sunburst': aoe (huge)~12d6
// 'symbol (death)': aoe (huge)~10d10/turn
// 'thunderous smite': 'w+2d6',
// 'tidal wave': aoe~4d8
// 'transmute rock (ceiling to mud)': aoe (large)~4d8
// 'tsunami': aoe (huge+)~6d10 -1d10/turn
// 'vampiric touch': '3d6',
// 'vitriolic sphere': aoe (large)~10d4+5d4
// 'wall of fire': aoe (line)~5d8/turn
// 'wall of thorns': aoe~7d8/turn
// 'weird': aoe (large)~4d10/turn
// 'whirlwind': aoe~10d6/turn
// 'witch bolt': '1d12+1d12/turn',
// 'wrathful smite': 'w+1d6',
