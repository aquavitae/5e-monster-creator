import { TraitTemplate, TraitType } from '../types';
import { parseSpells, Spell, spellDamage, SpellList, spellListMap, spellMod } from './spells';
import { Monster } from '../../types';
import { Ability, abilityName, parseAbility } from '../../ability';
import { possessive, sentence } from '../util';
import { calculateDC } from '../calculators';
import { withSign } from '../../utils';

export type SpellcastingProps = {
  level: number;
  list: string;
  slots: number[];
  ability: Ability;
  spells: SpellList;
};

const renderSpells = (spells: SpellList, slots: number[]) =>
  spellListMap(
    spells,
    (k, spells) =>
      `* ${
        k === 0 ? 'Cantrips (at will)' : `${levelTh(k)} level (${slots[k] || 0} slots)`
      }: *${spells.map((s) => s.name).join(', ')}*`,
  ).join('\n');

export const spellcasting: TraitTemplate<SpellcastingProps> = {
  name: 'Spellcasting',
  type: TraitType.Ability,
  attackBonus: (m: Monster, p: SpellcastingProps) => spellMod(m, p.ability),
  saveDC: (m: Monster, p: SpellcastingProps) => calculateDC(m, p.ability) + m.cr.proficiency,

  dpr: (m: Monster, p: SpellcastingProps) => {
    const slotsAvailable = [...p.slots];
    let dpr = 0;
    for (let i = 0; i < 3; i++) {
      const bestSpell = spellListMap(p.spells, (k, spells) => spells)
        .flatMap((s) => s)
        .filter((s) => slotsAvailable[s.level] > 0)
        .reduce((r: Spell | undefined, s) => (spellDamage(s) > spellDamage(r) ? s : r), undefined);
      if (bestSpell !== undefined) {
        slotsAvailable[bestSpell.level] -= 1;
        dpr += spellDamage(bestSpell);
      }
    }
    return dpr / 3;
  },

  explainCRStats: undefined,

  parseProps: (s: any): SpellcastingProps => ({
    level: parseInt(s.level) || 1,
    list: (s.list || 'wizard').toString(),
    slots: parseSlots(s.slots, parseInt(s.level) || 1),
    ability: parseAbility(s.ability) || Ability.Int,
    spells: parseSpells(s.spells),
  }),

  render: (m: Monster, p: SpellcastingProps) => `
    |${sentence(m.theName)} is a ${levelTh(p.level)}-level spellcaster.
    |${possessive(sentence(m.pronoun))} spellcasting ability is ${abilityName(p.ability)}
    |(spell save DC ${calculateDC(m, p.ability)}, ${withSign(spellMod(m, p.ability))}
    |to hit with spell attacks). ${sentence(m.theName)} has the following ${p.list} spells prepared:
    |
    |${renderSpells(p.spells, p.slots)}`,
};

const levelTh = (n: number) => {
  switch (n) {
    case 1:
      return '1st';
    case 2:
      return '2nd';
    case 3:
      return '3rd';
    default:
      return `${n}th`;
  }
};

const slots = (level: number) =>
  [
    [2], //1
    [3], //2
    [4, 2], //3
    [4, 3], //4
    [4, 3, 2], //5
    [4, 3, 3], //6
    [4, 3, 3, 1], //7
    [4, 3, 3, 2], //8
    [4, 3, 3, 3, 1], //9
    [4, 3, 3, 3, 2], //10
    [4, 3, 3, 3, 2, 1], //11
    [4, 3, 3, 3, 2, 1], //12
    [4, 3, 3, 3, 2, 1, 1], //13
    [4, 3, 3, 3, 2, 1, 1], //14
    [4, 3, 3, 3, 2, 1, 1, 1], //15
    [4, 3, 3, 3, 2, 1, 1, 1], //16
    [4, 3, 3, 3, 2, 1, 1, 1, 1], //17
    [4, 3, 3, 3, 3, 1, 1, 1, 1], //18
    [4, 3, 3, 3, 3, 2, 1, 1, 1], //19
    [4, 3, 3, 3, 3, 2, 2, 1, 1], //20
  ][level] || [];

const parseSlots = (s: any, level: number) => {
  const def = slots(level);
  if (s && typeof s === 'object') {
    Object.keys(s).forEach((k) => {
      const l = (parseInt(k) || -1) + 1;
      if (l >= 1 || l <= 9) {
        def[l] = parseInt(s[k]);
      }
    });
  }
  return def;
};
