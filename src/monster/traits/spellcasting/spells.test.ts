import { parseSpell, parseSpells } from './spells';
import { parseDice } from '../../dice';

describe('spells', () => {
  it('parses an simple spell list', () => {
    expect(parseSpells({ 0: [{ name: 'friends', level: 0 }] })).toEqual({
      0: [{ name: 'friends', level: 0 }],
    });
  });
});

describe('parseSpell', () => {
  it('parses known spells', () => {
    expect(parseSpell({ name: 'firebolt', level: 0 }, 0)).toEqual({
      name: 'firebolt',
      level: 0,
      damage: parseDice('1d10'),
    });
  });
});
