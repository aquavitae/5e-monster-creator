export enum WeaponType {
  Melee = 'melee',
  Ranged = 'ranged',
}

export const parseWeaponType = (t: any) => {
  switch ((t || '').toLowerCase()) {
    case 'melee':
      return WeaponType.Melee;
    case 'ranged':
      return WeaponType.Ranged;
    default:
      return undefined;
  }
};

export enum AttackType {
  Weapon = 'weapon',
  Spell = 'spell',
}

export const parseAttackType = (t: any) => {
  switch ((t || '').toLowerCase()) {
    case 'weapon':
      return AttackType.Weapon;
    case 'spell':
      return AttackType.Spell;
    default:
      return undefined;
  }
};
