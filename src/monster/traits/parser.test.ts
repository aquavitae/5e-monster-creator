import { findTemplate } from './parser';
import { traitsTable } from './traits';
import { weaponAttack } from './weapon';

describe('findTemplate', () => {
  test('finds a named trait', () => {
    const t = { trait: 'aggressive' };
    expect(findTemplate(t)).toEqual(traitsTable.aggressive);
  });

  test('finds a named trait with properties', () => {
    const t = {
      trait: 'charge',
      attackName: 'tusk',
      damage: {
        amount: '1d6',
        type: 'slashing',
      },
    };

    expect(findTemplate(t)).toEqual(traitsTable.charge);
  });

  test('finds a named weapon', () => {
    const t = findTemplate({ weapon: 'shortsword' });
    expect(t).toEqual({ ...weaponAttack, canonicalName: 'shortsword' });
  });

  test('finds a custom weapon', () => {
    const t = {
      weapon: 'Lightsabre',
      baseDamage: {
        amount: '2d6',
        type: 'radiant',
      },
    };
    expect(findTemplate(t)).toEqual({ ...weaponAttack, canonicalName: 'Lightsabre' });
  });
});
