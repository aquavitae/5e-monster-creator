import { compile, EvalFunction } from 'mathjs';
import { Calculator, CRStats, TraitTemplate, TraitType } from './types';
import { Monster } from '../types';
import { applyRender } from './util';

/**
 * Return a set of variables usable in math evaluation
 */
const calcVars = (m: Monster, value: number) => ({
  x: value, // Current value
  p: m.cr.proficiency, // proficiency bonus
});

const compileExpr = (expr: any) => {
  if (expr && typeof expr === 'string') {
    try {
      return compile(expr);
    } catch (e) {
      console.debug(e);
    }
  }
  return undefined;
};

export class Trait {
  readonly canonicalName: string = 'undefined';
  props: any = {};

  // For legendary actions
  private readonly laCost?: number;

  private readonly tmpl: TraitTemplate<object> = {} as TraitTemplate;
  private readonly name?: string;
  private readonly desc?: string;

  readonly type: TraitType = TraitType.Ability;

  private readonly adjust?: {
    hp?: EvalFunction;
    ac?: EvalFunction;
    ab?: EvalFunction;
    dc?: EvalFunction;
    dpr?: EvalFunction;
    explain?: string;
  };

  private readonly setStats?: {
    ab?: EvalFunction;
    dc?: EvalFunction;
    dpr?: EvalFunction;
    explain?: string;
  };

  constructor(tmpl?: TraitTemplate<any>, s?: any) {
    if (tmpl === undefined || s === undefined || s.trait === null) {
      this.tmpl = {};
    } else {
      this.tmpl = tmpl;
      this.props = tmpl.parseProps ? tmpl.parseProps(s) : {};
      this.canonicalName = (
        s.canonicalName ||
        tmpl.canonicalName ||
        s.name ||
        tmpl.name ||
        'unknown'
      ).toString();

      this.type = tmpl.type || TraitType.Ability;
      this.name = s.name?.toString();
      this.desc = s.desc?.toString();

      this.adjust = {
        hp: compileExpr(s.adjust?.hp),
        ac: compileExpr(s.adjust?.ac),
        ab: compileExpr(s.adjust?.ab),
        dc: compileExpr(s.adjust?.dc),
        dpr: compileExpr(s.adjust?.dpr),
        explain: s.adjust?.explain,
      };

      this.setStats = {
        ab: compileExpr(s.setStats?.ab),
        dc: compileExpr(s.setStats?.dc),
        dpr: compileExpr(s.setStats?.dpr),
        explain: s.setStats?.explain,
      };

      this.laCost = parseInt(s.cost) || undefined;
    }
  }

  private applyCalc(m: Monster, evalExpr?: math.EvalFunction, c?: Calculator<object>) {
    const value = (c && (typeof c == 'number' ? c : c(m, this.props))) || 0;
    if (evalExpr) {
      return evalExpr.evaluate(calcVars(m, value));
    }
    return value;
  }

  isValid() {
    return this.tmpl !== {};
  }

  cost() {
    return this.laCost || 1;
  }

  adjustMonster(m: Monster) {
    return this.tmpl.adjustMonster ? this.tmpl.adjustMonster(m, this.props) : m;
  }

  private adjustCRStatsFromTemplate(m: Monster, s: CRStats): [CRStats, string | null] {
    const stats = this.tmpl.adjustCRStats ? this.tmpl.adjustCRStats(m, s, this.props) : s;
    if (stats === s) {
      return [stats, null];
    }
    const explain = this.tmpl.explainCRStats ? this.tmpl.explainCRStats(m, this.props) : '';
    return [stats, explain];
  }

  private adjustCRStatsFromOverrides(m: Monster, s: CRStats): CRStats {
    return {
      hp: this.applyCalc(m, this.adjust?.hp, s.hp),
      ac: this.applyCalc(m, this.adjust?.ac, s.ac),
      dpr: this.applyCalc(m, this.adjust?.dpr, s.dpr),
      ab: this.applyCalc(m, this.adjust?.ab, s.ab),
      dc: this.applyCalc(m, this.adjust?.dc, s.dc),
    };
  }

  adjustCRStats(m: Monster, s: CRStats): [CRStats, string | null] {
    const [stats, explain] = this.adjustCRStatsFromTemplate(m, s);
    const stats2 = this.adjustCRStatsFromOverrides(m, stats);
    if (stats2 === stats) {
      return [stats, explain];
    }
    return [stats2, [explain, this.adjust?.explain].filter((x) => x).join('; ')];
  }

  calcDPR(m: Monster): number {
    return this.applyCalc(m, this.setStats?.dpr, this.tmpl.dpr);
  }

  calcAB(m: Monster): number {
    return this.applyCalc(m, this.setStats?.ab, this.tmpl.attackBonus);
  }

  calcDC(m: Monster): number {
    return this.applyCalc(m, this.setStats?.dc, this.tmpl.saveDC);
  }

  renderName(m: Monster) {
    return this.name || applyRender(this.tmpl.name, m, this.props);
  }

  renderDesc(m: Monster) {
    const desc = applyRender(this.tmpl.render, m, this.props);
    if (this.desc === undefined) return desc + '.';
    const params = [{ key: 'desc', value: desc }];
    return params.reduce(
      (r, { key, value }) => r.replace(new RegExp('\\$\\{' + key + '\\}', 'g'), value),
      this.desc,
    );
  }
}
