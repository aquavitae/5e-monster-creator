import { DamageType, parseDamage } from './damage';
import { parseDice } from '../dice';

describe('parseDamage', () => {
  const makeDmg = (d: string, t: DamageType) => ({
    amount: parseDice(d),
    type: t,
  });

  it('parses damage', () => {
    const got = parseDamage({ amount: '2d8', type: 'acid' });
    expect(got).toEqual(makeDmg('2d8', DamageType.Acid));
  });

  it('parses partial damage with defaults', () => {
    const got = parseDamage({ amount: '2d8' }, makeDmg('1d4', DamageType.Acid));
    expect(got).toEqual(makeDmg('2d8', DamageType.Acid));
  });
});
