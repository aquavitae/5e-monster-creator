import { parseDice } from '../dice';
import { WeaponTemplate } from './types';
import { calculateDC } from './calculators';
import { Monster } from '../types';
import { DamageType } from './damage';
import { AttackType, WeaponType } from './weapon-type';
import { Ability } from '../ability';

const club: WeaponTemplate = {
  weapon: 'Club',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Bludgeoning, amount: parseDice('1d4') },
  adjustForSize: true,
  extraDamage: [],
};

const dagger: WeaponTemplate = {
  weapon: 'Dagger',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  baseDamage: { type: DamageType.Piercing, amount: parseDice('1d4') },
  adjustForSize: true,
  extraDamage: [],
  finesse: true,
  thrown: [20, 60],
};

const greatclub: WeaponTemplate = {
  weapon: 'Greatclub',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Bludgeoning, amount: parseDice('1d8') },
  adjustForSize: true,
  extraDamage: [],
};

const handaxe: WeaponTemplate = {
  weapon: 'Handaxe',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Slashing, amount: parseDice('1d6') },
  adjustForSize: true,
  extraDamage: [],
  thrown: [20, 60],
};

const javelin: WeaponTemplate = {
  weapon: 'Javelin',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Piercing, amount: parseDice('1d6') },
  adjustForSize: true,
  extraDamage: [],
  thrown: [30, 120],
};

const lightHammer: WeaponTemplate = {
  weapon: 'Light hammer',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Bludgeoning, amount: parseDice('1d4') },
  adjustForSize: true,
  extraDamage: [],
  thrown: [20, 60],
};

const mace: WeaponTemplate = {
  weapon: 'Mace',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Bludgeoning, amount: parseDice('1d6') },
  adjustForSize: true,
  extraDamage: [],
};

const quarterstaff: WeaponTemplate = {
  weapon: 'Quarterstaff',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Bludgeoning, amount: parseDice('1d6') },
  adjustForSize: true,
  extraDamage: [],
  versatileDamage: { type: DamageType.Bludgeoning, amount: parseDice('1d8') },
};

const sickle: WeaponTemplate = {
  weapon: 'Sickle',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Slashing, amount: parseDice('1d4') },
  adjustForSize: true,
  extraDamage: [],
};

const spear: WeaponTemplate = {
  weapon: 'Spear',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Piercing, amount: parseDice('1d6') },
  adjustForSize: true,
  versatileDamage: { type: DamageType.Piercing, amount: parseDice('1d8') },
  extraDamage: [],
  thrown: [20, 60],
};

const unarmedStrike: WeaponTemplate = {
  weapon: 'Unarmed strike',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Bludgeoning, amount: parseDice('1') },
  adjustForSize: true,
  extraDamage: [],
};

const lightCrossbow: WeaponTemplate = {
  weapon: 'Light crossbow',
  weaponType: WeaponType.Ranged,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Piercing, amount: parseDice('1d8') },
  adjustForSize: true,
  extraDamage: [],
  range: [80, 320],
};

const dart: WeaponTemplate = {
  weapon: 'Dart',
  weaponType: WeaponType.Ranged,
  attackType: AttackType.Weapon,
  baseDamage: { type: DamageType.Piercing, amount: parseDice('1d4') },
  adjustForSize: true,
  extraDamage: [],
  finesse: true,
  thrown: [20, 60],
};

const shortbow: WeaponTemplate = {
  weapon: 'Shortbow',
  weaponType: WeaponType.Ranged,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Piercing, amount: parseDice('1d6') },
  adjustForSize: true,
  extraDamage: [],
  range: [80, 320],
};

const sling: WeaponTemplate = {
  weapon: 'Sling',
  weaponType: WeaponType.Ranged,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Bludgeoning, amount: parseDice('1d4') },
  adjustForSize: true,
  extraDamage: [],
  range: [30, 120],
};

const battleaxe: WeaponTemplate = {
  weapon: 'Battleaxe',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Slashing, amount: parseDice('1d8') },
  adjustForSize: true,
  extraDamage: [],
  versatileDamage: { type: DamageType.Slashing, amount: parseDice('1d10') },
};

const flail: WeaponTemplate = {
  weapon: 'Flail',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Bludgeoning, amount: parseDice('1d8') },
  adjustForSize: true,
  extraDamage: [],
};

const glaive: WeaponTemplate = {
  weapon: 'Glaive',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Slashing, amount: parseDice('1d10') },
  adjustForSize: true,
  extraDamage: [],
  reach: 10,
};

const greataxe: WeaponTemplate = {
  weapon: 'Greataxe',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Slashing, amount: parseDice('1d12') },
  adjustForSize: true,
  extraDamage: [],
};

const greatsword: WeaponTemplate = {
  weapon: 'Greatsword',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Slashing, amount: parseDice('2d6') },
  adjustForSize: true,
  extraDamage: [],
};

const halberd: WeaponTemplate = {
  weapon: 'Halberd',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Slashing, amount: parseDice('1d10') },
  adjustForSize: true,
  extraDamage: [],
  reach: 10,
};

const lance: WeaponTemplate = {
  weapon: 'Lance',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Piercing, amount: parseDice('1d12') },
  adjustForSize: true,
  extraDamage: [],
  reach: 10,
};

const longsword: WeaponTemplate = {
  weapon: 'Longsword',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Slashing, amount: parseDice('1d8') },
  adjustForSize: true,
  extraDamage: [],
  versatileDamage: { type: DamageType.Slashing, amount: parseDice('1d10') },
};

const maul: WeaponTemplate = {
  weapon: 'Maul',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Bludgeoning, amount: parseDice('2d6') },
  adjustForSize: true,
  extraDamage: [],
};

const morningstar: WeaponTemplate = {
  weapon: 'Morningstar',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Piercing, amount: parseDice('1d8') },
  adjustForSize: true,
  extraDamage: [],
};

const pike: WeaponTemplate = {
  weapon: 'Pike',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Piercing, amount: parseDice('1d10') },
  adjustForSize: true,
  extraDamage: [],
  reach: 10,
};

const rapier: WeaponTemplate = {
  weapon: 'Rapier',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  baseDamage: { type: DamageType.Piercing, amount: parseDice('1d8') },
  adjustForSize: true,
  extraDamage: [],
  finesse: true,
};

const scimitar: WeaponTemplate = {
  weapon: 'Scimitar',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  baseDamage: { type: DamageType.Slashing, amount: parseDice('1d6') },
  adjustForSize: true,
  extraDamage: [],
  finesse: true,
};

const shortsword: WeaponTemplate = {
  weapon: 'Shortsword',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  baseDamage: { type: DamageType.Slashing, amount: parseDice('1d6') },
  adjustForSize: true,
  extraDamage: [],
  finesse: true,
};

const trident: WeaponTemplate = {
  weapon: 'Trident',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Piercing, amount: parseDice('1d6') },
  adjustForSize: true,
  extraDamage: [],
  thrown: [20, 60],
  versatileDamage: { type: DamageType.Piercing, amount: parseDice('1d8') },
};

const warPick: WeaponTemplate = {
  weapon: 'War pick',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Piercing, amount: parseDice('1d8') },
  adjustForSize: true,
  extraDamage: [],
};

const warhammer: WeaponTemplate = {
  weapon: 'Warhammer',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Bludgeoning, amount: parseDice('1d8') },
  adjustForSize: true,
  extraDamage: [],
  versatileDamage: { type: DamageType.Bludgeoning, amount: parseDice('1d10') },
};

const whip: WeaponTemplate = {
  weapon: 'Whip',
  weaponType: WeaponType.Melee,
  attackType: AttackType.Weapon,
  baseDamage: { type: DamageType.Slashing, amount: parseDice('1d4') },
  adjustForSize: true,
  extraDamage: [],
  finesse: true,
  reach: 10,
};

const blowgun: WeaponTemplate = {
  weapon: 'Blowgun',
  weaponType: WeaponType.Ranged,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Piercing, amount: parseDice('1') },
  adjustForSize: true,
  extraDamage: [],
  range: [25, 100],
};

const handCrossbow: WeaponTemplate = {
  weapon: 'Hand crossbow',
  weaponType: WeaponType.Ranged,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Piercing, amount: parseDice('1d6') },
  adjustForSize: true,
  extraDamage: [],
  range: [30, 120],
};

const heavyCrossbow: WeaponTemplate = {
  weapon: 'Heavy crossbow',
  weaponType: WeaponType.Ranged,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Piercing, amount: parseDice('1d10') },
  adjustForSize: true,
  extraDamage: [],
  range: [100, 400],
};

const longbow: WeaponTemplate = {
  weapon: 'Longbow',
  weaponType: WeaponType.Ranged,
  attackType: AttackType.Weapon,
  finesse: false,
  baseDamage: { type: DamageType.Piercing, amount: parseDice('1d8') },
  adjustForSize: true,
  extraDamage: [],
  range: [150, 600],
};

const net: WeaponTemplate = {
  weapon: 'Net',
  weaponType: WeaponType.Ranged,
  attackType: AttackType.Weapon,
  finesse: false,
  extraDamage: [],
  adjustForSize: true,
  thrown: [5, 15],
  targets: 'one Large or smaller creature',
  extraDesc: (m: Monster) => `. The target is restrained.
        A creature can use its action to make a DC ${calculateDC(m, Ability.Str)}
        Strength check to free itself or another creature in a net, ending the effect on
        a success. Dealing 5 slashing damage to the net (AC 10) frees the target
        without harming it and destroys the net.`,
};

export const weaponsTable = {
  club: club,
  dagger: dagger,
  greatclub: greatclub,
  handaxe: handaxe,
  javelin: javelin,
  'light hammer': lightHammer,
  mace: mace,
  quarterstaff: quarterstaff,
  sickle: sickle,
  spear: spear,
  'unarmed strike': unarmedStrike,
  'light crossbow': lightCrossbow,
  dart: dart,
  shortbow: shortbow,
  sling: sling,
  battleaxe: battleaxe,
  flail: flail,
  glaive: glaive,
  greataxe: greataxe,
  greatsword: greatsword,
  halberd: halberd,
  lance: lance,
  longsword: longsword,
  maul: maul,
  morningstar: morningstar,
  pike: pike,
  rapier: rapier,
  scimitar: scimitar,
  shortsword: shortsword,
  trident: trident,
  'war pick': warPick,
  warhammer: warhammer,
  whip: whip,
  blowgun: blowgun,
  'hand crossbow': handCrossbow,
  'heavy crossbow': heavyCrossbow,
  longbow: longbow,
  net: net,
};
