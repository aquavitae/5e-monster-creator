import { Monster } from '../types';
import { Alignment } from '../alignment';
import { cr10 } from '../challenge-rating';
import { parseDice } from '../dice';
import { Pronoun } from '../pronoun';
import { Size } from '../size';

export const sampleMonster: Monster = {
  alignment: Alignment.Any,
  armor: [],
  cr: cr10,
  crStats: { hp: 0, ac: 0, dpr: 0, ab: 0, dc: 0 },
  expertise: [],
  hitDice: parseDice('2d8'),
  hitPoints: 9,
  languages: [],
  name: 'Monster',
  theName: 'the monster',
  optimalArmor: [],
  pronoun: Pronoun.It,
  saves: [],
  scores: { str: 10, dex: 10, con: 10, int: 10, wis: 10, cha: 10 },
  senses: [],
  size: Size.Medium,
  skills: [],
  speed: { walk: 30 },
  traits: [],
  type: '',
};

test('because jest insists', () => {});
