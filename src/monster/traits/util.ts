import { Render } from './types';
import { Monster } from '../types';

type ValueOf<T> = T[keyof T];

export const lookup = <T>(table: T, key: any): ValueOf<T> | undefined =>
  /// @ts-ignore
  table[key];

export const sentence = (s: string) => (s ? s[0].toUpperCase() + s.slice(1) : '');

export const possessive = (word: string) => {
  switch (word) {
    case '':
    case 'it':
      return 'its';
    case 'he':
      return 'his';
    case 'she':
      return 'her';
    default:
      return word + (word.match(/s$/) ? "'" : "'s");
  }
};

export const signed = (n: number) => `${n >= 0 ? '+' : ''}${n}`;

export const applyRender = <T>(r: Render<T> | undefined, m: Monster, p: T) =>
  (r ? (typeof r === 'string' ? r : r(m, p)) : '')
    .replace(/\.$/, '')
    .replace(/^\n/, '')
    .replace(/^ *\|/gm, '');
