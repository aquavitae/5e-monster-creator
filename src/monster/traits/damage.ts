import { Dice, parseDice } from '../dice';

export enum DamageType {
  Acid = 'acid',
  Bludgeoning = 'bludgeoning',
  Cold = 'cold',
  Fire = 'fire',
  Force = 'force',
  Lightning = 'lightning',
  Necrotic = 'necrotic',
  Piercing = 'piercing',
  Poison = 'poison',
  Psychic = 'psychic',
  Radiant = 'radiant',
  Slashing = 'slashing',
  Thunder = 'thunder',
}

export interface Damage {
  type: DamageType;
  amount: Dice;
}

export const parseDamageType = (t: any) => {
  switch ((t || '').toString().toLowerCase()) {
    case 'acid':
      return DamageType.Acid;
    default:
    case 'bludgeoning':
      return DamageType.Bludgeoning;
    case 'cold':
      return DamageType.Cold;
    case 'fire':
      return DamageType.Fire;
    case 'force':
      return DamageType.Force;
    case 'lightning':
      return DamageType.Lightning;
    case 'necrotic':
      return DamageType.Necrotic;
    case 'piercing':
      return DamageType.Piercing;
    case 'poison':
      return DamageType.Poison;
    case 'psychic':
      return DamageType.Psychic;
    case 'radiant':
      return DamageType.Radiant;
    case 'slashing':
      return DamageType.Slashing;
    case 'thunder':
      return DamageType.Thunder;
  }
};

export const parseDamage = (t: any, defaultValue?: Damage) => ({
  type: t && t.type ? parseDamageType(t.type) : defaultValue?.type || DamageType.Bludgeoning,
  amount: t && t.amount ? parseDice(t.amount) : defaultValue?.amount || parseDice(0),
});
