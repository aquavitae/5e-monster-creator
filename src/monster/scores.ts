export interface Scores {
  str: number;
  dex: number;
  con: number;
  int: number;
  wis: number;
  cha: number;
}

export const parseScores = (s: any) => ({
  str: (s && s.str) || 10,
  dex: (s && s.dex) || 10,
  con: (s && s.con) || 10,
  int: (s && s.int) || 10,
  wis: (s && s.wis) || 10,
  cha: (s && s.cha) || 10,
});
