import { Alignment } from './alignment';
import { Armor, ArmorTemplate } from './armor';
import { Pronoun } from './pronoun';
import { Size } from './size';
import { Speed } from './speed';
import { CRStatChange, CRStats } from './traits/types';
import { Dice } from './dice';
import { ChallengeRating } from './challenge-rating';
import { Resistances } from './resistances';
import { Skill } from './skills';
import { Ability } from './ability';
import { LegendaryActions } from './traits/legendary';
import { Scores } from './scores';
import { Trait } from './traits/trait';
import { MonsterType } from './monster-type';
import { Senses } from './senses';
import { Condition } from './conditions';

export interface MonsterDefinition {
  // Name of the monster
  name?: any;
  // Full name of the monster used in the heading, defaults to the name
  title?: any;

  // Pronoun is "he", "she", or "it" (default), depending on the type of monster. It
  // affects the way text descriptions are generated
  pronoun?: any;

  size?: any;
  type?: any;
  subtype?: any;
  alignment?: any;
  hpLevel?: any;
  speed?: any;
  challengeRating?: any;
  adjustCR?: any;

  // Since hit dice is really just a range, we can set the hit point
  // value within that range explicitly
  hitPoints?: any;

  scores?: any;

  saves?: any;
  skills?: any;
  expertise?: any;

  damageVulnerabilities?: any;
  damageResistances?: any;
  damageImmunities?: any;
  conditionImmunities?: any;
  senses?: any;
  languages?: any;
  languageExtra?: string;

  armor?: any;
  armorDesc?: any;

  traits?: any;
  legendaryActions?: any;
}

export interface Monster {
  title: string;
  name: string;
  pronoun: Pronoun;
  theName: string;

  size: Size;
  type: MonsterType;
  subtype: string;
  alignment: Alignment;
  speed: Speed;
  armor: ArmorTemplate[];
  armorDesc?: string;
  traitArmor: ArmorTemplate[];
  optimalArmor: Armor[];
  hitPoints: number;
  hitDice: Dice;

  explicitCR?: ChallengeRating;
  adjustCR: number;
  cr: ChallengeRating;
  crDetails: {
    stats: CRStats;
    statChanges: CRStatChange[];
    offensiveIndex?: number;
    defensiveIndex?: number;
    unadjusted: ChallengeRating;
  };

  scores: Scores;

  saves: Ability[];
  skills: Skill[];
  expertise: Skill[];

  damageVulnerabilities: Resistances;
  damageResistances: Resistances;
  damageImmunities: Resistances;
  conditionImmunities: Condition[];
  senses: Senses;
  languages: string[];
  languageExtra?: string;

  traits: Trait[];
  legendaryActions?: LegendaryActions;
}
