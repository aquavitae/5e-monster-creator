export enum Pronoun {
  He = 'he',
  She = 'she',
  It = 'it',
}

export const parsePronoun = (s?: string) => {
  switch ((s || '').toLowerCase()) {
    case 'he':
      return Pronoun.He;
    case 'she':
      return Pronoun.She;
    default:
    case 'it':
      return Pronoun.It;
  }
};
