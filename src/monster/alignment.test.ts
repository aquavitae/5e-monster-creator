import { Alignment, parseAlignment } from './alignment';

test('parses valid alignments', () => {
  expect(parseAlignment('lawful good')).toEqual(Alignment.LawfulGood);
  expect(parseAlignment('unaligned')).toEqual(Alignment.Unaligned);
});

test('parses invalid alignments as any', () => {
  expect(parseAlignment('dunno')).toEqual(Alignment.Any);
});

test('parses invalid objects as any', () => {
  expect(parseAlignment(undefined)).toEqual(Alignment.Any);
});
