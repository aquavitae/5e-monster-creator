import { parseStringArray } from './utils';

export enum Skill {
  Acrobatics = 'acrobatics',
  AnimalHandling = 'animal handling',
  Arcana = 'arcana',
  Athletics = 'athletics',
  Deception = 'deception',
  History = 'history',
  Insight = 'insight',
  Intimidation = 'intimidation',
  Investigation = 'investigation',
  Medicine = 'medicine',
  Nature = 'nature',
  Perception = 'perception',
  Performance = 'performance',
  Persuasion = 'persuasion',
  Religion = 'religion',
  SleightOfHand = 'sleight of hand',
  Stealth = 'stealth',
  Survival = 'survival',
}

const parseSkill = (s: any): Skill | undefined => {
  if (typeof s !== 'string') {
    return undefined;
  }

  if (Object.values(Skill).indexOf(s.toLowerCase() as Skill) >= 0) {
    return s.toLowerCase() as Skill;
  }
  return undefined;
};

export const parseSkills = (skills: any): Skill[] =>
  parseStringArray(skills)
    .map((p) => parseSkill(p))
    .filter((s) => s !== undefined) as Skill[];
