export enum MonsterType {
  Aberration = 'aberration',
  Beast = 'beast',
  Celestial = 'celestial',
  Construct = 'construct',
  Dragon = 'dragon',
  Elemental = 'elemental',
  Fey = 'fey',
  Fiend = 'fiend',
  Giant = 'giant',
  Humanoid = 'humanoid',
  Monstrosity = 'monstrosity',
  Ooze = 'ooze',
  Plant = 'plant',
  Undead = 'undead',
}

export const parseMonsterType = (s: any) => {
  switch ((s || '').toString().toLowerCase()) {
    case 'aberration':
      return MonsterType.Aberration;
    case 'beast':
      return MonsterType.Beast;
    case 'celestial':
      return MonsterType.Celestial;
    case 'construct':
      return MonsterType.Construct;
    case 'dragon':
      return MonsterType.Dragon;
    case 'elemental':
      return MonsterType.Elemental;
    case 'fey':
      return MonsterType.Fey;
    case 'fiend':
      return MonsterType.Fiend;
    case 'giant':
      return MonsterType.Giant;
    default:
    case 'humanoid':
      return MonsterType.Humanoid;
    case 'monstrosity':
      return MonsterType.Monstrosity;
    case 'ooze':
      return MonsterType.Ooze;
    case 'plant':
      return MonsterType.Plant;
    case 'undead':
      return MonsterType.Undead;
  }
};
