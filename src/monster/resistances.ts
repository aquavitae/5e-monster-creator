export type Resistances = {
  desc: string;
  affectsCR: boolean;
};

export const parseResistances = (s: string | undefined): Resistances => ({
  desc: s || '',
  affectsCR: !!(s || '').match(/(.*(bludgeoning|piercing|slashing)){3}/),
});
