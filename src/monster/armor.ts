import { modifier } from './utils';
import { Monster } from './types';

export interface ArmorTemplate {
  name: string;
  baseAC: number;
  maxAddDex?: number;
  ac?: number;
  isAdditional: boolean;
}

export interface Armor extends ArmorTemplate {
  ac: number;
}

const unarmored: ArmorTemplate = {
  name: 'unarmored',
  baseAC: 10,
  isAdditional: false,
};

const natural: ArmorTemplate = {
  name: 'natural armor',
  baseAC: 11,
  isAdditional: false,
};

const padded: ArmorTemplate = {
  name: 'padded armor',
  baseAC: 11,
  isAdditional: false,
};

const leather: ArmorTemplate = {
  name: 'leather armor',
  baseAC: 11,
  isAdditional: false,
};

const studdedLeather: ArmorTemplate = {
  name: 'studded leather armor',
  baseAC: 12,
  isAdditional: false,
};

const hide: ArmorTemplate = {
  name: 'hide armor',
  baseAC: 12,
  maxAddDex: 2,
  isAdditional: false,
};

const chainShirt: ArmorTemplate = {
  name: 'chain shirt',
  baseAC: 13,
  maxAddDex: 2,
  isAdditional: false,
};

const scaleMail: ArmorTemplate = {
  name: 'scale mail',
  baseAC: 14,
  maxAddDex: 2,
  isAdditional: false,
};

const breastplate: ArmorTemplate = {
  name: 'breastplate',
  baseAC: 14,
  maxAddDex: 2,
  isAdditional: false,
};

const halfPlate: ArmorTemplate = {
  name: 'half plate',
  baseAC: 15,
  maxAddDex: 2,
  isAdditional: false,
};

const ringMail: ArmorTemplate = {
  name: 'ring mail',
  baseAC: 14,
  isAdditional: false,
};

const chainMail: ArmorTemplate = {
  name: 'chain mail',
  baseAC: 15,
  isAdditional: false,
};

const splint: ArmorTemplate = {
  name: 'splint armor',
  baseAC: 16,
  isAdditional: false,
};

const plate: ArmorTemplate = {
  name: 'plate armor',
  baseAC: 18,
  isAdditional: false,
};

const shield: ArmorTemplate = {
  name: 'shield',
  baseAC: 2,
  maxAddDex: 0,
  isAdditional: true,
};

export const armorList = [
  'unarmored',
  'natural armor',
  'padded armor',
  'leather armor',
  'studded leather armor',
  'hide armor',
  'chain shirt',
  'scale mail',
  'breastplate',
  'half plate',
  'ring mail',
  'chain mail',
  'splint armor',
  'plate armor',
  'shield',
];

const parseNamedArmor = (name: any) => {
  switch ((name || '').toLowerCase()) {
    case 'unarmored':
      return unarmored;
    case 'natural armor':
      return natural;
    case 'padded':
    case 'padded armor':
      return padded;
    case 'leather':
    case 'leather armor':
      return leather;
    case 'studded leather':
    case 'studded leather armor':
      return studdedLeather;
    case 'hide':
    case 'hide armor':
      return hide;
    case 'chain shirt':
      return chainShirt;
    case 'scale mail':
      return scaleMail;
    case 'breastplate':
      return breastplate;
    case 'half plate':
      return halfPlate;
    case 'ring mail':
      return ringMail;
    case 'chain mail':
      return chainMail;
    case 'splint':
    case 'splint armor':
      return splint;
    case 'plate':
    case 'plate armor':
      return plate;
    case 'shield':
      return shield;
    default:
      return undefined;
  }
};

export const parseArmorTemplate = (s: any): ArmorTemplate => {
  if (!s) return unarmored;
  const a = typeof s === 'string' ? { name: s } : s;
  const p = { ...(parseNamedArmor(a.name) || a) };
  if (typeof s === 'object') {
    p.baseAC = parseInt(s.baseAC) || p.baseAC;
    p.maxAddDex = parseInt(s.maxAddDex) || p.maxAddDex;
    p.ac = parseInt(s.ac) || p.ac;
    p.isAdditional = 'isAdditional' in s ? !!s.isAdditional : p.isAdditional;
  }
  return p;
};

export const parseArmorTemplates = (s: any) => {
  if (!s) return [];
  if (typeof s === 'string') return [parseArmorTemplate(s)];
  if (Array.isArray(s)) return s.map(parseArmorTemplate);
  return [];
};

export const optimalArmor = (m: Monster): Armor[] =>
  [unarmored, ...m.armor, ...m.traitArmor]
    .map((a) => ({ ...a, ac: armorAC(a, modifier(m.scores.dex)) }))
    .reduce((r: Armor[], a: Armor, i: number) => {
      if (i === 0) {
        return [a];
      }
      if (a.isAdditional) {
        return [...r, a];
      }
      return a.ac > r[0].ac ? [a, ...r.slice(1)] : r;
    }, []);

const armorAC = (tmpl: ArmorTemplate, dexMod: number) =>
  tmpl.ac || Math.min(dexMod, tmpl.maxAddDex === undefined ? 999 : tmpl.maxAddDex) + tmpl.baseAC;

export const totalArmorAC = (armors: ArmorTemplate[], dexMod: number) =>
  armors.map((a) => armorAC(a, dexMod)).reduce((r, ac) => r + ac);
