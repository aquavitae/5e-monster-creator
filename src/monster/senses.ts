export type Senses = {
  blindsight?: number;
  darkvision?: number;
  tremorsense?: number;
  truesight?: number;
  extra?: string;
};

export const parseSenses = (s: any): Senses =>
  s
    ? typeof s === 'string'
      ? { extra: s }
      : {
          darkvision: parseInt(s.darkvision) || undefined,
          blindsight: parseInt(s.blindsight) || undefined,
          truesight: parseInt(s.truesight) || undefined,
          tremorsense: parseInt(s.tremorsense) || undefined,
          extra: s.extra,
        }
    : {};

export const renderSenses = (s: Senses, passivePerception: number) =>
  [
    s.blindsight && `blindsight ${s.blindsight} ft.`,
    s.darkvision && `darkvision ${s.darkvision} ft.`,
    s.tremorsense && `tremorsense ${s.tremorsense} ft.`,
    s.truesight && `truesight ${s.truesight} ft.`,
    s.extra,
    `passive Perception ${passivePerception}`,
  ]
    .filter((x) => x)
    .join(', ');
