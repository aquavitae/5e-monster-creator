import { Ability, parseAbilities, parseAbility } from './ability';

describe('parseAbility', () => {
  it('parses an ability', () => {
    expect(parseAbility('str')).toEqual(Ability.Str);
  });
});

describe('parseAbilities', () => {
  it('parses a string list of abilities', () => {
    expect(parseAbilities('str, int')).toEqual([Ability.Str, Ability.Int]);
  });

  it('sorts a list of abilities', () => {
    expect(parseAbilities('cha, dex, str, int')).toEqual([
      Ability.Str,
      Ability.Dex,
      Ability.Int,
      Ability.Cha,
    ]);
  });
});
