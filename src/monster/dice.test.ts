import { parseDice } from './dice';

describe('dice', () => {
  test('A single dice is parsed correctly', () => {
    expect(parseDice('1d8')).toEqual({
      average: 4,
      dice: [{ d: 8, n: 1 }],
      mod: 0,
      text: '1d8',
    });
  });

  test('Dice are parsed correctly with spaces', () => {
    expect(parseDice('1d8 + 2')).toEqual({
      average: 6,
      dice: [{ d: 8, n: 1 }],
      mod: 2,
      text: '1d8\u00A0+\u00A02',
    });
  });

  test('A couple of dice with a mod is parsed correctly', () => {
    expect(parseDice('2d4+6d8+10')).toEqual({
      average: 42,
      dice: [
        { d: 4, n: 2 },
        { d: 8, n: 6 },
      ],
      mod: 10,
      text: '2d4\u00A0+\u00A06d8\u00A0+\u00A010',
    });
  });

  test('Negatives are parsed correctly', () => {
    expect(parseDice('-2d4-6d8-10')).toEqual({
      average: -42,
      dice: [
        { d: 4, n: -2 },
        { d: 8, n: -6 },
      ],
      mod: -10,
      text: '-2d4\u00A0-\u00A06d8\u00A0-\u00A010',
    });
  });
});
