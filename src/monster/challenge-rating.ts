export interface ChallengeRating {
  value: number;
  text: string;
  experience: number;
  proficiency: number;
  armorClass: number;
  hitPoints: [number, number];
  attackBonus: number;
  damagePerRound: [number, number];
  saveDC: number;
}

// PartyMemberHP returns the average HP of a party member at the same level as cr
export const partyMemberHP = (cr: ChallengeRating) => 8 + 4.5 * (cr.value < 1 ? 0 : cr.value - 1);

export const cr0x0: ChallengeRating = {
  value: 0,
  text: '0',
  experience: 0,
  proficiency: 2,
  armorClass: 13,
  hitPoints: [1, 6],
  attackBonus: 3,
  damagePerRound: [0, 1],
  saveDC: 13,
};
export const cr0x10: ChallengeRating = {
  value: 0,
  text: '0',
  experience: 10,
  proficiency: 2,
  armorClass: 13,
  hitPoints: [1, 6],
  attackBonus: 3,
  damagePerRound: [0, 1],
  saveDC: 13,
};
export const cr0125: ChallengeRating = {
  value: 0.125,
  text: '1/8',
  experience: 25,
  proficiency: 2,
  armorClass: 13,
  hitPoints: [7, 35],
  attackBonus: 3,
  damagePerRound: [2, 3],
  saveDC: 13,
};
export const cr025: ChallengeRating = {
  value: 0.25,
  text: '1/4',
  experience: 50,
  proficiency: 2,
  armorClass: 13,
  hitPoints: [36, 49],
  attackBonus: 3,
  damagePerRound: [4, 5],
  saveDC: 13,
};
export const cr05: ChallengeRating = {
  value: 0.5,
  text: '1/2',
  experience: 100,
  proficiency: 2,
  armorClass: 13,
  hitPoints: [50, 70],
  attackBonus: 3,
  damagePerRound: [6, 8],
  saveDC: 13,
};
export const cr1: ChallengeRating = {
  value: 1,
  text: '1',
  experience: 200,
  proficiency: 2,
  armorClass: 13,
  hitPoints: [71, 85],
  attackBonus: 3,
  damagePerRound: [9, 14],
  saveDC: 13,
};
export const cr2: ChallengeRating = {
  value: 2,
  text: '2',
  experience: 450,
  proficiency: 2,
  armorClass: 13,
  hitPoints: [86, 100],
  attackBonus: 3,
  damagePerRound: [15, 20],
  saveDC: 13,
};
export const cr3: ChallengeRating = {
  value: 3,
  text: '3',
  experience: 700,
  proficiency: 2,
  armorClass: 13,
  hitPoints: [101, 115],
  attackBonus: 4,
  damagePerRound: [21, 26],
  saveDC: 13,
};
export const cr4: ChallengeRating = {
  value: 4,
  text: '4',
  experience: 1100,
  proficiency: 2,
  armorClass: 14,
  hitPoints: [116, 130],
  attackBonus: 5,
  damagePerRound: [27, 32],
  saveDC: 14,
};
export const cr5: ChallengeRating = {
  value: 5,
  text: '5',
  experience: 1800,
  proficiency: 3,
  armorClass: 15,
  hitPoints: [131, 145],
  attackBonus: 6,
  damagePerRound: [33, 38],
  saveDC: 15,
};
export const cr6: ChallengeRating = {
  value: 6,
  text: '6',
  experience: 2300,
  proficiency: 3,
  armorClass: 15,
  hitPoints: [146, 160],
  attackBonus: 6,
  damagePerRound: [39, 44],
  saveDC: 15,
};
export const cr7: ChallengeRating = {
  value: 7,
  text: '7',
  experience: 2900,
  proficiency: 3,
  armorClass: 15,
  hitPoints: [161, 175],
  attackBonus: 6,
  damagePerRound: [45, 50],
  saveDC: 15,
};
export const cr8: ChallengeRating = {
  value: 8,
  text: '8',
  experience: 3900,
  proficiency: 3,
  armorClass: 16,
  hitPoints: [176, 190],
  attackBonus: 7,
  damagePerRound: [51, 56],
  saveDC: 16,
};
export const cr9: ChallengeRating = {
  value: 9,
  text: '9',
  experience: 5000,
  proficiency: 4,
  armorClass: 16,
  hitPoints: [191, 205],
  attackBonus: 7,
  damagePerRound: [57, 62],
  saveDC: 16,
};
export const cr10: ChallengeRating = {
  value: 10,
  text: '10',
  experience: 5900,
  proficiency: 4,
  armorClass: 17,
  hitPoints: [206, 220],
  attackBonus: 7,
  damagePerRound: [63, 68],
  saveDC: 16,
};
export const cr11: ChallengeRating = {
  value: 11,
  text: '11',
  experience: 7200,
  proficiency: 4,
  armorClass: 17,
  hitPoints: [221, 235],
  attackBonus: 8,
  damagePerRound: [69, 74],
  saveDC: 17,
};
export const cr12: ChallengeRating = {
  value: 12,
  text: '12',
  experience: 8400,
  proficiency: 4,
  armorClass: 17,
  hitPoints: [236, 250],
  attackBonus: 8,
  damagePerRound: [75, 80],
  saveDC: 18,
};
export const cr13: ChallengeRating = {
  value: 13,
  text: '13',
  experience: 10000,
  proficiency: 5,
  armorClass: 18,
  hitPoints: [251, 265],
  attackBonus: 8,
  damagePerRound: [81, 86],
  saveDC: 18,
};
export const cr14: ChallengeRating = {
  value: 14,
  text: '14',
  experience: 11500,
  proficiency: 5,
  armorClass: 18,
  hitPoints: [266, 280],
  attackBonus: 8,
  damagePerRound: [87, 92],
  saveDC: 18,
};
export const cr15: ChallengeRating = {
  value: 15,
  text: '15',
  experience: 13000,
  proficiency: 5,
  armorClass: 18,
  hitPoints: [281, 295],
  attackBonus: 8,
  damagePerRound: [93, 98],
  saveDC: 18,
};
export const cr16: ChallengeRating = {
  value: 16,
  text: '16',
  experience: 15000,
  proficiency: 5,
  armorClass: 18,
  hitPoints: [296, 310],
  attackBonus: 9,
  damagePerRound: [99, 104],
  saveDC: 18,
};
export const cr17: ChallengeRating = {
  value: 17,
  text: '17',
  experience: 18000,
  proficiency: 6,
  armorClass: 19,
  hitPoints: [311, 325],
  attackBonus: 10,
  damagePerRound: [105, 110],
  saveDC: 19,
};
export const cr18: ChallengeRating = {
  value: 18,
  text: '18',
  experience: 20000,
  proficiency: 6,
  armorClass: 19,
  hitPoints: [326, 340],
  attackBonus: 10,
  damagePerRound: [111, 116],
  saveDC: 19,
};
export const cr19: ChallengeRating = {
  value: 19,
  text: '19',
  experience: 22000,
  proficiency: 6,
  armorClass: 19,
  hitPoints: [341, 355],
  attackBonus: 10,
  damagePerRound: [117, 122],
  saveDC: 19,
};
export const cr20: ChallengeRating = {
  value: 20,
  text: '20',
  experience: 25000,
  proficiency: 6,
  armorClass: 19,
  hitPoints: [356, 400],
  attackBonus: 10,
  damagePerRound: [123, 140],
  saveDC: 19,
};
export const cr21: ChallengeRating = {
  value: 21,
  text: '21',
  experience: 30000,
  proficiency: 7,
  armorClass: 19,
  hitPoints: [401, 445],
  attackBonus: 11,
  damagePerRound: [141, 158],
  saveDC: 20,
};
export const cr22: ChallengeRating = {
  value: 22,
  text: '22',
  experience: 41000,
  proficiency: 7,
  armorClass: 19,
  hitPoints: [446, 490],
  attackBonus: 11,
  damagePerRound: [159, 176],
  saveDC: 20,
};
export const cr23: ChallengeRating = {
  value: 23,
  text: '23',
  experience: 50000,
  proficiency: 7,
  armorClass: 19,
  hitPoints: [491, 535],
  attackBonus: 11,
  damagePerRound: [177, 194],
  saveDC: 20,
};
export const cr24: ChallengeRating = {
  value: 24,
  text: '24',
  experience: 62000,
  proficiency: 7,
  armorClass: 19,
  hitPoints: [536, 580],
  attackBonus: 11,
  damagePerRound: [195, 212],
  saveDC: 21,
};
export const cr25: ChallengeRating = {
  value: 25,
  text: '25',
  experience: 75000,
  proficiency: 8,
  armorClass: 19,
  hitPoints: [581, 625],
  attackBonus: 12,
  damagePerRound: [213, 230],
  saveDC: 21,
};
export const cr26: ChallengeRating = {
  value: 26,
  text: '26',
  experience: 90000,
  proficiency: 8,
  armorClass: 19,
  hitPoints: [626, 670],
  attackBonus: 12,
  damagePerRound: [231, 248],
  saveDC: 21,
};
export const cr27: ChallengeRating = {
  value: 27,
  text: '27',
  experience: 105000,
  proficiency: 8,
  armorClass: 19,
  hitPoints: [671, 715],
  attackBonus: 13,
  damagePerRound: [249, 266],
  saveDC: 22,
};
export const cr28: ChallengeRating = {
  value: 28,
  text: '28',
  experience: 120000,
  proficiency: 8,
  armorClass: 19,
  hitPoints: [716, 760],
  attackBonus: 13,
  damagePerRound: [267, 284],
  saveDC: 22,
};
export const cr29: ChallengeRating = {
  value: 29,
  text: '29',
  experience: 135000,
  proficiency: 9,
  armorClass: 19,
  hitPoints: [760, 805],
  attackBonus: 13,
  damagePerRound: [285, 302],
  saveDC: 22,
};
export const cr30: ChallengeRating = {
  value: 30,
  text: '30',
  experience: 155000,
  proficiency: 9,
  armorClass: 19,
  hitPoints: [805, 850],
  attackBonus: 14,
  damagePerRound: [303, 320],
  saveDC: 23,
};

export const crTable: ChallengeRating[] = [
  cr0x0,
  cr0x10,
  cr0125,
  cr025,
  cr05,
  cr1,
  cr2,
  cr3,
  cr4,
  cr5,
  cr6,
  cr7,
  cr8,
  cr9,
  cr10,
  cr11,
  cr12,
  cr13,
  cr14,
  cr15,
  cr16,
  cr17,
  cr18,
  cr19,
  cr20,
  cr21,
  cr22,
  cr23,
  cr24,
  cr25,
  cr26,
  cr27,
  cr28,
  cr29,
  cr30,
];

export const crByIndexFloor = (index: number) => {
  const i = Math.floor(index);
  return crTable[i < 0 ? 0 : i > 34 ? 34 : i];
};

export const crByValue = (value: number) => {
  if (value <= 0) return cr0x0;
  if (value >= 30) return cr30;
  if (value < 0.1875) return cr0125;
  if (value < 0.375) return cr025;
  if (value < 0.75) return cr05;
  const v = Math.round(value);
  for (const cr of crTable) {
    if (cr.value === v) {
      return cr;
    }
  }
  // Mathematically, this line can never be reached.
  return cr0x0;
};

export const crByString = (text: string) => {
  switch (text) {
    case '00':
      return cr0x0;
    case '0':
      return cr0x10;
    case '1/2':
      return cr05;
    case '1/4':
      return cr025;
    case '1/8':
      return cr0125;
    case '1':
      return cr1;
    case '2':
      return cr2;
    case '3':
      return cr3;
    case '4':
      return cr4;
    case '5':
      return cr5;
    case '6':
      return cr6;
    case '7':
      return cr7;
    case '8':
      return cr8;
    case '9':
      return cr9;
    case '10':
      return cr10;
    case '11':
      return cr11;
    case '12':
      return cr12;
    case '13':
      return cr13;
    case '14':
      return cr14;
    case '15':
      return cr15;
    case '16':
      return cr16;
    case '17':
      return cr17;
    case '18':
      return cr18;
    case '19':
      return cr19;
    case '20':
      return cr20;
    case '21':
      return cr21;
    case '22':
      return cr22;
    case '23':
      return cr23;
    case '24':
      return cr24;
    case '25':
      return cr25;
    case '26':
      return cr26;
    case '27':
      return cr27;
    case '28':
      return cr28;
    case '29':
      return cr29;
    case '30':
      return cr30;
    default:
      return cr0x0;
  }
};

export const indexFor = (value: number, attr: 'hp' | 'dpr') => {
  for (let i = 0; i < crTable.length; i++) {
    const [lower, upper] = crTable[i][attr === 'hp' ? 'hitPoints' : 'damagePerRound'];
    if (value <= upper) {
      return i + Math.min((value - lower) / (upper - lower), 0.9);
    }
  }
  return crTable.length - 1;
};

export const indexAsCRFloat = (index: number) => {
  const i = Math.floor(index);
  if (i < 0) return 0;
  if (i >= 34) return 30;
  const a = crTable[i].value;
  const b = crTable[i + 1].value;
  const cr = a + (index - i) * (b - a);
  return +cr.toFixed(cr < 1 ? 2 : 1);
};
