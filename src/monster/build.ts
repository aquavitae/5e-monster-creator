import { cr4, crByString, crByValue } from './challenge-rating';
import { optimalArmor, parseArmorTemplates } from './armor';
import { makeDice } from './dice';
import { hitDieForSize, parseSize } from './size';
import { parsePronoun, Pronoun } from './pronoun';
import { parseAlignment } from './alignment';
import { parseSpeed } from './speed';
import { parseResistances } from './resistances';
import { parseTraits } from './traits/parser';
import { Monster, MonsterDefinition } from './types';
import { modifier, parseLanguages } from './utils';
import { parseSkills } from './skills';
import { parseAbilities } from './ability';
import { parseLegendaryActions } from './traits/legendary';
import { calculateCR } from './cr-calcs';
import { parseScores } from './scores';
import { Trait } from './traits/trait';
import { parseMonsterType } from './monster-type';
import { parseSenses } from './senses';
import { parseConditions } from './conditions';

// Use 4 as the startingCR since values higher than that affect the proficiency
// bonus and cn result is slightly higher final CRs that would otherwise
// be calculated.
const startingCR = cr4;

export const compile = (props: MonsterDefinition): Monster => {
  const name = props.name || '<.name>';
  const pronoun = parsePronoun(props.pronoun);
  const size = parseSize(props.size);
  const hpLevel = props.hpLevel || 1;
  const scores = parseScores(props.scores);

  const hitDice = makeDice({
    dice: [{ n: hpLevel, d: hitDieForSize(size) }],
    mod: modifier(scores.con) * hpLevel,
  });
  const armorTemplates = parseArmorTemplates(props.armor);
  const traits = parseTraits(props.traits || []);
  const explicitCR = props.challengeRating ? crByString(props.challengeRating) : undefined;

  let monster: Monster = {
    title: props.title || name,
    name,
    pronoun,
    theName: pronoun === Pronoun.It ? `the ${name.toLowerCase()}` : name,
    size,
    type: parseMonsterType(props.type),
    subtype: props.subtype,
    alignment: parseAlignment(props.alignment),
    speed: parseSpeed(props.speed),
    hitPoints: props.hitPoints || hitDice.average,
    hitDice,
    armor: armorTemplates,
    armorDesc: props.armorDesc,
    optimalArmor: [],
    traitArmor: [],
    saves: parseAbilities(props.saves),
    skills: parseSkills(props.skills),
    expertise: parseSkills(props.expertise),

    explicitCR,
    adjustCR: props.adjustCR || 0,
    cr: explicitCR || startingCR,
    crDetails: {
      stats: { hp: 0, ac: 0, dpr: 0, ab: 0, dc: 0 },
      statChanges: [],
      offensiveIndex: undefined,
      defensiveIndex: undefined,
      unadjusted: startingCR,
    },

    scores,

    damageVulnerabilities: parseResistances(props.damageVulnerabilities),
    damageResistances: parseResistances(props.damageResistances),
    damageImmunities: parseResistances(props.damageImmunities),
    conditionImmunities: parseConditions(props.conditionImmunities),
    senses: parseSenses(props.senses),
    languages: parseLanguages(props.languages),
    languageExtra: props.languageExtra,

    traits,
    legendaryActions: parseLegendaryActions(props.legendaryActions),
  };

  monster = applyTraitsToMonster(monster, traits);
  monster.optimalArmor = optimalArmor(monster);

  return applyCR(monster);
};

const applyTraitsToMonster = (monster: Monster, traits: Trait[]) =>
  traits.reduce((m, t) => t.adjustMonster(m), monster);

const applyCR = (m: Monster) => {
  const crs = [m.cr];
  for (let i = 0; i < 10; i++) {
    m = calculateCR(m);
    crs.push(m.cr);

    console.debug('Calculated CR', m.cr.value);

    // If we're in a cycle, take the average
    if (i > 1 && crs[i] === crs[i - 2]) {
      m.cr = crByValue((m.cr.value + crs[i - 1].value) / 2);
    }

    // If it is the same as the last one, then we've converged, so stop
    if (i > 0 && crs[i - 1].value === m.cr.value) {
      return m;
    }
  }

  console.log(`${m.name}: too many iterations`);
  return m;
};
