export enum Size {
  Tiny = 'Tiny',
  Small = 'Small',
  Medium = 'Medium',
  Large = 'Large',
  Huge = 'Huge',
  Gargantuan = 'Gargantuan',
}

export const hitDieForSize = (size: Size) => {
  switch (size) {
    case Size.Tiny:
      return 4;
    case Size.Small:
      return 6;
    case Size.Medium:
      return 8;
    case Size.Large:
      return 10;
    case Size.Huge:
      return 12;
    case Size.Gargantuan:
      return 20;
  }
};

export const parseSize = (s?: string) => {
  switch ((s || '').toLowerCase()) {
    case 'tiny':
      return Size.Tiny;
    case 'small':
      return Size.Small;
    default:
    case 'medium':
      return Size.Medium;
    case 'large':
      return Size.Large;
    case 'huge':
      return Size.Huge;
    case 'gargantuan':
      return Size.Gargantuan;
  }
};
