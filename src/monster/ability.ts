import { parseStringArray } from './utils';
import { cmp } from '../util';
import { sentence } from './traits/util';

export enum Ability {
  Str = 'str',
  Dex = 'dex',
  Con = 'con',
  Int = 'int',
  Wis = 'wis',
  Cha = 'cha',
}

export const parseAbility = (s: any): Ability | undefined => {
  if (typeof s !== 'string') {
    return undefined;
  }
  return Ability[sentence(s.slice(0, 3)) as keyof typeof Ability] || undefined;
};

const uniqueReducer = <T>(r: T[], c: T) =>
  r.length === 0 || c !== r[r.length - 1] ? [...r, c] : r;

export const parseAbilities = (abilities: any): Ability[] =>
  parseStringArray(abilities)
    .map((p) => parseAbility(p))
    .filter((s): s is Ability => s !== undefined)
    .sort(cmp((x: Ability) => Object.getOwnPropertyNames(Ability).indexOf(sentence(x))))
    .reduce(uniqueReducer, [] as Ability[]);

export const abilityName = (name: Ability) => {
  switch (name) {
    case 'str':
      return 'Strength';
    case 'dex':
      return 'Dexterity';
    case 'con':
      return 'Constitution';
    case 'int':
      return 'Intelligence';
    case 'wis':
      return 'Wisdom';
    case 'cha':
      return 'Charisma';
  }
};
