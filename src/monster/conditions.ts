import { parseStringArray } from './utils';

export enum Condition {
  Blinded = 'blinded',
  Charmed = 'charmed',
  Deafened = 'deafened',
  Fatigued = 'fatigued',
  Frightened = 'frightened',
  Grappled = 'grappled',
  Incapacitated = 'incapacitated',
  Invisible = 'invisible',
  Paralyzed = 'paralyzed',
  Petrified = 'petrified',
  Poisoned = 'poisoned',
  Prone = 'prone',
  Restrained = 'restrained',
  Stunned = 'stunned',
  Unconscious = 'unconscious',
  Exhaustion = 'exhaustion',
}

const parseCondition = (s: any): Condition | undefined => {
  if (typeof s !== 'string') {
    return undefined;
  }

  if (Object.values(Condition).indexOf(s.toLowerCase() as Condition) >= 0) {
    return s.toLowerCase() as Condition;
  }
  return undefined;
};

export const parseConditions = (skills: any): Condition[] =>
  parseStringArray(skills)
    .map((p) => parseCondition(p))
    .filter((s) => s !== undefined) as Condition[];
