import React from 'react';
import styled from '@emotion/styled';
import Navbar from 'react-bootstrap/Navbar';
import { H1 } from './styles';
import Nav from 'react-bootstrap/Nav';
import { Session } from '../api/session';

type Props = {
  session: Session;
};

const StyledNavbar = styled(Navbar)({ gridArea: 'nav' });

const NavMenu: React.FC<Props> = ({ session }) => (
  <StyledNavbar bg="dark" variant="dark" expand="lg">
    <Navbar.Brand as={H1} href="#home">
      5e Monster Creator
    </Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mr-auto" />
      <Nav>
        {session.isLoggedIn() ? (
          <Nav.Link onClick={() => session.logout()}>Logout</Nav.Link>
        ) : (
          <Nav.Link onClick={() => session.login()}>Login</Nav.Link>
        )}
      </Nav>
    </Navbar.Collapse>
  </StyledNavbar>
);

export default NavMenu;
