import { complementColor, H1, H2, SidebarDiv } from './styles';
import WideLine from './generic/WideLine';
import React from 'react';
import HiddenButtonBlock from './generic/HiddenButtonBlock';
import styled from '@emotion/styled/macro';
import { lighten, tint } from 'polished';
import { faPlus, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import MonstersList from './MonstersList';
import { StoredMonster } from './storage';

type Props = {
  monsters: StoredMonster[];
  activeIndex: number;
  select: (i: number) => void;
  add: (...docs: string[]) => void;
  remove: (i: number) => void;
};

const Row = styled.div<{ active?: boolean }>(({ active }) => ({
  ...(active ? { background: tint(0.1, complementColor) } : {}),
  margin: '0.2em 0',
  padding: '0.2em 0.5em',
  cursor: 'pointer',
  display: 'flex',
  '&:hover': {
    background: lighten(0.1, complementColor),
  },
}));

const Span = styled.div({
  cursor: 'pointer',
  flex: 1,
  'svg + &': {
    paddingLeft: '0.5em',
  },
});

const LeftSidebarDiv = styled(SidebarDiv)({ gridArea: 'left' });

const LeftSidebar: React.FC<Props> = ({ monsters, activeIndex, select, add, remove }) => (
  <LeftSidebarDiv>
    <H1>5e Monster Creator</H1>
    <WideLine />
    <H2>Monsters</H2>
    <MonstersList add={add}>
      {monsters.map((m, i) => (
        <Row active={i === activeIndex} key={i} onClick={() => select(i)}>
          <HiddenButtonBlock icon={faTrashAlt} confirm onClick={() => remove(i)}>
            <Span>
              {m.name} (CR {m.cr})&nbsp;
            </Span>
          </HiddenButtonBlock>
        </Row>
      ))}
      <WideLine thickness={2} />
      <Row onClick={() => add('')}>
        <FontAwesomeIcon fixedWidth icon={faPlus} />
        <Span>Create new</Span>
      </Row>
    </MonstersList>
  </LeftSidebarDiv>
);

export default LeftSidebar;
