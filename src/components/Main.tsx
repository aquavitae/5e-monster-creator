import React, { useEffect, useState } from 'react';
import StatBlock from './statblock/StatBlock';
import { compile } from '../monster/build';
import { MonsterDefinition } from '../monster/types';
import YAML from 'yaml';
import YAMLEditor from './editor/YAMLEditor';
import { parseYAML } from '../util';
import styled from '@emotion/styled/macro';
import ChallengeDetails from './calcdetails/ChallengeDetails';
import Row from './generic/Row';
import { complementColor, complementDark, shadow } from './styles';
import { shade } from 'polished';
import MenuButton from './generic/MenuButton';

type Props = {
  doc: string;
  updateDoc: (doc: string) => void;
};

const Buttons = styled.div({
  background: complementDark,
  boxShadow: shadow(shade(0.6, complementColor)),
  margin: '1.5rem 2rem',
  color: 'white',
});

const Centre = styled.div({
  display: 'flex',
  gridArea: 'main',
  flexDirection: 'column',
  flex: 1,
});

const Column = styled.div<{ flex: string | number }>(({ flex }) => ({
  padding: 20,
  overflowY: 'auto',
  overflowX: 'hidden',
  display: 'flex',
  flexDirection: 'column',
  flex: flex,
  alignItems: 'center',
}));

const Main: React.FC<Props> = ({ doc, updateDoc }) => {
  const [showEditControls, setShowEditControls] = useState(true);
  const [showYamlEditor, setShowYamlEditor] = useState(false);
  const [openAll, setOpenAll] = useState<boolean | undefined>(undefined);
  const [columns, setColumns] = useState<number>(0);

  useEffect(() => {
    if (openAll !== undefined) {
      setOpenAll(undefined);
    }
  }, [openAll]);

  const updateDef = (def: MonsterDefinition) => updateDoc(YAML.stringify(def));

  const def = parseYAML(doc);
  const monster = compile(def);

  return (
    <>
      <Centre>
        <Buttons>
          <MenuButton toggled={showYamlEditor} onClick={() => setShowYamlEditor(!showYamlEditor)}>
            YAML Editor
          </MenuButton>
          <MenuButton
            toggled={showEditControls}
            onClick={() => setShowEditControls(!showEditControls)}
          >
            Show Edit Controls
          </MenuButton>
          <MenuButton onClick={() => setOpenAll(true)}>Open all</MenuButton>
          <MenuButton onClick={() => setOpenAll(false)}>Close all</MenuButton>
          <MenuButton onClick={() => setColumns(columns === 2 ? 0 : columns + 1)}>
            Columns: {columns || 'auto'}
          </MenuButton>
        </Buttons>
        <Row>
          {showYamlEditor && (
            <Column flex={2}>
              <YAMLEditor doc={doc} setDoc={updateDoc} />
            </Column>
          )}
          <Column flex={showYamlEditor ? 'none' : 1}>
            <div className="pane">
              <StatBlock
                raw={def}
                parsed={monster}
                setRaw={updateDef}
                showEditControls={showEditControls}
                openAll={openAll}
                columns={columns ? columns : showYamlEditor ? 1 : 0}
              />
            </div>
          </Column>
        </Row>
      </Centre>
      <ChallengeDetails monster={monster} />
    </>
  );
};

export default Main;
