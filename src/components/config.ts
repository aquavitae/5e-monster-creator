export type Config = {
  identityPoolId: string;
  loginClientId: string;
  loginRedirectUri: string;
  authDomain: string;
};

export const emptyConfig = {
  identityPoolId: '',
  loginClientId: '',
  loginRedirectUri: '',
  authDomain: '',
};

export const loadConfig = () =>
  fetch('/5e-monster-creator/config.json')
    .then((d) => d.json())
    .then((d) => ({
      identityPoolId: d.identity_pool_id,
      loginClientId: d.login_client_id.value,
      loginRedirectUri: d.login_redirect_uri.value,
      authDomain: d.auth_domain.value,
    }))
    .catch((e) => {
      console.error(e);
      return emptyConfig;
    });
