import React from 'react';
import { Monster } from '../../monster/types';
import { sentence } from '../../monster/traits/util';
import { TraitType } from '../../monster/traits/types';
import TraitBlock from './TraitBlock';
import EditArray from '../editor/inputs/EditArray';
import { groupProps, GroupProps, propsFor } from '../editor/inputs/InputProps';
import { LegendaryActions } from '../../monster/traits/legendary';
import Editable from '../editor/Editable';
import Row from '../generic/Row';
import NumberInput from '../editor/inputs/NumberInput';

type Props = GroupProps<LegendaryActions> & {
  monster: Monster;
  open?: boolean;
  showEditControls: boolean;
};

const LegendaryActionsBlock: React.FC<Props> = ({
  raw,
  parsed,
  setRaw,
  monster,
  showEditControls,
  open,
}) => {
  if (!(showEditControls || (parsed && parsed.actions && parsed.actions.length > 0))) {
    return <></>;
  }

  const actionProps = groupProps(raw, parsed, setRaw, [])('actions');
  const numberProps = propsFor(raw, parsed, setRaw)('number');

  return (
    <>
      <h3>Legendary Actions</h3>
      <Editable
        render={
          <p>
            {sentence(monster.theName)} can take {parsed.number} legendary actions, choosing from
            the options below. Only one legendary action option can be used at a time and only at
            the end of another creature's turn. {sentence(monster.theName)} regains spent legendary
            actions at the start of its turn.
          </p>
        }
      >
        <Row>
          <NumberInput {...numberProps} />
        </Row>
      </Editable>

      <EditArray
        {...actionProps}
        addText={showEditControls ? `Add Action` : undefined}
        filter={(t) => t.isValid() || showEditControls}
        newItem={() => ({ type: TraitType.Action })}
        renderItem={({ value, defaultValue, onChange, ...editActions }) => (
          <TraitBlock
            trait={value}
            parsedTrait={defaultValue}
            monster={monster}
            setTrait={onChange}
            open={open}
            isLegendary
            {...editActions}
          />
        )}
      />
    </>
  );
};

export default LegendaryActionsBlock;
