import React, { useEffect, useRef, useState } from 'react';
import WideLine from '../generic/WideLine';
import { Monster } from '../../monster/types';
import { TraitType } from '../../monster/traits/types';
import TraitsBlock from './TraitsBlock';
import Panel from '../generic/Panel';
import BlockHeaderA from './BlockHeaderA';
import BlockHeaderB from './BlockHeaderB';
import BlockHeaderC from './BlockHeaderC';
import styled from '@emotion/styled/macro';
import { groupProps, GroupProps } from '../editor/inputs/InputProps';
import LegendaryActionsBlock from './LegendaryActionsBlock';

type Props = GroupProps<Monster> & {
  showEditControls: boolean;
  openAll?: boolean;
  columns: number;
};

const Columns = styled.div<{ twoColumn?: boolean }>(({ twoColumn }) => {
  const s = {
    marginTop: '1rem',
    marginBottom: '1rem',
  };
  return twoColumn
    ? { ...s, maxWidth: 900, columnCount: 2, columnWidth: 440, columnGap: 20 }
    : { ...s, width: 440 };
});

const StatBlock: React.FC<Props> = ({
  raw,
  parsed,
  setRaw,
  showEditControls,
  openAll,
  columns,
}) => {
  const [twoColumns, setTwoColumns] = useState(false);
  const ref = useRef<any>(null);

  useEffect(() => {
    const fullHeight = Array.from(ref.current.childNodes)
      .map((child: any): number => child.offsetHeight || child.height.baseVal.value)
      .reduce((r: number, x: number) => r + x, 0);
    setTwoColumns(fullHeight > 700);
  }, [raw, parsed, setRaw, showEditControls, openAll]);

  const cProps = { raw, parsed, setRaw, showEditControls: showEditControls, open: openAll };
  const gProps = groupProps(raw, parsed, setRaw, []);
  const commonProps = {
    monster: parsed,
    showEditControls: showEditControls,
    open: openAll,
  };
  const tProps = { ...gProps('traits'), ...commonProps };

  return (
    <Panel>
      <Columns ref={ref} twoColumn={(columns === 0 && twoColumns) || columns === 2}>
        <BlockHeaderA {...cProps} />
        <WideLine />
        <BlockHeaderB {...cProps} />
        <WideLine />
        <BlockHeaderC {...cProps} />

        <WideLine />

        <TraitsBlock {...tProps} type={TraitType.Ability} />
        <TraitsBlock {...tProps} type={TraitType.Action} />
        <TraitsBlock {...tProps} type={TraitType.Reaction} />

        <LegendaryActionsBlock {...gProps('legendaryActions')} {...commonProps} />
      </Columns>
    </Panel>
  );
};

export default StatBlock;
