import React from 'react';
import { Monster } from '../../monster/types';
import TraitBlock from './TraitBlock';
import { TraitType } from '../../monster/traits/types';
import { GroupProps } from '../editor/inputs/InputProps';
import EditArray from '../editor/inputs/EditArray';
import { Trait } from '../../monster/traits/trait';

type Props = GroupProps<Trait[]> & {
  monster: Monster;
  type: TraitType;
  open?: boolean;
  showEditControls: boolean;
};

const TraitsBlock: React.FC<Props> = ({
  raw,
  parsed,
  setRaw,
  monster,
  type,
  showEditControls,
  open,
}) => {
  const noTraits = parsed.filter((t) => t.type === type).length === 0;

  if (noTraits && !showEditControls) {
    return <></>;
  }

  return (
    <>
      {type !== TraitType.Ability && <h3>{type.toString()}s</h3>}
      <EditArray
        raw={raw}
        parsed={parsed}
        setRaw={setRaw}
        addText={showEditControls ? `Add ${type}` : undefined}
        filter={(t) => t.type === type && (t.isValid() || showEditControls)}
        newItem={() => ({ type })}
        renderItem={({ value, defaultValue, onChange, ...editActions }) => (
          <TraitBlock
            trait={value}
            parsedTrait={defaultValue}
            monster={monster}
            setTrait={onChange}
            open={open}
            {...editActions}
          />
        )}
      />
    </>
  );
};

export default TraitsBlock;
