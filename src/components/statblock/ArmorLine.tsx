import React from 'react';
import { Armor } from '../../monster/armor';
import PropertyLine from '../generic/PropertyLine';

type Props = {
  armor: Armor[];
  desc?: string;
};

const ArmorLine: React.FC<Props> = ({ armor, desc }) => {
  return (
    <PropertyLine name="Armor Class">
      {desc ||
        `${armor.reduce((ac, a) => ac + a.ac, 0)} 
      ${armor
        .map((a) => a.name)
        .filter((n) => n)
        .join(', ')}`}
    </PropertyLine>
  );
};

export default ArmorLine;
