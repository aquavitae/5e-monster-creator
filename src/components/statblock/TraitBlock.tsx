import React from 'react';
import { Monster } from '../../monster/types';
import { sentence } from '../../monster/traits/util';
import { Trait } from '../../monster/traits/trait';
import Editable from '../editor/Editable';
import EditTrait from '../editor/EditTrait';
import styled from '@emotion/styled/macro';
import Markdown from './Markdown';
import MoveRow from '../generic/MoveRow';
import ConfirmDelete from '../generic/ConfirmDelete';

type Props = {
  monster: Monster;
  trait: any;
  parsedTrait: Trait;
  setTrait: (trait: any) => void;
  onDelete: () => void;
  onMoveUp?: () => void;
  onMoveDown?: () => void;
  open?: boolean;
  isLegendary?: boolean;
};

const TraitDiv = styled.div<{ isLegendary?: boolean }>(({ isLegendary }) => ({
  breakInside: 'avoid',
  marginBottom: isLegendary ? 0 : '0.2em',
  width: '100%',
  '& p': {
    margin: 0,
    textIndent: isLegendary ? '-1em' : '1em',
    paddingLeft: isLegendary ? '1em' : 0,
  },
  '& p:first-child': isLegendary ? { marginTop: 0 } : { marginTop: '0.5em', textIndent: 0 },
  '& p:last-child': { marginBottom: '0.5em' },
  '& h1': {
    font: 'inherit',
    fontSize: 'inherit',
    textIndent: '-1em',
    paddingLeft: '1em',
  },
  '& ul': {
    padding: 0,
    listStyleType: 'none',
  },
}));

const TraitBlock: React.FC<Props> = ({
  monster,
  trait,
  parsedTrait,
  setTrait,
  open,
  onDelete,
  onMoveUp,
  onMoveDown,
  isLegendary,
}) => {
  const name = sentence(parsedTrait.renderName(monster));
  const desc = parsedTrait.renderDesc(monster);

  const cost = parsedTrait.cost() > 1 ? ` (Costs ${parsedTrait.cost()} actions)` : '';
  const title = isLegendary ? `${name}${cost}.` : `*${name}.*`;
  const text = `**${title}** ${desc}`;
  const props = { monster, trait, parsedTrait, setTrait };
  return (
    <TraitDiv isLegendary={isLegendary}>
      <Editable
        open={open}
        render={<Markdown key={name} text={text} />}
        extraButtons={
          <>
            {onMoveUp && <MoveRow direction={'up'} onClick={onMoveUp} />}
            {onMoveDown && <MoveRow direction={'down'} onClick={onMoveDown} />}
            <ConfirmDelete onDelete={onDelete} />
          </>
        }
      >
        <EditTrait {...props} />
      </Editable>
    </TraitDiv>
  );
};

export default TraitBlock;
