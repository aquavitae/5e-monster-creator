import React from 'react';
import PropertyLine from '../generic/PropertyLine';
import { Dice } from '../../monster/dice';

type Props = {
  hp: number;
  dice: Dice;
};

const HitPointsLine: React.FC<Props> = ({ hp, dice }) => {
  return (
    <PropertyLine name="HitPoints">
      {hp} ({dice.text})
    </PropertyLine>
  );
};

export default HitPointsLine;
