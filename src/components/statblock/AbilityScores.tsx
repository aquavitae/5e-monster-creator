import React from 'react';
import { modifier, withSign } from '../../monster/utils';
import { Scores } from '../../monster/scores';

type Props = {
  scores: Scores;
};

const AbilityScores: React.FC<Props> = ({ scores }) => {
  const score = (name: string, value: number) => (
    <div>
      <h4>{name}</h4>
      {value} ({withSign(modifier(value))})
    </div>
  );

  return (
    <div className="scores">
      {score('STR', scores.str)}
      {score('DEX', scores.dex)}
      {score('CON', scores.con)}
      {score('INT', scores.int)}
      {score('WIS', scores.wis)}
      {score('CHA', scores.cha)}
    </div>
  );
};

export default AbilityScores;
