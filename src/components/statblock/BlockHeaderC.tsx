import React from 'react';
import PropertyLine from '../generic/PropertyLine';
import { Monster } from '../../monster/types';
import ChallengeLine from './ChallengeLine';
import Editable from '../editor/Editable';
import Row from '../generic/Row';
import { modifier, withSign } from '../../monster/utils';
import { resolveAbility } from '../../monster/traits/calculators';
import { Ability } from '../../monster/ability';
import { lookup, sentence } from '../../monster/traits/util';
import { groupProps, GroupProps, propsFor } from '../editor/inputs/InputProps';
import TextInput from '../editor/inputs/TextInput';
import { makeEnumInput } from '../editor/inputs/EnumInput';
import { Skill } from '../../monster/skills';
import { renderSenses } from '../../monster/senses';
import EditSenses from '../editor/EditSenses';
import EditLanguages from '../editor/EditLanguages';
import { Condition } from '../../monster/conditions';

type Props = GroupProps<Monster> & { open?: boolean; showEditControls?: boolean };

const skillsMap: { [key: string]: Ability } = {
  acrobatics: Ability.Dex,
  'animal handling': Ability.Wis,
  arcana: Ability.Int,
  athletics: Ability.Str,
  deception: Ability.Cha,
  history: Ability.Int,
  insight: Ability.Int,
  intimidation: Ability.Cha,
  investigation: Ability.Int,
  medicine: Ability.Int,
  nature: Ability.Int,
  perception: Ability.Wis,
  performance: Ability.Cha,
  persuasion: Ability.Cha,
  religion: Ability.Int,
  'sleight of hand': Ability.Dex,
  stealth: Ability.Dex,
  survival: Ability.Wis,
};

const modifierForSkill = (m: Monster, skill: string) => {
  const ability = lookup(skillsMap, skill.toLowerCase());
  return ability ? modifier(m.scores[ability]) : 10;
};

type LProps = {
  title: string;
  value: string;
  showEditControls?: boolean;
  open?: boolean;
};

const AbilityInput = makeEnumInput(Ability, true);
const SkillsInput = makeEnumInput(Skill, true);
const ConditionsInput = makeEnumInput(Condition, true);

const Line: React.FC<LProps> = ({ value, title, showEditControls, open, children }) =>
  showEditControls || value ? (
    <Editable open={open} render={<PropertyLine name={title}>{value}</PropertyLine>}>
      <Row>{children}</Row>
    </Editable>
  ) : (
    <></>
  );

const BlockHeaderC: React.FC<Props> = ({ raw, parsed, setRaw, open, showEditControls }) => {
  const props = propsFor(raw, parsed, setRaw);
  const gProps = groupProps(raw, parsed, setRaw);

  const saves = parsed.saves
    .map(
      (s) =>
        `${sentence(s)}\u00A0${withSign(
          modifier(resolveAbility(parsed, s)) + parsed.cr.proficiency,
        )}`,
    )
    .join(', ');

  const renderSkill = (s: string, prof: number) =>
    `${sentence(s)} ${withSign(modifierForSkill(parsed, s) + prof)}`;

  const skills = [
    ...parsed.skills.map((s) => renderSkill(s, parsed.cr.proficiency)),
    ...parsed.expertise.map((s) => renderSkill(s, parsed.cr.proficiency * 2)),
  ]
    .sort()
    .join(', ');

  const percSkill = parsed.skills.indexOf(Skill.Perception) >= 0;
  const percExpertise = parsed.expertise.indexOf(Skill.Perception) >= 0;
  const ppProfMul = percSkill ? 1 : percExpertise ? 2 : 0;
  const p = 10 + modifier(parsed.scores.wis) + parsed.cr.proficiency * ppProfMul;

  const conditionImmunities = parsed.conditionImmunities.sort().join(', ');

  const lang =
    parsed.languages.length > 0
      ? parsed.languageExtra
        ? `${parsed.languages.join(', ')} (${parsed.languageExtra})`
        : parsed.languages.join(', ')
      : parsed.languageExtra || '-';

  const lProps = { showEditControls, open };

  return (
    <>
      <Line {...lProps} title="Saves" value={saves}>
        <AbilityInput {...props('saves')} />
      </Line>
      <Line {...lProps} title="Skills" value={skills}>
        <SkillsInput {...props('skills')} />
        <SkillsInput {...props('expertise')} />
      </Line>
      <Line {...lProps} title="Damage Vulnerabilities" value={parsed.damageVulnerabilities.desc}>
        <TextInput {...props('damageVulnerabilities')} defaultValue="" />
      </Line>
      <Line {...lProps} title="Damage Resistances" value={parsed.damageResistances.desc}>
        <TextInput {...props('damageResistances')} defaultValue="" />
      </Line>
      <Line {...lProps} title="Damage Immunities" value={parsed.damageImmunities.desc}>
        <TextInput {...props('damageImmunities')} defaultValue="" />
      </Line>
      <Line {...lProps} title="Condition Immunities" value={conditionImmunities}>
        <ConditionsInput {...props('conditionImmunities')} />
      </Line>
      <Editable
        open={open}
        render={<PropertyLine name="Senses">{renderSenses(parsed.senses, p)}</PropertyLine>}
      >
        <EditSenses {...gProps('senses')} />
      </Editable>
      <Editable open={open} render={<PropertyLine name="Languages">{lang}</PropertyLine>}>
        <EditLanguages raw={raw} parsed={parsed} setRaw={setRaw} />
      </Editable>

      <ChallengeLine cr={parsed.cr} />
    </>
  );
};

export default BlockHeaderC;
