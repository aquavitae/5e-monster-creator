import React from 'react';
import { Monster } from '../../monster/types';
import { Pronoun } from '../../monster/pronoun';
import Editable from '../editor/Editable';
import Row from '../generic/Row';
import { Size } from '../../monster/size';
import { Alignment } from '../../monster/alignment';
import { H1, sansSerif } from '../styles';
import TextInput from '../editor/inputs/TextInput';
import { GroupProps, propsFor } from '../editor/inputs/InputProps';
import { makeEnumInput } from '../editor/inputs/EnumInput';
import styled from '@emotion/styled/macro';
import { MonsterType } from '../../monster/monster-type';

type Props = GroupProps<Monster> & { open?: boolean };

const PronounInput = makeEnumInput(Pronoun);
const SizeInput = makeEnumInput(Size);
const AlignmentInput = makeEnumInput(Alignment);
const MonsterTypeInput = makeEnumInput(MonsterType);

const Subheader = styled.div({
  fontFamily: sansSerif,
  fontStyle: 'italic',
  lineHeight: '1.2em',
  margin: 0,
});

const BlockHeaderA: React.FC<Props> = ({ raw, parsed, setRaw, open }) => {
  const props = propsFor(raw, parsed, setRaw);

  return (
    <>
      <Editable open={open} render={<H1>{parsed.title}</H1>}>
        <Row>
          <TextInput {...props('name')} autoFocus />
          <TextInput {...props('title')} />
          <PronounInput {...props('pronoun')} />
        </Row>
      </Editable>
      <Editable
        open={open}
        render={
          <Subheader>
            {parsed.size} {parsed.type}
            {parsed.subtype && ` (${parsed.subtype})`}, {parsed.alignment}
          </Subheader>
        }
      >
        <Row>
          <SizeInput {...props('size')} autoFocus />
          <MonsterTypeInput {...props('type')} />
        </Row>
        <Row>
          <AlignmentInput {...props('alignment')} />
          <TextInput {...props('subtype')} />
        </Row>
      </Editable>
    </>
  );
};

export default BlockHeaderA;
