import React from 'react';
import PropertyLine from '../generic/PropertyLine';
import { ChallengeRating } from '../../monster/challenge-rating';

type Props = {
  cr: ChallengeRating;
};

const ChallengeLine: React.FC<Props> = ({ cr }) => {
  return (
    <PropertyLine name="Challenge">
      {cr.text} ({cr.experience} XP)
    </PropertyLine>
  );
};

export default ChallengeLine;
