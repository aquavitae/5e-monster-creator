import React from 'react';
import ReactMarkdown from 'react-markdown/with-html';
import { NodeType } from 'react-markdown';

type Props = {
  text: string;
};
const allowNodes = {
  root: true, //Root container element that contains the rendered markdown
  text: true, //Text rendered inside of other elements, such as paragraphs
  break: true, //Hard-break (<br>)
  paragraph: true, //Paragraph (<p>)
  emphasis: true, //Emphasis (<em>)
  strong: true, //Strong/bold (<strong>)
  thematicBreak: false, //Horizontal rule / thematic break (<hr>)
  blockquote: false, //Block quote (<blockquote>)
  delete: false, //Deleted/strike-through (<del>)
  link: false, //Link (<a>)
  image: false, //Image (<img>)
  linkReference: false, //Link (through a reference) (<a>)
  imageReference: false, //Image (through a reference) (<img>)
  table: true, //Table (<table>)
  tableHead: true, //Table head (<thead>)
  tableBody: true, //Table body (<tbody>)
  tableRow: true, //Table row (<tr>)
  tableCell: true, //Table cell (<td>/<th>)
  list: true, //List (<ul>/<ol>)
  listItem: true, //List item (<li>)
  definition: false, //Definition (not rendered by default)
  heading: true, //Heading (<h1>-<h6>)
  inlineCode: false, //Inline code (<code>)
  code: false, //Block of code (<pre><code>)
  html: false, //HTML node (Best-effort rendering)
  virtualHtml: false, //When not using the HTML parser plugin, a cheap and dirty approach to supporting simple HTML elements without a complete parser.
  parsedHtml: false, //When using the HTML parser plugin, HTML parsed to a React element.
};

const nodes = Object.keys(allowNodes) as Array<NodeType>;
const allowedTypes = nodes.filter((n) => allowNodes[n]);

const Markdown: React.FC<Props> = ({ text }) => (
  <ReactMarkdown source={text} allowedTypes={allowedTypes} />
);

export default Markdown;
