import React from 'react';
import PropertyLine from '../generic/PropertyLine';
import { Speed } from '../../monster/speed';

type Props = {
  speed: Speed;
};

const SpeedLine: React.FC<Props> = ({ speed }) => {
  return (
    <PropertyLine name="Speed">
      {[
        speed.walk && `${speed.walk} ft.`,
        speed.fly && `fly ${speed.fly} ft.`,
        speed.climb && `climb ${speed.climb} ft.`,
        speed.burrow && `burrow ${speed.burrow} ft.`,
        speed.swim && `swim ${speed.swim} ft.`,
        speed.extra && `(${speed.extra})`,
      ]
        .filter((s) => s)
        .join(', ')}
    </PropertyLine>
  );
};

export default SpeedLine;
