import React from 'react';
import WideLine from '../generic/WideLine';
import ArmorLine from './ArmorLine';
import AbilityScores from './AbilityScores';
import HitPointsLine from './HitPointsLine';
import { Monster } from '../../monster/types';
import SpeedLine from './SpeedLine';
import Editable from '../editor/Editable';
import Row from '../generic/Row';
import EditSpeed from '../editor/EditSpeed';
import EditScores from '../editor/EditScores';
import { groupProps, GroupProps, propsFor } from '../editor/inputs/InputProps';
import NumberInput from '../editor/inputs/NumberInput';
import EditArmors from '../editor/EditArmors';
import TextInput from '../editor/inputs/TextInput';
import Collapsible from '../generic/Collapsible';

type Props = GroupProps<Monster> & { open?: boolean };

const BlockHeaderB: React.FC<Props> = ({ raw, parsed, setRaw, open }) => {
  const props = propsFor(raw, parsed, setRaw);
  const gProps = groupProps(raw, parsed, setRaw, []);
  return (
    <>
      <Editable
        open={open}
        render={<ArmorLine armor={parsed.optimalArmor} desc={parsed.armorDesc} />}
      >
        <EditArmors {...gProps('armor')} />
        <Collapsible label={'Override'}>
          <Row>
            <TextInput {...props('armorDesc', 'Description')} />
          </Row>
        </Collapsible>
      </Editable>

      <Editable open={open} render={<HitPointsLine hp={parsed.hitPoints} dice={parsed.hitDice} />}>
        <Row>
          <NumberInput {...props('hpLevel', 'Number of hit dice')} />
          <NumberInput {...props('hitPoints')} />
        </Row>
      </Editable>

      <Editable open={open} render={<SpeedLine speed={parsed.speed} />}>
        <EditSpeed {...gProps('speed')} />
      </Editable>

      <WideLine />

      <Editable open={open} render={<AbilityScores scores={parsed.scores} />}>
        <EditScores {...gProps('scores')} />
      </Editable>
    </>
  );
};

export default BlockHeaderB;
