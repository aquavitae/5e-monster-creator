import { compile } from '../monster/build';
import { parseYAML } from '../util';

export type StoredMonster = {
  doc: string;
  name: string;
  cr: string;
};

export const makeStoredMonster = (doc: string, name?: string, cr?: string) => {
  if (name === undefined || cr === undefined) {
    const m = compile(parseYAML(doc));
    return { doc, name: m.title, cr: m.cr.text };
  }
  return { doc, name, cr };
};
