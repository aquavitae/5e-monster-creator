import styled from '@emotion/styled/macro';
import { complement, darken, lighten, shade } from 'polished';

export const colors = {
  xprimary: {
    lighter: '#fffaf2',
    light: '#fff2dc',
    base: '#f9e2ba',
    dark: '#d2b480',
    darker: '#AD8B4F',
    darkest: '#694A15',
  },
  xcompliment: {
    lightest: 'F9FAFA',
    lighter: '#DEE2E9',
    light: '#AFB9C8',
    base: '#7F8DA5',
    dark: '#5A6D8B',
    darker: '#3A5072',
    darkest: '#263C5E',
  },
  xsecondary: {
    lightest: '#FFB2A2',
    lighter: '#F7917B',
    light: '#D16149',
    base: '#B34129',
    dark: '#902610',
    darker: '#631100',
    darkest: '#3D0B00',
  },
};

export const primaryColor = '#f7ca7c';
export const secondaryColor = '#922610';
export const tertiaryColor = '#0C6A2F';
export const complementColor = complement(primaryColor);

export const primaryLight = lighten(0.1, primaryColor);
export const primaryDark = darken(0.1, primaryColor);
export const secondaryLighter = lighten(0.5, secondaryColor);
export const secondaryDark = darken(0.1, secondaryColor);
export const complementDark = darken(0.3, shade(0.2, complementColor));

export const serif =
  "'Libre Baskerville', 'Lora', 'Calisto MT', 'Bookman Old Style', Bookman, 'Goudy Old Style', Garamond, 'Hoefler Text', 'Bitstream Charter', Georgia, serif";
export const sansSerif = "'Noto Sans', 'Myriad Pro', Calibri, Helvetica, Arial, sans-serif";

export const shadow = (color: string) => `0 0 1.5rem ${color}`;
export const pane = (c: any) => ({
  background: lighten(0.2, c),
  boxShadow: shadow(shade(0.6, c)),
});

export const H1 = styled.h1({
  fontFamily: serif,
  fontSize: 24,
  color: secondaryDark,
  lineHeight: '1em',
  margin: 0,
  fontVariant: 'small-caps',
  fontWeight: 'bold',
});

export const H2 = styled.h1({
  fontFamily: serif,
  fontSize: 18,
  color: secondaryDark,
  lineHeight: '1.2em',
  margin: '1em 0 0.5em',
  borderBottom: `2px solid ${secondaryColor}`,
  fontVariant: 'small-caps',
  fontWeight: 'bold',
});

export const SidebarDiv = styled.div({
  ...pane(complementColor),
  padding: '2rem',
  display: 'flex',
  flexDirection: 'column',
});
