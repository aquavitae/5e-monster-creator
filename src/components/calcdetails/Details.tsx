import React from 'react';
import { CRStatChange } from '../../monster/traits/types';
import { withSign } from '../../monster/utils';
import styled from '@emotion/styled';

const Notes = styled.span({ fontWeight: 'normal', fontStyle: 'italic' });
const Title = styled.div({ fontWeight: 600 });

type Props = {
  title: string;
  baseValue?: number;
  value: number;
  notes?: string;
  attr: 'hp' | 'ac' | 'dpr' | 'ab' | 'dc';
  changes: CRStatChange[];
};

const Details: React.FC<Props> = ({ title, baseValue, value, notes, attr, changes }) => {
  return (
    <div className="cr-details">
      <Title>
        {title}
        <span className="value">{Math.round(value)}</span>
        {notes && <Notes> ({notes})</Notes>}
      </Title>

      {baseValue && (
        <div className="line">
          <span className="trait">Base value</span>
          <span className="value">{baseValue}</span>
        </div>
      )}

      {changes
        .filter((x) => x.diff[attr] !== 0)
        .map(({ trait, explain, diff }) => (
          <div className="line" key={trait}>
            <span className="trait">
              {trait}
              {explain && <i> ({explain})</i>}
            </span>
            <span className="value">{withSign(Math.round(diff[attr]))}</span>
          </div>
        ))}
    </div>
  );
};

export default Details;
