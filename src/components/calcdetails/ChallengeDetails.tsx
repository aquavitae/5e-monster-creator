import React from 'react';
import { Monster } from '../../monster/types';
import WideLine from '../generic/WideLine';
import Details from './Details';
import PropertyLine from '../generic/PropertyLine';
import {
  crByIndexFloor,
  indexAsCRFloat,
  indexFor,
  partyMemberHP,
} from '../../monster/challenge-rating';
import { H1, H2, SidebarDiv } from '../styles';

type Props = {
  monster: Monster;
};

const ChallengeDetails: React.FC<Props> = ({ monster }) => {
  const d = monster.crDetails;
  const partyHP = partyMemberHP(d.unadjusted);
  const offensive = crByIndexFloor(d.offensiveIndex || 0);
  const defensive = crByIndexFloor(d.defensiveIndex || 0);

  const indexHP = indexFor(d.stats.hp, 'hp');
  const addAC = +((d.stats.ac - crByIndexFloor(indexHP).armorClass) / 2).toFixed(1);
  const indexDPR = indexFor(d.stats.dpr, 'dpr');
  const addDC = d.stats.dc
    ? +((d.stats.dc - crByIndexFloor(indexDPR).saveDC) / 2).toFixed(1)
    : null;
  const addAB = d.stats.dc
    ? null
    : +((d.stats.ab - crByIndexFloor(indexDPR).attackBonus) / 2).toFixed(1);

  return (
    <SidebarDiv>
      <H1>CR {monster.cr.text} Details</H1>
      <WideLine />
      <PropertyLine name="Proficiency Bonus">{monster.cr.proficiency}</PropertyLine>
      <PropertyLine name="Save DC">{monster.cr.saveDC}</PropertyLine>
      <PropertyLine name="Typical party member HP">{partyHP}</PropertyLine>
      <WideLine />

      <H2>Offensive CR {indexAsCRFloat(d.offensiveIndex || 0)} </H2>
      <PropertyLine name="Damage per round">{offensive.damagePerRound.join(' - ')}</PropertyLine>
      <PropertyLine name="Attack bonus">{offensive.attackBonus}</PropertyLine>
      <PropertyLine name="Save DC">{offensive.saveDC}</PropertyLine>
      <WideLine thickness={2} />
      <Details
        title="Effective damage per round"
        notes={`CR ${indexAsCRFloat(indexDPR)}`}
        value={d.stats.dpr}
        attr="dpr"
        changes={d.statChanges}
      />
      <Details
        title="Effective attack bonus"
        notes={addAB ? `add CR ${addAB}` : undefined}
        value={d.stats.ab}
        attr="ab"
        changes={d.statChanges}
      />
      <Details
        title="Effective save DC"
        notes={addDC ? `add CR ${addDC}` : undefined}
        value={d.stats.dc}
        attr="dc"
        changes={d.statChanges}
      />

      <H2>Defensive CR {indexAsCRFloat(d.defensiveIndex || 0)}</H2>
      <PropertyLine name="Hit points">{defensive.hitPoints.join(' - ')}</PropertyLine>
      <PropertyLine name="Armor class">{defensive.armorClass}</PropertyLine>
      <WideLine thickness={2} />
      <Details
        title="Effective hit points"
        notes={`CR ${indexAsCRFloat(indexHP)}`}
        baseValue={monster.hitPoints}
        value={d.stats.hp}
        attr="hp"
        changes={d.statChanges}
      />
      <Details
        title="Effective armor class"
        notes={`add CR ${addAC}`}
        baseValue={monster.optimalArmor.reduce((r, a) => r + a.ac, 0)}
        value={d.stats.ac}
        attr="ac"
        changes={d.statChanges}
      />
    </SidebarDiv>
  );
};

export default ChallengeDetails;
