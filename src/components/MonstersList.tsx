import React, { useState } from 'react';
import styled from '@emotion/styled';
import { complementColor, complementDark } from './styles';
import { lighten, transparentize } from 'polished';

type Props = {
  add: (...docs: string[]) => void;
};

const Container = styled.div({
  position: 'relative',
  flex: 1,
});

const DropTargetDiv = styled.div({
  position: 'absolute',
  paddingTop: 300,
  top: 0,
  left: 0,
  border: `3px dashed ${complementDark}`,
  borderRadius: 15,
  height: '100%',
  width: '100%',
  textAlign: 'center',
  fontSize: '1.2em',
  fontWeight: 'bold',
  background: transparentize(0.4, lighten(0.1, complementColor)),
});

const readFile = (f: File): Promise<string> =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = (e) => {
      if (typeof reader.result === 'string') {
        return resolve(reader.result);
      }
      return resolve('');
    };
    reader.onerror = (e) => {
      debugger;
      reject(reader.error);
    };
    reader.readAsText(f);
  });

const MonstersList: React.FC<Props> = ({ add, children }) => {
  const [isDroppable, setIsDroppable] = useState(false);

  const stop = (e: any) => {
    e.preventDefault();
    e.stopPropagation();
  };

  const loadFiles = (files: DataTransferItemList) =>
    Promise.all(
      Array.from(files)
        .map((f) => f.getAsFile())
        .filter((f): f is File => f !== null)
        .map(async (f) => await readFile(f)),
    ).then((docs) => add(...docs.filter((d) => d)));

  return (
    <Container onDragEnter={() => setIsDroppable(true)}>
      {children}
      {isDroppable && (
        <DropTargetDiv
          onDragOver={(e) => stop(e)}
          onDragEnter={(e) => stop(e)}
          onDragLeave={(e) => {
            stop(e);
            setIsDroppable(false);
          }}
          onDropCapture={(e) => {
            loadFiles(e.dataTransfer.items).catch((e) => console.log('ERROR', e));
            setIsDroppable(false);
            stop(e);
          }}
        >
          Drop monster files here
        </DropTargetDiv>
      )}
    </Container>
  );
};

export default MonstersList;
