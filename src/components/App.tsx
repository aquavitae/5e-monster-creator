import React, { useCallback, useEffect, useState } from 'react';
import './App.css';
import LeftSidebar from './LeftSidebar';
import { cmp } from '../util';
import Main from './Main';
import { makeStoredMonster, StoredMonster } from './storage';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import NavMenu from './Nav';
import styled from '@emotion/styled';
import { loadConfig } from './config';
import { initSession, Session } from '../api/session';

const localMonsters = JSON.parse(localStorage.getItem('monsters') || '[{}]').map(
  (sm: any): StoredMonster =>
    typeof sm === 'string'
      ? makeStoredMonster(sm)
      : makeStoredMonster(sm.doc || '', sm.name || 'UNNAMED', '0'),
);

const localIndex = parseInt(localStorage.getItem('activeIndex') || '0');

const AppContainer = styled.div({
  display: 'grid',
  gridTemplateColumns: '350px auto 350px',
  gridTemplateRows: '50px auto',
  gridTemplateAreas: '"nav nav nav" "left main right"',
  alignContent: 'stretch',
  height: '100vh',
});

const App: React.FC = () => {
  const [monsters, setMonsters] = useState<StoredMonster[]>(localMonsters);
  const [activeIndex, setActiveIndex] = useState(localIndex);
  const [session, setSession] = useState(new Session());

  const saveMonsters = useCallback(
    (m: StoredMonster[]) => {
      const selected = m[activeIndex];
      const sorted = m.sort(cmp((x) => x.name));
      const index = sorted.indexOf(selected);
      localStorage.setItem('monsters', JSON.stringify(sorted));
      setMonsters(sorted);
      setActiveIndex(index);
    },
    [activeIndex],
  );

  const saveActiveIndex = (n: number) => {
    localStorage.setItem('activeIndex', n.toString());
    setActiveIndex(n);
  };

  const updateDoc = (doc: string, name?: string, cr?: string) => {
    saveMonsters(
      monsters.map((m, i) => (i === activeIndex ? makeStoredMonster(doc, name, cr) : m)),
    );
  };

  const selectMonster = (i: number) => saveActiveIndex(i);

  const addMonsters = useCallback(
    (...docs: string[]) => {
      const m = [...monsters, ...docs.map((d) => makeStoredMonster(d))];
      saveMonsters(m);
      saveActiveIndex(0);
    },
    [monsters, saveMonsters],
  );

  const removeMonster = (index: number) => {
    saveMonsters([...monsters.slice(0, index), ...monsters.slice(index + 1)]);
  };

  useEffect(() => {
    loadConfig().then((c) => setSession(initSession(c)));
    if (monsters.length === 0) addMonsters('');
    if (activeIndex < 0) setActiveIndex(0);
    if (activeIndex >= monsters.length) setActiveIndex(monsters.length - 1);
  }, [monsters, activeIndex, addMonsters]);

  const sm = monsters[activeIndex] || '';

  return (
    <AppContainer>
      <Router basename="5e-monster-creator">
        <NavMenu session={session} />
        <LeftSidebar
          monsters={monsters}
          activeIndex={activeIndex}
          select={selectMonster}
          add={addMonsters}
          remove={removeMonster}
        />
        <Switch>
          <Route path="/">
            <Main doc={sm.doc} updateDoc={updateDoc} />
          </Route>
        </Switch>
      </Router>
    </AppContainer>
  );
};

export default App;
