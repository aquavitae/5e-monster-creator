import FlatButton from './FlatButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowDown, faArrowUp } from '@fortawesome/free-solid-svg-icons';
import React from 'react';

type Props = {
  direction: 'up' | 'down';
  onClick: () => void;
};

const MoveRow: React.FC<Props> = ({ direction, onClick }) => (
  <FlatButton visible={true} onClick={onClick}>
    <FontAwesomeIcon icon={direction === 'up' ? faArrowUp : faArrowDown} />
  </FlatButton>
);

export default MoveRow;
