import styled from '@emotion/styled/macro';

const Row = styled.div<{ flex?: number }>(({ flex = 1 }) => ({
  display: 'flex',
  flex,
  flexDirection: 'row',
  margin: '0',
  '& + &': { marginTop: '0.3em' },
}));

export default Row;
