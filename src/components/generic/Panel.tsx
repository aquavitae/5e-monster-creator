import React from 'react';

type Props = {
  width?: string;
};

const Panel: React.FC<Props> = ({ width, children }) => {
  return (
    <div style={{ width: width || '100%', display: 'flex' }}>
      <div className="panel">
        <hr className="orange-border" />
        {children}
        <hr className="orange-border bottom" />
      </div>
    </div>
  );
};

export default Panel;
