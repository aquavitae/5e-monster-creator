import React from 'react';
import styled from '@emotion/styled/macro';
import { secondaryColor } from '../styles';

type Props = {
  thickness?: number;
};

const Svg = styled.svg<Props>(({ thickness }) => ({
  display: 'block',
  width: '100%',
  height: thickness || 5,
  border: 'none',
  color: secondaryColor,
  fill: secondaryColor,
  margin: '0.5em 0',
}));

const WideLine: React.FC<Props> = (props) => (
  <Svg {...props} height="5" width="100%" viewBox="0 0 100 5" preserveAspectRatio="none">
    <polyline points="0,0 100,2.5 0,5" />
  </Svg>
);

export default WideLine;
