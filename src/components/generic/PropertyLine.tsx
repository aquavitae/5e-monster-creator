import React from 'react';
import styled from '@emotion/styled';
import { secondaryDark } from '../styles';

type Props = {
  name: string;
  hidden?: boolean;
};

const Line = styled.div({
  textIndent: '-1em',
  paddingLeft: '1.1em',
  lineHeight: '1.4em',
  maxHeight: '100px',
  overflow: 'hidden',
  '& > *': {
    display: 'inline',
    margin: 0,
    lineHeight: '1.2em',
  },
});

const Title = styled.span({
  color: secondaryDark,
  fontWeight: 600,
  marginRight: '0.5em',
});

const PropertyLine: React.FC<Props> = ({ name, hidden, children }) => {
  return hidden ? (
    <></>
  ) : (
    <Line>
      <Title>{name}</Title>
      <span>{children}</span>
    </Line>
  );
};

export default PropertyLine;
