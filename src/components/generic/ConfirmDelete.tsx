import React from 'react';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import styled from '@emotion/styled';
import HiddenButtonBlock from '../generic/HiddenButtonBlock';

const Div = styled.div({
  display: 'flex',
  alignItems: 'flex-end',
});

type Props = {
  onDelete: () => void;
};

const ConfirmDelete: React.FC<Props> = ({ onDelete }) => {
  return (
    <Div>
      <HiddenButtonBlock visible={true} icon={faTrashAlt} confirm onClick={onDelete} />
    </Div>
  );
};

export default ConfirmDelete;
