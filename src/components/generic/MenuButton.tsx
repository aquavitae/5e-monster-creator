import React from 'react';
import styled from '@emotion/styled/macro';
import { darken, lighten } from 'polished';
import { complementDark } from '../styles';

type ButtonProps = {
  color?: string;
  toggled?: boolean;
};

type Props = ButtonProps & {
  onClick?: () => void;
};

const Button = styled.button<ButtonProps>(({ toggled }) => ({
  border: 'none',
  background: toggled ? lighten(0.2, complementDark) : 'none',
  cursor: 'pointer',
  color: '#DDD',
  font: 'inherit',
  fontSize: 'inherit',
  padding: '1em',
  transition: 'color 200ms',
  '&:hover': {
    color: 'white',
  },
  '&:focus': {
    outline: 'none',
  },
  '&:active': {
    background: toggled ? lighten(0.1, complementDark) : darken(0.1, complementDark),
  },
}));

const MenuButton: React.FC<Props> = ({ onClick, ...buttonProps }) => (
  <Button
    {...buttonProps}
    onClick={(e) => {
      e.preventDefault();
      e.stopPropagation();
      onClick && onClick();
    }}
  />
);

export default MenuButton;
