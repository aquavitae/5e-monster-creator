import React, { useState } from 'react';
import styled from '@emotion/styled/macro';
import { secondaryColor, secondaryDark } from '../styles';
import { transparentize } from 'polished';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown, faChevronRight } from '@fortawesome/free-solid-svg-icons';

const col1 = transparentize(1, secondaryDark);
const col2 = transparentize(0.25, secondaryDark);

const Header = styled.div({
  display: 'flex',
  margin: '0.5em 0',
  alignItems: 'center',
});

const Text = styled.div({
  paddingRight: '0.5em',
  marginLeft: '0.2em',
  color: secondaryDark,
  whiteSpace: 'nowrap',
});

const Line = styled.div({
  display: 'flex',
  width: '100%',
  color: secondaryColor,
  fill: secondaryColor,
  border: 0,
  height: 1,
  backgroundImage: `linear-gradient(to right,  ${col2}, ${col1})`,
  alignItems: 'center',
});

type Props = {
  label: string;
  expanded?: boolean;
};
const Collapsible: React.FC<Props> = ({ label, expanded, children }) => {
  const [open, setOpen] = useState(expanded);

  return (
    <div>
      <Header onClick={() => setOpen(!open)}>
        <FontAwesomeIcon
          fixedWidth
          icon={open ? faChevronDown : faChevronRight}
          color={secondaryDark}
          size="sm"
        />
        <Text>{label}</Text>
        <Line />
      </Header>
      {open && children}
    </div>
  );
};

export default Collapsible;
