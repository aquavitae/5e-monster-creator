import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import FlatButton from '../generic/FlatButton';
import { faCheck, faTimes } from '@fortawesome/free-solid-svg-icons';
import { secondaryColor, secondaryLighter, tertiaryColor } from '../styles';
import styled from '@emotion/styled';
import { shade } from 'polished';

type Props = {
  icon: any;
  visible?: boolean;
  confirm?: boolean;
  clickableBlock?: boolean;
  onClick: () => void;
};

const Container = styled.div({
  display: 'flex',
  position: 'relative',
  width: '100%',
});

const Children = styled.div<{ clickableBlock?: boolean }>(({ clickableBlock }) => ({
  flex: 1,
  cursor: clickableBlock ? 'pointer' : 'auto',
}));

const Confirm = styled.span({
  position: 'absolute',
  right: 0,
  bottom: 0,
  whiteSpace: 'nowrap',
  background: secondaryLighter,
  borderRadius: 100,
  paddingLeft: 3,
  paddingRight: 3,
  boxShadow: `0 0 5px ${shade(0.3, secondaryLighter)}`,
  zIndex: 999,
});

const Spacer = styled.span({
  width: 20,
});

const HiddenButtonBlock: React.FC<Props> = ({
  icon,
  visible,
  confirm,
  clickableBlock,
  onClick,
  children,
}) => {
  const [hovered, setHovered] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);

  const handler = () => (confirm ? setShowConfirm(true) : onClick());
  const handleConfirm = (ok: boolean) => {
    if (ok) onClick();
    setShowConfirm(false);
  };

  const clear = () => {
    setHovered(false);
    setShowConfirm(false);
  };

  return (
    <Container
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={() => clear()}
      onClick={clickableBlock ? () => handler() : undefined}
    >
      <Children clickableBlock={clickableBlock}>{children}</Children>
      {showConfirm ? (
        <>
          <Confirm>
            <i>Confirm? </i>
            <FlatButton visible color={tertiaryColor} onClick={() => handleConfirm(true)}>
              <FontAwesomeIcon icon={faCheck} />
            </FlatButton>
            <FlatButton visible color={secondaryColor} onClick={() => handleConfirm(false)}>
              <FontAwesomeIcon icon={faTimes} />
            </FlatButton>
          </Confirm>
          <Spacer />
        </>
      ) : (
        <FlatButton visible={visible || hovered} onClick={handler}>
          <FontAwesomeIcon icon={icon} />
        </FlatButton>
      )}
    </Container>
  );
};

export default HiddenButtonBlock;
