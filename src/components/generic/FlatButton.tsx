import React from 'react';
import styled from '@emotion/styled/macro';
import { shade } from 'polished';
import { secondaryColor } from '../styles';

type ButtonProps = {
  color?: string;
  visible?: boolean;
};

type Props = ButtonProps & {
  onClick?: () => void;
};

const Button = styled.button<ButtonProps>(({ color, visible }) => ({
  opacity: visible ? 1 : 0,
  padding: 2,
  border: 'none',
  borderRadius: '50%',
  background: 'none',
  color: color || secondaryColor,
  cursor: 'pointer',
  minHeight: 20,
  minWidth: 20,
  fontSize: '0.9rem',
  transition: 'opacity cubic-bezier(0.46, 0.03, 0.52, 0.96) 300ms, color 300ms',
  '&:hover': {
    opacity: 1,
    filter: `drop-shadow(0 0 5px ${shade(0.3, color || secondaryColor)})`,
  },
  '&:focus': {
    outline: 'none',
  },
}));

const FlatButton: React.FC<Props> = ({ onClick, ...buttonProps }) => (
  <Button
    {...buttonProps}
    onClick={(e) => {
      e.preventDefault();
      e.stopPropagation();
      onClick && onClick();
    }}
  />
);

export default FlatButton;
