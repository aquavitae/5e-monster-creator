import React from 'react';
import { Monster } from '../../monster/types';
import { Trait } from '../../monster/traits/trait';
import { TraitType } from '../../monster/traits/types';
import { groupProps, GroupProps, propsFor } from './inputs/InputProps';
import EditArray, { ArrayItemProps } from './inputs/EditArray';
import Row from '../generic/Row';
import NumberInput from './inputs/NumberInput';
import styled from '@emotion/styled/macro';
import { secondaryColor } from '../styles';
import InputWrapper from './inputs/InputWrapper';
import Selector from './inputs/Selector';
import ConfirmDelete from '../generic/ConfirmDelete';
import Collapsible from '../generic/Collapsible';

type Props = GroupProps<Trait> & { monster: Monster };
type GProps = ArrayItemProps<any> & { attacks: string[]; index: number };

const GroupBlock = styled.div({ display: 'flex', flex: 1 });
const GroupItems = styled.div({ display: 'flex', flex: 4, flexDirection: 'column' });

const AllOfBlock = styled.span({
  borderRight: `1px solid ${secondaryColor}`,
  display: 'flex',
  alignItems: 'center',
  whiteSpace: 'nowrap',
  margin: 3,
  minWidth: '4.5em',
  flex: 1,
});

const AllOfText = styled.span({
  width: '100%',
  paddingRight: '0.5em',
  textAlign: 'end',
});

const EditAttack: React.FC<GProps> = ({ value, defaultValue, onChange, attacks, onDelete }) => {
  const props = propsFor(value, defaultValue, onChange);

  return (
    <Row>
      <NumberInput {...props('number')} flex={1} />
      <InputWrapper title={'Attack'} flex={3}>
        <Selector {...props('attack')} options={attacks} isCreatable />
      </InputWrapper>
      <ConfirmDelete onDelete={onDelete} />
    </Row>
  );
};

const EditAttackGroup: React.FC<GProps> = ({
  value,
  defaultValue,
  onChange,
  attacks,
  onDelete,
  index,
}) => {
  type Attack = { attack: string; number: number };
  const toArray = (x: any) =>
    Object.keys(x)
      .sort()
      .map((k) => ({ attack: k, number: x[k] }));

  const fromArray = (a: Attack[]) => a.reduce((r, x) => ({ ...r, [x.attack]: x.number }), {});

  return (
    <GroupBlock>
      <AllOfBlock>
        <ConfirmDelete onDelete={onDelete} />
        <AllOfText>{index !== 0 ? 'Or all of' : 'All of'}</AllOfText>
      </AllOfBlock>
      <GroupItems>
        <EditArray
          raw={toArray(value)}
          parsed={toArray(defaultValue)}
          setRaw={(m) => onChange(fromArray(m))}
          addText="Add attack"
          renderItem={(props, index) => <EditAttack {...props} index={index} attacks={attacks} />}
          newItem={() => ({ attack: '', number: 1 })}
        />
      </GroupItems>
    </GroupBlock>
  );
};

const EditMultiattack: React.FC<Props> = ({ raw, parsed, setRaw, monster }) => {
  const props = groupProps(raw, parsed.props, setRaw, []);

  const attacks = monster.traits
    .filter((t: Trait) => t.type === TraitType.Action)
    .filter((t) => !('multiattack' in t.props))
    .map((t: Trait) => t.canonicalName);

  return (
    <Collapsible label="Multiattack details">
      <EditArray
        {...props('multiattack')}
        addText="Add alternative group"
        renderItem={(props, index) => (
          <EditAttackGroup {...props} attacks={attacks} index={index} />
        )}
      />
    </Collapsible>
  );
};

export default EditMultiattack;
