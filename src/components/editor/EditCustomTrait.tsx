import React from 'react';
import Row from '../generic/Row';
import TextInput from './inputs/TextInput';
import { GroupProps, propsFor } from './inputs/InputProps';
import Collapsible from '../generic/Collapsible';

const EditCustomTrait: React.FC<GroupProps<any>> = ({ raw, parsed, setRaw }) => {
  const props = propsFor(raw, parsed, setRaw);

  return (
    <Collapsible label="Custom trait details">
      <Row>
        <TextInput {...props('dpr', 'DPR Calc')} />
        <TextInput {...props('ab', 'AB Calc')} />
        <TextInput {...props('dc', 'DC Calc')} />
        <TextInput {...props('explain', 'Explanation')} />
      </Row>
    </Collapsible>
  );
};

export default EditCustomTrait;
