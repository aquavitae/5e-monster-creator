import React from 'react';
import Row from '../generic/Row';
import TextInput from './inputs/TextInput';
import { GroupProps, propsFor } from './inputs/InputProps';

const EditTraitAdjust: React.FC<GroupProps<any>> = ({ raw, parsed, setRaw }) => {
  const props = propsFor(raw, parsed, setRaw);

  return (
    <>
      <Row>
        <TextInput {...props('hp', 'Adjust HP Calc')} />
        <TextInput {...props('ac', 'Adjust AC Calc')} />
      </Row>
      <Row>
        <TextInput {...props('dpr', 'Adjust DPR Calc')} />
        <TextInput {...props('ab', 'Adjust AB Calc')} />
        <TextInput {...props('dc', 'Adjust DC Calc')} />
      </Row>
      <Row>
        <TextInput {...props('explain')} />
      </Row>
    </>
  );
};

export default EditTraitAdjust;
