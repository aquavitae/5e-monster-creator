import React from 'react';
import Row from '../generic/Row';
import { groupProps, GroupProps, propsFor } from './inputs/InputProps';
import Selector from './inputs/Selector';
import InputWrapper from './inputs/InputWrapper';
import { Monster } from '../../monster/types';
import TextInput from './inputs/TextInput';

const languageOptions = [
  { label: 'Abyssal', value: 'Abyssal' },
  { label: 'Aquan', value: 'Aquan' },
  { label: 'Auran', value: 'Auran' },
  { label: 'Celestial', value: 'Celestial' },
  { label: 'Common', value: 'Common' },
  { label: 'Deep', value: 'Deep' },
  { label: 'Draconic', value: 'Draconic' },
  { label: 'Druidic', value: 'Druidic' },
  { label: 'Dwarvish', value: 'Dwarvish' },
  { label: 'Elvish', value: 'Elvish' },
  { label: 'Giant', value: 'Giant' },
  { label: 'Gnoll', value: 'Gnoll' },
  { label: 'Gnomish', value: 'Gnomish' },
  { label: 'Goblin', value: 'Goblin' },
  { label: 'Halfling', value: 'Halfling' },
  { label: 'Ignan', value: 'Ignan' },
  { label: 'Infernal', value: 'Infernal' },
  { label: 'Orc', value: 'Orc' },
  { label: 'Primordial', value: 'Primordial' },
  { label: 'Sylvan', value: 'Sylvan' },
  { label: 'Terran', value: 'Terran' },
  { label: 'Undercommon', value: 'Undercommon' },
];

const EditLanguages: React.FC<GroupProps<Monster>> = ({ raw, parsed, setRaw, children }) => {
  const lProps = groupProps(raw, parsed, setRaw)('languages');
  return (
    <>
      <Row>
        <InputWrapper title="Languages">
          <Selector
            options={languageOptions}
            value={lProps.parsed.map((p: any) => ({ value: p, label: p }))}
            onChange={lProps.setRaw}
            isMulti
            isCreatable
          />
        </InputWrapper>
      </Row>
      <Row>
        <TextInput {...propsFor(raw, parsed, setRaw)('languageExtra', 'Extra text')} />
      </Row>
    </>
  );
};

export default EditLanguages;
