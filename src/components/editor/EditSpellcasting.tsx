import React from 'react';
import { Trait } from '../../monster/traits/trait';
import Row from '../generic/Row';
import { GroupProps, propsFor } from './inputs/InputProps';
import NumberInput from './inputs/NumberInput';
import { makeEnumInput } from './inputs/EnumInput';
import { Ability } from '../../monster/ability';
import Collapsible from '../generic/Collapsible';
import EditArray, { ArrayItemProps } from './inputs/EditArray';
import ConfirmDelete from '../generic/ConfirmDelete';
import { SpellList } from '../../monster/traits/spellcasting/spells';
import { ArmorTemplate } from '../../monster/armor';
import TextInput from './inputs/TextInput';
import DiceInput from './inputs/DiceInput';

enum CasterList {
  bard = 'bard',
  cleric = 'cleric',
  druid = 'druid',
  sorcerer = 'sorcerer',
  warlock = 'warlock',
  wizard = 'wizard',
}

const levels = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
const CasterListInput = makeEnumInput(CasterList);
const AbilityInput = makeEnumInput(Ability);

const EditSpell: React.FC<ArrayItemProps<ArmorTemplate>> = ({
  value,
  defaultValue,
  onChange,
  onDelete,
}) => {
  const props = propsFor(value, defaultValue, onChange);
  return (
    <Row>
      <TextInput {...props('name')} />
      <DiceInput {...props('damage', 'Damage dice')} />
      <ConfirmDelete onDelete={onDelete} />
    </Row>
  );
};

const EditSpellcasting: React.FC<GroupProps<Trait>> = ({ raw, parsed, setRaw }) => {
  const props = propsFor(raw, parsed.props, setRaw);

  const levelTitle = (level: number) => (level === 0 ? 'Cantrips' : `Level ${level}`);
  const setSlots = (level: number, value: number) =>
    setRaw({
      ...raw,
      slots: levels.map((l) => (l === level ? value : (raw.slots && raw.slots[level]) || 0)),
    });

  const setSpells = (level: number, spells: SpellList) =>
    setRaw({ ...raw, spells: { ...raw.spells, [level]: spells } });

  return (
    <>
      <Row>
        <NumberInput {...props('level', 'Caster level')} />
        <CasterListInput {...props('list', 'Spell list')} />
        <AbilityInput {...props('ability')} />
      </Row>
      {levels.map((level) => (
        <Collapsible key={level} label={levelTitle(level)}>
          <Row>
            <NumberInput
              label="Slots"
              value={(raw.slots && raw.slots[level]) || 0}
              defaultValue={parsed.props.slots[level]}
              onChange={(value: number) => setSlots(level, value)}
            />
          </Row>
          <EditArray
            raw={(raw.spells && raw.spells[level]) || []}
            parsed={parsed.props.spells[level] || []}
            setRaw={(v) => setSpells(level, v)}
            renderItem={(p, index) => <EditSpell {...p} />}
            addText="Add spell"
          />
        </Collapsible>
      ))}
    </>
  );
};

export default EditSpellcasting;
