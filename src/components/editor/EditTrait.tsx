import React from 'react';
import { Monster } from '../../monster/types';
import { traitsTable } from '../../monster/traits/traits';
import { Trait } from '../../monster/traits/trait';
import Row from '../generic/Row';
import EditProps from './EditProps';
import InputWrapper from './inputs/InputWrapper';
import Selector from './inputs/Selector';
import EditWeapon from './EditWeapon';
import Collapsible from '../generic/Collapsible';
import { groupProps } from './inputs/InputProps';
import { sentence } from '../../monster/traits/util';
import EditMultiattack from './EditMultiattack';
import TextInput from './inputs/TextInput';
import TextAreaInput from './inputs/TextAreaInput';
import EditTraitAdjust from './EditTraitAdjust';
import EditCustomTrait from './EditCustomTrait';
import EditSpellcasting from './EditSpellcasting';

type Props = {
  monster: Monster;
  trait: any;
  parsedTrait: Trait;
  setTrait: (t: Trait) => void;
};

const getTraitType = (trait: any) => {
  if ('trait' in trait && trait.trait in traitsTable) return trait.trait;
  if ('weapon' in trait) return 'weapon attack';
  if ('multiattack' in trait) return 'multiattack';
  return 'custom trait';
};

const EditTrait: React.FC<Props> = ({ monster, trait, parsedTrait, setTrait }) => {
  trait = trait || {};
  const traitType = getTraitType(trait);

  const setTraitType = (v: string, oldTrait: any) => {
    const { trait, weapon, multiattack, ...p } = oldTrait;

    if (v in traitsTable) {
      setTrait({ ...p, trait: v });
    } else if (v === 'weapon attack') {
      setTrait({ ...p, weapon: weapon || '' });
    } else if (v === 'multiattack') {
      setTrait({ ...p, multiattack: multiattack || [] });
    } else {
      setTrait(p);
    }
  };

  const traitsList = (Object.keys(traitsTable) as Array<keyof typeof traitsTable>)
    .sort()
    .filter((k) => traitsTable[k].type === parsedTrait.type)
    .map((k) => ({
      label: sentence((traitsTable[k].canonicalName || traitsTable[k].name || '').toString()),
      value: k,
    }));

  const options = ['custom trait', 'weapon attack', 'multiattack', ...traitsList];

  const props = { raw: trait, parsed: parsedTrait, setRaw: setTrait };
  const gProps = groupProps(trait, parsedTrait, setTrait);
  console.log(parsedTrait);
  return (
    <div className="edit-trait">
      <Row>
        <InputWrapper title="Type">
          <Selector value={traitType} options={options} onChange={(v) => setTraitType(v, trait)} />
        </InputWrapper>
      </Row>
      {traitType === 'weapon attack' ? (
        <EditWeapon {...props} />
      ) : traitType === 'multiattack' ? (
        <EditMultiattack {...props} monster={monster} />
      ) : traitType === 'custom trait' ? (
        <EditCustomTrait {...gProps('setStats')} />
      ) : parsedTrait.canonicalName === 'Spellcasting' ? (
        <EditSpellcasting {...props} />
      ) : (
        Object.keys(parsedTrait.props).length > 0 && <EditProps {...props} />
      )}
      <Collapsible label={'Overrides'}>
        <Row>
          <TextInput
            label="Override Name"
            value={trait.name}
            defaultValue={parsedTrait.renderName(monster).toString()}
            onChange={(n) => setTrait({ ...trait, name: n })}
          />
        </Row>
        <Row>
          <TextAreaInput
            label="Override Description"
            value={trait.desc}
            defaultValue={parsedTrait.renderDesc(monster).toString()}
            onChange={(n) => setTrait({ ...trait, desc: n })}
          />
        </Row>
        <EditTraitAdjust {...gProps('adjust')} />
      </Collapsible>
    </div>
  );
};

export default EditTrait;
