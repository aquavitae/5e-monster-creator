import React from 'react';
import InputWrapper from './InputWrapper';
import Selector from './Selector';
import { InputProps } from './InputProps';
import { armorList } from '../../../monster/armor';

type Props = InputProps<string>;

const ArmorInput: React.FC<Props> = ({ label, value, defaultValue, onChange, flex }) => {
  const asString = (v: any) => (v === undefined || v === null ? undefined : v.toString());

  return (
    <InputWrapper title={label} flex={flex}>
      <Selector
        options={armorList}
        value={asString(value)}
        defaultValue={asString(defaultValue)}
        onChange={onChange}
        isCreatable
      />
    </InputWrapper>
  );
};

export default ArmorInput;
