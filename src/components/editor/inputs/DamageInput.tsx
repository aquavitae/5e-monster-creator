import React from 'react';
import { makeEnumInput } from './EnumInput';
import { InputProps } from './InputProps';
import { DamageType } from '../../../monster/traits/damage';
import InputWrapper from './InputWrapper';
import { Input } from './Input';

const DamageTypeInput = makeEnumInput(DamageType);

const DamageInput: React.FC<InputProps<any>> = ({ label, value, defaultValue, onChange, flex }) => {
  const setValue = (v: any, field: string) => {
    return onChange({ ...value, [field]: v });
  };

  return (
    <InputWrapper title={label} flex={flex}>
      <Input
        value={value?.amount || ''}
        type="text"
        placeholder={defaultValue?.amount.text || ''}
        onChange={(e) => setValue(e.target.value, 'amount')}
        size={4}
        flex={1}
      />
      <DamageTypeInput
        value={value?.type}
        defaultValue={defaultValue?.type}
        onChange={(v) => setValue(v, 'type')}
        flex={2}
      />
    </InputWrapper>
  );
};

export default DamageInput;
