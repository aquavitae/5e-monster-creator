import { capitalCase } from 'change-case';

export type InputProps<T> = {
  label?: string;
  value?: T;
  defaultValue?: T;
  onChange: (value: T) => void;
  flex?: number;
  autoFocus?: boolean;
};

export const propsFor = (obj: any, parsed: any, setObj: (v: any) => void) => (
  field: string,
  label?: string,
): InputProps<any> => ({
  label: label || capitalCase(field),
  value: (obj && obj[field]) || undefined,
  defaultValue: (parsed && parsed[field]) || undefined,
  onChange: (v: any) => setObj({ ...obj, [field]: v }),
});

export type GroupProps<T> = {
  raw: any;
  parsed: T;
  setRaw: (m: any) => void;
};

export const groupProps = (
  raw: any,
  parsed: any,
  setRaw: (v: any) => void,
  emptyValue: any = {},
) => (field: string): GroupProps<any> => ({
  raw: (raw && raw[field]) || emptyValue,
  parsed: (parsed && parsed[field]) || emptyValue,
  setRaw: (v: any) => v !== null && v !== undefined && setRaw({ ...raw, [field]: v }),
});
