import React from 'react';
import styled from '@emotion/styled';
import { GroupProps, InputProps } from './InputProps';
import Row from '../../generic/Row';
import FlatButton from '../../generic/FlatButton';

const Div = styled.div({
  breakInside: 'avoid',
  display: 'flex',
  alignItems: 'flex-end',
});

export type ArrayItemProps<U> = InputProps<U> & {
  onDelete: () => void;
  onMoveUp?: () => void;
  onMoveDown?: () => void;
};

type Props<U, A extends Array<U>> = GroupProps<A> & {
  addText?: string;
  renderItem: (props: ArrayItemProps<U>, index: number) => JSX.Element;
  newItem?: () => U;
  filter?: (item: U) => boolean;
};

type IEditArray<U = any, A extends Array<U> = Array<U>> = React.FC<Props<U, A>>;

const EditArray: IEditArray = <U, A extends Array<U> = Array<U>>({
  raw,
  parsed,
  setRaw,
  addText,
  renderItem,
  filter,
  newItem,
}: Props<U, A>) => {
  const setItem = (t: any, i: number) => setRaw([...raw.slice(0, i), t, ...raw.slice(i + 1)]);
  const deleteItem = (i: number) => setRaw([...raw.slice(0, i), ...raw.slice(i + 1)]);
  const moveItemUp = (i: number) =>
    setRaw([...raw.slice(0, i - 1), raw[i], raw[i - 1], ...raw.slice(i + 1)]);
  const moveItemDown = (i: number) =>
    setRaw([...raw.slice(0, i), raw[i + 1], raw[i], ...raw.slice(i + 2)]);

  return (
    <>
      {parsed
        .map((p, i) => ({ value: raw[i], defaultValue: p, index: i }))
        .filter((x) => (filter ? filter(x.defaultValue) : true))
        .map(({ value, defaultValue, index }, i, all) => (
          <Div key={index}>
            {renderItem(
              {
                value,
                defaultValue,
                onChange: (a) => setItem(a, index),
                onDelete: () => deleteItem(index),
                onMoveUp: i === 0 ? undefined : () => moveItemUp(index),
                onMoveDown: i === all.length - 1 ? undefined : () => moveItemDown(index),
              },
              index,
            )}
          </Div>
        ))}
      {addText && (
        <Row>
          <FlatButton visible={true} onClick={() => setRaw([...raw, newItem ? newItem() : {}])}>
            {addText}
          </FlatButton>
        </Row>
      )}
    </>
  );
};

export default EditArray;
