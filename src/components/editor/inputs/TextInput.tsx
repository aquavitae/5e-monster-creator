import React, { useLayoutEffect, useRef } from 'react';
import InputWrapper from './InputWrapper';
import { InputProps } from './InputProps';
import { Input } from './Input';

const TextInput: React.FC<InputProps<string>> = ({
  label,
  value,
  defaultValue,
  onChange,
  flex,
  autoFocus,
}) => {
  const ref = useRef<HTMLInputElement>(null);
  useLayoutEffect(() => {
    if (autoFocus && ref.current !== null) ref.current.focus();
  }, [autoFocus]);

  return (
    <InputWrapper title={label} flex={flex}>
      <Input
        ref={ref}
        value={value || ''}
        type="text"
        placeholder={defaultValue || ''}
        onChange={(e) => onChange(e.target.value)}
        size={4}
      />
    </InputWrapper>
  );
};

export default TextInput;
