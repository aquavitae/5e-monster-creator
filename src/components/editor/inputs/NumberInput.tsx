import React from 'react';
import InputWrapper from './InputWrapper';
import { InputProps } from './InputProps';
import { Input } from './Input';

const NumberInput: React.FC<InputProps<number>> = ({
  label,
  value,
  defaultValue,
  onChange,
  flex,
}) => {
  return (
    <InputWrapper title={label} flex={flex}>
      <Input
        value={value || ''}
        type="number"
        placeholder={(defaultValue || '').toString()}
        onChange={(e) => onChange(parseInt(e.target.value))}
        size={4}
      />
    </InputWrapper>
  );
};

export default NumberInput;
