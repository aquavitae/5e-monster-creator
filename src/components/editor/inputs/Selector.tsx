import React, { useLayoutEffect, useRef } from 'react';
import { capitalCase } from 'change-case';
import CreatableSelect from 'react-select/creatable';
import Creatable from 'react-select/creatable';
import Select from 'react-select';
import { StateManager } from 'react-select/src/stateManager';

type Option = { label: string; value: string };

type Props = {
  options: (string | Option)[];
  value?: any;
  defaultValue?: string;
  onChange: (s: string) => void;
  isMulti?: boolean;
  isCreatable?: boolean;
  flex?: number;
  autoFocus?: boolean;
};

const Selector: React.FC<Props> = ({
  options,
  value,
  defaultValue,
  onChange,
  isMulti,
  isCreatable,
  flex,
  autoFocus,
}) => {
  const selectStyles = {
    container: (base: any, state: any) => ({ ...base, flex: flex || 1 }),
    control: (base: any, state: any) => ({
      ...base,
      border: 'none',
      borderRadius: 0,
      borderBottom: '1px solid #7A200D',
      paddingLeft: '0.3em',
      minHeight: 0,
      outline: 'none',
      fontSize: '0.9em',
      boxShadow: 'none',
      '&:hover': {
        borderBottom: '1px solid #7A200D',
      },
    }),
    input: (base: any, state: any) => ({
      ...base,
      margin: 0,
      padding: 0,
    }),
    clearIndicator: (base: any, state: any) => ({
      ...base,
      padding: 0,
    }),
    indicatorSeparator: (base: any, state: any) => ({
      ...base,
      marginTop: '3px',
      marginBottom: '3px',
    }),
    dropdownIndicator: (base: any, state: any) => ({ ...base, padding: 0 }),
    valueContainer: (base: any, state: any) => ({
      ...base,
      padding: 0,
    }),
    multiValueLabel: (base: any, state: any) => ({ ...base, padding: '0 2px 0' }),
    menu: (base: any, state: any) => ({ ...base, marginTop: 0, borderRadius: 0 }),
    option: (base: any, state: any) => ({
      ...base,
      padding: '0.2em 0.3em',
      color: 'black',
      fontSize: '0.9em',
      backgroundColor: state.isSelected
        ? '#e4c9c3'
        : state.isFocused
        ? '#F1E4E1'
        : base.backgroundColor,
    }),
    singleValue: (base: any, state: any) => ({
      ...base,
      color: 'black',
    }),
  };

  const creatableRef = useRef<CreatableSelect<Option>>(null);
  const selectRef = useRef<StateManager<Option>>(null);

  useLayoutEffect(() => {
    const ref = isCreatable ? creatableRef : selectRef;
    if (autoFocus && ref.current !== null) ref.current.focus();
  }, [autoFocus, isCreatable]);

  const makeOpt = (x: string | Option) =>
    typeof x === 'string' ? { value: x.toLowerCase(), label: capitalCase(x) } : x;

  const props = {
    options: options.map(makeOpt),
    styles: selectStyles,
    defaultValue: value ? makeOpt(value) : undefined,
    hideSelectedOptions: isMulti,
    isClearable: defaultValue !== undefined,
    placeholder: defaultValue,
    isMulti: isMulti,
    onChange: (e: any) => {
      onChange(e ? (isMulti ? e.map((x: any) => x.value).join(', ') : e.value) : undefined);
    },
  };

  return isCreatable ? (
    <Creatable {...props} ref={creatableRef} />
  ) : (
    <Select {...props} ref={selectRef} />
  );
};

export default Selector;
