import styled from '@emotion/styled/macro';

type Props = {
  flex?: number;
};

const base = {
  fontSize: '0.9em',
  paddingLeft: '0.3em',
  paddingRight: '0.3em',
  border: 'none',
  borderBottom: '1px solid #7A200D',
  width: '100%',
  background: 'white',
  fontFamily: 'inherit',
  '&:focus': { outline: 'none' },
};

export const Input = styled.input<Props>(({ flex }) => ({
  ...base,
  minHeight: '21px',
  flex: flex || 1,
}));

export const TextArea = styled.textarea<Props>(({ flex }) => ({
  ...base,
  minHeight: '42px',
  resize: 'vertical',
  flex: flex || 1,
}));
