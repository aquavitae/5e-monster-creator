import React from 'react';
import InputWrapper from './InputWrapper';
import Selector from './Selector';
import { InputProps } from './InputProps';

type Props<T> = InputProps<T> & {
  enumType: T;
  isMulti?: boolean;
};

type IEnumInput<T = any> = React.FC<Props<T>>;

const EnumInput: IEnumInput = ({
  label,
  enumType,
  value,
  defaultValue,
  onChange,
  flex,
  isMulti,
  autoFocus,
}) => {
  const asString = (v: any) => (v === undefined || v === null ? undefined : v.toString());
  const selector = (
    <Selector
      options={Object.values(enumType)
        .map((x: any) => x.toString())
        .sort()}
      value={asString(value)}
      defaultValue={asString(defaultValue)}
      isMulti={isMulti}
      onChange={onChange}
      flex={flex}
      autoFocus={autoFocus}
    />
  );

  return label ? (
    <InputWrapper title={label} flex={flex}>
      {selector}
    </InputWrapper>
  ) : (
    selector
  );
};

export const makeEnumInput = <T,>(enumType: T, isMulti?: boolean) => (
  props: React.PropsWithChildren<InputProps<T>>,
) => EnumInput({ ...props, enumType, isMulti });
