import React from 'react';
import styled from '@emotion/styled/macro';
import { sansSerif } from '../../styles';

type Props = {
  title?: string;
  flex?: number;
};

const Wrapper: any = styled.div<{ flex: number }>(({ flex }) => ({
  fontFamily: sansSerif,
  flex: flex,
  padding: 0,
  margin: 0,
}));

const Label = styled.label({
  display: 'block',
  fontSize: '0.75rem',
  margin: '0 3px',
});

const Container = styled.div({
  display: 'flex',
  flex: 1,
  margin: '0 3px',
});

const InputWrapper: React.FC<Props> = ({ title, flex, children }) => (
  <Wrapper flex={flex || 1}>
    <Label>{title}</Label>
    <Container>{children}</Container>
  </Wrapper>
);

export default InputWrapper;
