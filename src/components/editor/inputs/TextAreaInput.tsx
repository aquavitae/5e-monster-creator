import React from 'react';
import InputWrapper from './InputWrapper';
import { InputProps } from './InputProps';
import { TextArea } from './Input';

const TextAreaInput: React.FC<InputProps<string>> = ({
  label,
  value,
  defaultValue,
  onChange,
  flex,
}) => {
  return (
    <InputWrapper title={label} flex={flex}>
      <TextArea
        value={value || ''}
        placeholder={defaultValue || ''}
        onChange={(e) => onChange(e.target.value)}
      />
    </InputWrapper>
  );
};

export default TextAreaInput;
