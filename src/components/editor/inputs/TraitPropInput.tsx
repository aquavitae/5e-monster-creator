import React from 'react';
import TextInput from './TextInput';
import { makeEnumInput } from './EnumInput';
import { InputProps } from './InputProps';
import NumberInput from './NumberInput';
import { DamageType } from '../../../monster/traits/damage';
import BoolInput from './BoolInput';

type Props = InputProps<any> & { defaultValue: any };

const DamageTypeInput = makeEnumInput(DamageType);

const TraitPropInput: React.FC<Props> = (props) => {
  const { label, value, defaultValue, onChange } = props;

  switch (typeof defaultValue) {
    case 'number':
      return <NumberInput {...props} />;
    case 'boolean':
      return <BoolInput {...props} />;
    case 'object':
      if ('type' in defaultValue && 'amount' in defaultValue) {
        const v = (value || {}) as { amount?: any; type?: any };
        return (
          <>
            <TextInput
              value={v.amount}
              label={`${label} amount`}
              defaultValue={defaultValue.amount.text}
              onChange={(n) => onChange({ ...v, amount: n })}
            />
            <DamageTypeInput
              value={v.type as any}
              label={`${label} type`}
              defaultValue={defaultValue.type}
              onChange={(v) => onChange({ ...v, type: v })}
            />
          </>
        );
      } else if ('text' in defaultValue) {
        return (
          <TextInput
            value={value}
            label={label}
            defaultValue={defaultValue.text}
            onChange={(v) => onChange(v)}
          />
        );
      }
  }

  return <TextInput {...props} />;
};

export default TraitPropInput;
