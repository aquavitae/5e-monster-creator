import React from 'react';
import { InputProps } from './InputProps';
import { Input } from './Input';
import InputWrapper from './InputWrapper';

const DiceInput: React.FC<InputProps<any>> = ({ label, value, defaultValue, onChange, flex }) => {
  const setValue = (v: any, field: string) => onChange({ ...value, [field]: v });

  const input = (
    <Input
      value={value || ''}
      type="text"
      placeholder={defaultValue?.text || ''}
      onChange={(e) => setValue(e.target.value, 'amount')}
      size={4}
      flex={1}
    />
  );

  return label ? (
    <InputWrapper title={label} flex={flex}>
      {input}
    </InputWrapper>
  ) : (
    input
  );
};

export default DiceInput;
