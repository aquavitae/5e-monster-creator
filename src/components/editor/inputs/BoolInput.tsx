import React from 'react';
import InputWrapper from './InputWrapper';
import { InputProps } from './InputProps';
import Selector from './Selector';

const BoolInput: React.FC<InputProps<boolean | undefined>> = ({
  label,
  value,
  defaultValue,
  onChange,
  flex,
}) => {
  const asString = (v?: boolean) => (v === undefined ? undefined : v.toString());
  return (
    <InputWrapper title={label} flex={flex}>
      <Selector
        options={['True', 'False']}
        value={asString(value)}
        defaultValue={asString(defaultValue)}
        onChange={(s) => onChange(s === 'true' ? true : s === 'false' ? false : undefined)}
      />
    </InputWrapper>
  );
};

export default BoolInput;
