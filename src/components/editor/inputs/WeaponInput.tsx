import React from 'react';
import InputWrapper from './InputWrapper';
import Selector from './Selector';
import { InputProps } from './InputProps';
import { weaponsTable } from '../../../monster/traits/weapons';

type Props = InputProps<string>;

const WeaponInput: React.FC<Props> = ({ label, value, defaultValue, onChange, flex }) => {
  const asString = (v: any) => (v === undefined || v === null ? undefined : v.toString());
  const weapons = Object.keys(weaponsTable);

  return (
    <InputWrapper title={label} flex={flex}>
      <Selector
        options={weapons}
        value={asString(value)}
        defaultValue={asString(defaultValue)}
        onChange={onChange}
        isCreatable
      />
    </InputWrapper>
  );
};

export default WeaponInput;
