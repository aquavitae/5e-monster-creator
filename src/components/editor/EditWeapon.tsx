import React from 'react';
import { Trait } from '../../monster/traits/trait';
import Row from '../generic/Row';
import { groupProps, GroupProps, propsFor } from './inputs/InputProps';
import { AttackType, WeaponType } from '../../monster/traits/weapon-type';
import TextInput from './inputs/TextInput';
import NumberInput from './inputs/NumberInput';
import DamageInput from './inputs/DamageInput';
import { makeEnumInput } from './inputs/EnumInput';
import BoolInput from './inputs/BoolInput';
import { Ability } from '../../monster/ability';
import WeaponInput from './inputs/WeaponInput';
import Collapsible from '../generic/Collapsible';
import EditArray from './inputs/EditArray';
import ConfirmDelete from '../generic/ConfirmDelete';

const WeaponTypeInput = makeEnumInput(WeaponType);
const AttackTypeInput = makeEnumInput(AttackType);
const AbilityInput = makeEnumInput(Ability);

const EditWeapon: React.FC<GroupProps<Trait>> = ({ raw, parsed, setRaw }) => {
  const props = propsFor(raw, parsed.props, setRaw);
  const gProps = groupProps(raw, parsed.props, setRaw, []);

  return (
    <>
      <Collapsible expanded label="Weapon details">
        <Row>
          <WeaponInput {...props('weapon')} flex={2} />
          <WeaponTypeInput {...props('weaponType')} />
          <AttackTypeInput {...props('attackType')} />
        </Row>
        <Row>
          <DamageInput {...props('baseDamage')} />
          <BoolInput {...props('finesse')} />
        </Row>
        <Row>
          <DamageInput {...props('versatileDamage')} />
        </Row>
      </Collapsible>
      <Collapsible label={'Advanced weapon details'}>
        <EditArray
          {...gProps('extraDamage')}
          addText="Add extra damage"
          renderItem={(p) => (
            <Row>
              <DamageInput {...p} />
              <ConfirmDelete onDelete={p.onDelete} />
            </Row>
          )}
        />
        <Row>
          <NumberInput {...props('reach')} />
          <TextInput {...props('thrown')} />
          <TextInput {...props('range')} />
          <TextInput {...props('targets')} />
        </Row>
        <Row>
          <BoolInput {...props('omitDamageModifier', 'Omit modifier')} />
          <NumberInput {...props('bonus', 'Weapon bonus')} />
        </Row>
        <Row>
          <AbilityInput {...props('ability')} />
          <BoolInput {...props('adjustForSize')} />
        </Row>
      </Collapsible>
    </>
  );
};

export default EditWeapon;
