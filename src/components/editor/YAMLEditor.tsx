import React from 'react';
import AceEditor from 'react-ace';
import 'ace-builds/src-noconflict/mode-yaml';
import 'ace-builds/src-noconflict/theme-tomorrow';
import 'ace-builds/src-min-noconflict/ext-language_tools';

type Props = {
  doc: string;
  setDoc: (doc: string) => void;
};

const YAMLEditor: React.FC<Props> = ({ doc, setDoc }) => {
  return (
    <div style={{ border: '1px solid black', height: '100%', width: '100%' }}>
      <AceEditor
        mode="yaml"
        theme="tomorrow"
        fontSize={14}
        tabSize={2}
        value={doc}
        width="100%"
        height="100%"
        onChange={(s) => setDoc(s)}
        setOptions={{
          enableBasicAutocompletion: true,
          enableLiveAutocompletion: true,
          enableSnippets: true,
          scrollPastEnd: true,
          spellcheck: true,
        }}
        editorProps={{ $blockScrolling: Infinity }}
      />
    </div>
  );
};

export default YAMLEditor;
