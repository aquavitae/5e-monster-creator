import React from 'react';
import { Trait } from '../../monster/traits/trait';
import Row from '../generic/Row';
import TraitPropInput from './inputs/TraitPropInput';
import { GroupProps, propsFor } from './inputs/InputProps';
import Collapsible from '../generic/Collapsible';

const EditProps: React.FC<GroupProps<Trait>> = ({ raw, parsed, setRaw }) => {
  const props = propsFor(raw, parsed.props, setRaw);

  return (
    <Collapsible expanded label="Trait details">
      {Object.keys(parsed.props)
        .sort()
        .map((key) => {
          const p = { defaultValue: '', ...props(key) };
          return (
            <Row key={key}>
              <TraitPropInput {...p} />
            </Row>
          );
        })}
    </Collapsible>
  );
};

export default EditProps;
