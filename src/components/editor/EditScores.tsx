import React from 'react';
import Row from '../generic/Row';
import { Scores } from '../../monster/scores';
import { Ability } from '../../monster/ability';
import NumberInput from './inputs/NumberInput';
import { GroupProps, propsFor } from './inputs/InputProps';

const EditScores: React.FC<GroupProps<Scores>> = ({ raw, parsed, setRaw, children }) => {
  const props = propsFor(raw, parsed, setRaw);

  return (
    <Row>
      {Object.getOwnPropertyNames(Ability).map((a) => (
        <NumberInput key={a} {...props(a.toLowerCase())} />
      ))}
    </Row>
  );
};

export default EditScores;
