import React, { useEffect, useState } from 'react';
import styled from '@emotion/styled/macro';
import { primaryColor, primaryLight, secondaryColor } from '../styles';
import { shade, transparentize } from 'polished';

type Props = {
  render: any;
  showEditControls?: boolean;
  open?: boolean;
  extraButtons?: JSX.Element;
};

const Main = styled.div({
  breakInside: 'avoid',
});

const Content = styled.div<{ isOpen: boolean }>(({ isOpen }) => ({
  background: isOpen ? primaryLight : 'inherit',
  cursor: 'pointer',
  position: 'relative',
  transition: 'background cubic-bezier(0.46, 0.03, 0.52, 0.96) 300ms, color 300ms',
  '&:hover': { background: primaryLight },
}));

const Form = styled.div({
  boxShadow: `1px 1px 3px ${shade(0.5, primaryColor)} inset`,
  padding: '0.5em',
  paddingBottom: '0.8em',
  background: primaryLight,
  margin: '0 0 0.2em',
});

const EditButtons = styled.div<{ visible: boolean }>(({ visible }) => ({
  opacity: visible ? 1 : 0,
  padding: 2,
  border: 'none',
  background: transparentize(0.2, shade(0.2, primaryColor)),
  position: 'absolute',
  top: 0,
  right: 0,
  whiteSpace: 'pre',
  display: 'flex',
  zIndex: 800,
  transition: 'opacity cubic-bezier(0.46, 0.03, 0.52, 0.96) 300ms, color 300ms',
  '&:hover': { color: secondaryColor },
  '&:focus': { outline: 'none' },
}));

const Editable: React.FC<Props> = ({ render, children, open, extraButtons }) => {
  const [isOpen, setIsOpen] = useState(open || false);
  const [showEdit, setShowEdit] = useState(false);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => {
    if (open !== undefined) setIsOpen(open);
  }, [open]);

  return (
    <Main className="editable">
      <Content
        isOpen={isOpen}
        onMouseEnter={() => setShowEdit(true)}
        onMouseLeave={() => setShowEdit(false)}
        onClick={() => setIsOpen(!isOpen)}
      >
        {render}
        {extraButtons && <EditButtons visible={showEdit || isOpen}>{extraButtons}</EditButtons>}
      </Content>
      {isOpen && <Form>{children}</Form>}
    </Main>
  );
};

export default Editable;
