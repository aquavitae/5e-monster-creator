import React from 'react';
import { Speed } from '../../monster/speed';
import Row from '../generic/Row';
import NumberInput from './inputs/NumberInput';
import TextInput from './inputs/TextInput';
import { GroupProps, propsFor } from './inputs/InputProps';

const speeds = ['walk', 'fly', 'climb', 'burrow', 'swim'] as Array<keyof Speed>;

const EditSpeed: React.FC<GroupProps<Speed>> = ({ raw, parsed, setRaw, children }) => {
  const props = propsFor(raw, parsed, setRaw);

  return (
    <>
      <Row>
        {speeds.map((k) => (
          <NumberInput key={k} {...props(k)} />
        ))}
      </Row>
      <Row>
        <TextInput {...props('extra')} />
      </Row>
    </>
  );
};

export default EditSpeed;
