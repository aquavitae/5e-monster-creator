import React from 'react';
import Row from '../generic/Row';
import NumberInput from './inputs/NumberInput';
import TextInput from './inputs/TextInput';
import { GroupProps, propsFor } from './inputs/InputProps';
import { Senses } from '../../monster/senses';

const EditSenses: React.FC<GroupProps<Senses>> = ({ raw, parsed, setRaw, children }) => {
  const props = propsFor(raw, parsed, setRaw);

  return (
    <>
      <Row>
        <NumberInput {...props('blindsight')} />
        <NumberInput {...props('darkvision')} />
        <NumberInput {...props('tremorsense')} />
        <NumberInput {...props('truesight')} />
      </Row>
      <Row>
        <TextInput {...props('extra')} />
      </Row>
    </>
  );
};

export default EditSenses;
