import React from 'react';
import { GroupProps, propsFor } from './inputs/InputProps';
import Row from '../generic/Row';
import { ArmorTemplate } from '../../monster/armor';
import ArmorInput from './inputs/ArmorInput';
import EditArray, { ArrayItemProps } from './inputs/EditArray';
import ConfirmDelete from '../generic/ConfirmDelete';
import NumberInput from './inputs/NumberInput';

export const EditArmor: React.FC<ArrayItemProps<ArmorTemplate>> = ({
  value,
  defaultValue,
  onChange,
  onDelete,
}) => {
  const props = propsFor(value, defaultValue, onChange);
  return (
    <Row flex={1}>
      <ArmorInput {...props('name')} flex={2} />
      <NumberInput {...props('baseAC', 'Base AC')} />
      <NumberInput {...props('maxAddDex')} />
      <NumberInput {...props('ac', 'Armor Class')} />
      <ConfirmDelete onDelete={onDelete} />
    </Row>
  );
};

const EditArmors: React.FC<GroupProps<ArmorTemplate[]>> = (props) => {
  return <EditArray {...props} addText="Add Armor" renderItem={(p) => <EditArmor {...p} />} />;
};

export default EditArmors;
