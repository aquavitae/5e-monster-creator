import React, { useEffect, useState } from 'react';

type Props = {
  yaml: any;
  gui: any;
};

const EditorPane: React.FC<Props> = ({ yaml, gui }) => {
  const [editor, setEditor] = useState<'gui' | 'yaml'>('gui');

  const saveEditor = (name: 'gui' | 'yaml') => {
    setEditor(name);
    localStorage.setItem('editor', name);
  };

  useEffect(() => {
    const ed = localStorage.getItem('editor');
    if (ed === 'gui' || ed === 'yaml') {
      setEditor(ed);
    }
  }, []);

  return (
    <div className="editor">
      <div className="editor-buttons">
        <button
          className={editor === 'gui' ? 'selected' : 'not-selected'}
          onClick={() => saveEditor('gui')}
        >
          GUI Editor
        </button>
        <span style={{ flexGrow: 1 }} />
        <button
          className={editor === 'yaml' ? 'selected' : 'not-selected'}
          onClick={() => saveEditor('yaml')}
        >
          YAML Editor
        </button>
      </div>

      {editor === 'gui' && gui}
      {editor === 'yaml' && yaml}
    </div>
  );
};

export default EditorPane;
