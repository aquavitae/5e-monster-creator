#!/bin/bash

set -ev

test -n "$AWS_SECRET_ACCESS_KEY"
test -n "$AWS_ACCESS_KEY_ID"

pushd .deploy

terraform workspace select dev
terraform apply --auto-approve
terraform output -json > ../public/config.json

popd
