# Monster Builder

This tool assists with designing and building monsters by removing much
of the details. For example, instead of spending your time calculating the CR
and typing up 3 subtly different weapon descriptions, you can simply say
"this monster has sunlight sensitivity, a greatsword and hide armor. CR is
way too low? Ok, add multiattack."

Monsters are described in yaml files, detailing it's basic information (e.g.
name, type, abilities, etc), a list of equipment it possesses, and a list of
traits it has. For example, consider this monster:

```yaml
# Every monster has a name
name: Tooth Fairy

# Stuff that appears at the top of the stat block, doesn't really affect much
type: fey
alignment: LN

# Stats and abilities
strength: 8
dexterity: 13
constitution: 10
intelligence: 14
wisdom: 12
charisma: 16

# Skills, saves, etc
saves: [ wisdom, charisma ]
skills: [ stealth, sleight of hand ]
expertise: [ stealth ]
damage resistances: bludgeoning, piercing and slashing damage from non-magical weapons
condition immunities: blinded
sense: truesight 120ft.
languages: all

# Features

weapons:
  # A shortbow is a standard weapon, so we don't need to say anything more
  - name: Shortbow

  # A toothpick is not standard, so we can specify a little more
  - name: Toothpick
    damage dice: 1d6
    damage type: piercing

armor:
  # It has leather armor, but will only wear it if it is
  # better than it's natural armor
  - name: Leather

  # Natural armor is the most common type of custom armor.
  - name: Natural armor
    ac: 17

traits:
  # A standard and simple trait
  - name: nimble escape

  # A slightly more complex one with a parameter
  - name: legendary resistance
    number: 3

  # probably one of the most complex
  - name: spellcasting
    ability: charisma
    list: wizard
    damage per round: 3d4 + 3
    spells:
      cantrips: mage hand, prestidigitation
      1st (2 slots): detect magic, magic missile, shield
      2nd (2 slots): detect thoughts, invisibility, mirror image
      3rd (1 slots): counterspell, dispel magic

  # A custom ability
  - ability: find tooth
    desc: The tooth fairy can sense the precise location and number of
      all teeth with a 30 ft. radius.

  # A custom action
  - action: extract teeth
    desc: The tooth fairy pulls all teeth from a target which it can see within 60 ft of it.
      The target must make a Constitution saving throw (DC {{ .SaveDC }}). If it fails,
      the target takes 3d6 damage and is stunned until the end of its next turn.
    damage per round: 3d6
    requires save: true

  - reaction: shatter tooth
    desc: When hit with a melee attack, the tooth fairy can use it's reaction to
      shatter a tooth of the attacker, dealing 1d4 damage.

legendary actions:
  number: 3
  actions:
    - name: Cast cantrip
      desc: The tooth fairly casts a cantrip

    - name: Tooth monster
      cost: 2
      desc: The tooth fairy summons a tooth monster at a point within 30 ft.
        The tooth monster can make a single melee attack on the tooth fairy's turn
        using the tooth fairy's attack bonus, dealing 2d4 + 2 damage on a hit.
      damage per round:
        add: 2d4 + 2
```

## Adjusting CR

The CR is calculated from 5 stats:
* Hit points
* Armor class
* Damage per round
* Attack bonus
* Save DC

Each feature (item, trait and legendary action) can affect these values. Some, such as most weapons,
simply set a value, assuming that weapon is used. Others, such as most traits, affect
increase values. For all built in features, these calculations are set automatically.
